package com.appetiser.module.notification.fcm

import io.reactivex.Single

interface FirebaseRepository {

    fun getDeviceToken(): Single<String>
}
