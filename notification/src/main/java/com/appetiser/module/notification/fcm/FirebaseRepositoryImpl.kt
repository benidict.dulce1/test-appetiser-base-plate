package com.appetiser.module.notification.fcm

import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.math.pow

class FirebaseRepositoryImpl @Inject constructor() : FirebaseRepository {

    override fun getDeviceToken(): Single<String> {
        return Single.create<String> {
            FirebaseInstanceId
                .getInstance()
                .instanceId
                .addOnCompleteListener(
                    OnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            Timber.e("getInstanceId failed ${task.exception}")
                            it.onError(task.exception!!)
                            return@OnCompleteListener
                        }

                        // Get new Instance ID token
                        val token = task.result?.token
                        it.onSuccess(token.orEmpty())
                        Timber.d("Token = $token")
                    }
                )
        }.retryWhen { errors: Flowable<Throwable> ->
            errors.zipWith(
                Flowable.range(1, 5 + 1),
                BiFunction<Throwable, Int, Int> { error: Throwable, retryCount: Int ->
                    if (error is Exception) {
                        throw error
                    } else {
                        retryCount
                    }
                }
            ).flatMap { retryCount: Int ->
                Flowable.timer(
                    2.toDouble().pow(retryCount.toDouble()).toLong(),
                    TimeUnit.SECONDS
                )
            }
        }
    }
}
