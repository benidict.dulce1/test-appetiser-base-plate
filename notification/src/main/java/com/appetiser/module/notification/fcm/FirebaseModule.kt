package com.appetiser.module.notification.fcm

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FirebaseModule {

    @Provides
    @Singleton
    fun providesFirebaseRepository(): FirebaseRepository = FirebaseRepositoryImpl()
}
