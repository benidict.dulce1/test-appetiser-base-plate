package com.appetiser.module.domain.utils

const val PASSWORD_MIN_LENGTH = 8
const val PASSWORD_MAX_LENGTH = 32
const val PROFILE_DESCRIPTION_MAX_LENGTH = 250

const val PROFILE_PHOTO_MAX_DIMENSION_PX = 480
const val DEFAULT_PHOTO_MAX_DIMENSION_PX = 760