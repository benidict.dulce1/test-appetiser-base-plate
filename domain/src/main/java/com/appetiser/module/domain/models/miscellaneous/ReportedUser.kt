package com.appetiser.module.domain.models.miscellaneous

import com.appetiser.module.domain.models.user.User

data class ReportedUser(
    val id: Int,
    val description: String,
    val reportType: String,
    val reportedAt: String,
    val reportedBy: Int,
    val reasonId: String,
    val reported: User
)
