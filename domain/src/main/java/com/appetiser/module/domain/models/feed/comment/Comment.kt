package com.appetiser.module.domain.models.feed.comment

import com.appetiser.module.domain.models.user.User

data class Comment(
    val id: Long = 0,
    val body: String = "",
    val authorId: Long = 0,
    val createdAt: String? = "",
    val updatedAt: String? = "",
    val author: User = User.empty()
)