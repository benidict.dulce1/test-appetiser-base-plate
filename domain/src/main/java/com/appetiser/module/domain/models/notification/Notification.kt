package com.appetiser.module.domain.models.notification

import com.appetiser.module.domain.models.user.User

data class Notification(
    val id: String,
    val type: NotificationType = NotificationType.DEFAULT,
    val notifiableId: Long = 0,
    val actorId: Long = 0,
    val message: String = "",
    val readAt: String = "",
    val read: Boolean = false,
    val created_at: String = "",
    val notifiable: User? = null,
    val actor: User? = null
)

enum class NotificationType(val type: String) {
   DEFAULT("default"), LIKE("like"), COMMENT("comment"), FOLLOW("follow")
}

