package com.appetiser.module.domain.models.track

import com.google.gson.annotations.SerializedName

data class Track(
    val dataId: Int = 0,
    val artistId: String?= null,
    val wrapperType: String?= null,
    val kind: String?= null,
    val collectionId: String?= null,
    val trackId: String?= null,
    val artistName: String?= null,
    val collectionName: String?= null,
    val trackName: String?= null,
    val collectionCensoredName: String?= null,
    val trackCensoredName: String?= null,
    val collectionTrackId: String?= null,
    val collectionTrackViewUrl: String?= null,
    val collectionViewUrl: String?= null,
    val trackViewUrl: String?= null,
    val previewUrl: String?= null,
    val artworkUrl30: String?= null,
    val artworkUrl60: String?= null,
    val artworkUrl100: String?= null,
    val collectionPrice: String?= null,
    val trackPrice: String?= null,
    val trackRentalPrice: String?= null,
    val collectionHdPrice: String?= null,
    val trackHdPrice: String?= null,
    val trackHdRentalPrice: String?= null,
    val releaseDate: String?= null,
    val collectionExplicitness: String?= null,
    val trackExplicitness: String?= null,
    val discCount: String?= null,
    val discNumber: String?= null,
    val trackCount: String?= null,
    val trackNumber: String?= null,
    val trackTimeMillis: String?= null,
    val country: String?= null,
    val currency: String?= null,
    val primaryGenreName: String?= null,
    val contentAdvisoryRating: String?= null,
    val shortDescription: String?= null,
    val longDescription: String?= null,
    val hasITunesExtras: String?= null,
)