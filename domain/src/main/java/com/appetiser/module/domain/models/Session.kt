package com.appetiser.module.domain.models

import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User

data class Session(
    var user: User,
    var accessToken: AccessToken
)