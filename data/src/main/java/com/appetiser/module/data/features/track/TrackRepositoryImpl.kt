package com.appetiser.module.data.features.track

import com.appetiser.module.domain.models.track.Track
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.local.features.track.TrackLocalSource
import com.appetiser.module.network.features.track.TrackRemoteSource
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class TrackRepositoryImpl @Inject constructor(
    private val trackLocalSource: TrackLocalSource,
    private val trackRemoteSource: TrackRemoteSource
): TrackRepository{
    override fun loadTrackList(): Single<List<Track>>
        = trackRemoteSource.loadTrackList()

    override fun deleteTrackDetails(): Completable
        = trackLocalSource.deleteTrack()

    override fun loadTrackDetails(): Single<Track> = trackLocalSource.getTrack()

    override fun saveTrackDetails(track: Track): Single<Track> = trackLocalSource.saveTrack(track)

    override fun saveLastVisitDate(date: String) {
        trackLocalSource.saveLastVisitDate(date)
    }

    override fun loadLastVisitDate(): Single<String>
        = trackLocalSource.getLastVisitDate()
}