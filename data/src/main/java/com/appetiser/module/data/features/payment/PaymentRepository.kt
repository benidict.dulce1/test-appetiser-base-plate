package com.appetiser.module.data.features.payment

import io.reactivex.Single

interface PaymentRepository {
    /**
     * Used to initialize stripe CustomerSession.
     */
    fun createEphemeralKey(): Single<String>
}
