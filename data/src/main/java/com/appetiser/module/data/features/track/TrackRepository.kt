package com.appetiser.module.data.features.track

import com.appetiser.module.domain.models.track.Track
import io.reactivex.Completable
import io.reactivex.Single

interface TrackRepository {
    fun loadTrackList(): Single<List<Track>>
    fun saveTrackDetails(track: Track): Single<Track>
    fun loadTrackDetails(): Single<Track>
    fun deleteTrackDetails(): Completable
    fun saveLastVisitDate(date: String)
    fun loadLastVisitDate(): Single<String>
}