package com.appetiser.module.data.features

import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.auth.AuthRepositoryImpl
import com.appetiser.module.data.features.comment.CommentRepository
import com.appetiser.module.data.features.comment.CommentRepositoryImpl
import com.appetiser.module.data.features.feeds.FeedRepository
import com.appetiser.module.data.features.feeds.FeedRepositoryImpl
import com.appetiser.module.data.features.miscellaneous.MiscellaneousRepository
import com.appetiser.module.data.features.miscellaneous.MiscellaneousRepositoryImpl
import com.appetiser.module.data.features.notification.NotificationRepository
import com.appetiser.module.data.features.notification.NotificationRepositoryImpl
import com.appetiser.module.data.features.payment.PaymentRepository
import com.appetiser.module.data.features.payment.PaymentRepositoryImpl
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.data.features.session.SessionRepositoryImpl
import com.appetiser.module.data.features.track.TrackRepository
import com.appetiser.module.data.features.track.TrackRepositoryImpl
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.data.features.user.UserRepositoryImpl
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.local.features.track.TrackLocalSource
import com.appetiser.module.network.features.auth.AuthRemoteSource
import com.appetiser.module.network.features.comment.CommentRemoteSource
import com.appetiser.module.network.features.feed.FeedRemoteSource
import com.appetiser.module.network.features.miscellaneous.MiscellaneousRemoteSource
import com.appetiser.module.network.features.notification.NotificationRemoteSource
import com.appetiser.module.network.features.payment.PaymentRemoteSource
import com.appetiser.module.network.features.track.TrackRemoteSource
import com.appetiser.module.network.features.user.UserRemoteSource
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesAuthRepository(
        authRemoteSource: AuthRemoteSource,
        sessionLocalSource: SessionLocalSource,
        userRemoteSource: UserRemoteSource
    ): AuthRepository {
        return AuthRepositoryImpl(authRemoteSource, sessionLocalSource, userRemoteSource)
    }

    @Provides
    @Singleton
    fun providesSessionRepository(
        sessionLocalSource: SessionLocalSource
    ): SessionRepository {
        return SessionRepositoryImpl(sessionLocalSource)
    }

    @Provides
    @Singleton
    fun providesUserRepository(
        sessionLocalSource: SessionLocalSource,
        userRemoteSource: UserRemoteSource
    ): UserRepository {
        return UserRepositoryImpl(sessionLocalSource, userRemoteSource)
    }

    @Provides
    @Singleton
    fun providesNotificationRepository(
        sessionLocalSource: SessionLocalSource,
        notificationRemoteSource: NotificationRemoteSource
    ): NotificationRepository {
        return NotificationRepositoryImpl(sessionLocalSource, notificationRemoteSource)
    }

    @Provides
    @Singleton
    fun providesFeedRepository(
        sessionLocalSource: SessionLocalSource,
        feedRemoteSource: FeedRemoteSource
    ): FeedRepository {
        return FeedRepositoryImpl(sessionLocalSource, feedRemoteSource)
    }

    @Provides
    @Singleton
    fun providesCommentRepository(
        sessionLocalSource: SessionLocalSource,
        commentRemoteSource: CommentRemoteSource
    ): CommentRepository {
        return CommentRepositoryImpl(sessionLocalSource, commentRemoteSource)
    }

    @Provides
    fun providesMiscellaneousRepository(
        sessionLocalSource: SessionLocalSource,
        miscellaneousRemoteSource: MiscellaneousRemoteSource
    ): MiscellaneousRepository {
        return MiscellaneousRepositoryImpl(sessionLocalSource, miscellaneousRemoteSource)
    }

    @Provides
    fun providesPaymentRepository(
        sessionLocalSource: SessionLocalSource,
        paymentRemoteSource: PaymentRemoteSource
    ): PaymentRepository {
        return PaymentRepositoryImpl(sessionLocalSource, paymentRemoteSource)
    }

    @Provides
    @Singleton
    fun provideTrackRepository(
        trackLocalSource: TrackLocalSource,
        trackRemoteSource: TrackRemoteSource
    ): TrackRepository {
        return TrackRepositoryImpl(trackLocalSource, trackRemoteSource)
    }

}
