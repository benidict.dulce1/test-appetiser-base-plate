package com.appetiser.module.data.features.miscellaneous

import com.appetiser.module.domain.models.miscellaneous.ReportCategory
import com.appetiser.module.domain.models.miscellaneous.ReportedUser
import io.reactivex.Single

interface MiscellaneousRepository {
    fun getReportCategories(): Single<List<ReportCategory>>

    fun reportUser(
        reportedUserId: String,
        reasonId: String,
        description: String,
        attachmentPathList: List<String>
    ): Single<ReportedUser>
}
