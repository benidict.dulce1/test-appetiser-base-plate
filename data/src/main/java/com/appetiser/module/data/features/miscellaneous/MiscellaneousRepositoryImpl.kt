package com.appetiser.module.data.features.miscellaneous

import com.appetiser.module.domain.models.miscellaneous.ReportCategory
import com.appetiser.module.domain.models.miscellaneous.ReportedUser
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.miscellaneous.MiscellaneousRemoteSource
import io.reactivex.Single
import javax.inject.Inject

class MiscellaneousRepositoryImpl @Inject constructor(
    private val sessionLocalSource: SessionLocalSource,
    private val miscellaneousRemoteSource: MiscellaneousRemoteSource
) : MiscellaneousRepository {
    override fun getReportCategories(): Single<List<ReportCategory>> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                miscellaneousRemoteSource
                    .getReportCategories(session.accessToken)
            }
    }

    override fun reportUser(
        reportedUserId: String,
        reasonId: String,
        description: String,
        attachmentPathList: List<String>
    ): Single<ReportedUser> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                miscellaneousRemoteSource
                    .reportUser(
                        session.accessToken.bearerToken,
                        reportedUserId,
                        reasonId,
                        description,
                        attachmentPathList
                    )
            }
    }
}
