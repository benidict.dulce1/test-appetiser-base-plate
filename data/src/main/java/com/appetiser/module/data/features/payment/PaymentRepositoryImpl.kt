package com.appetiser.module.data.features.payment

import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.payment.PaymentRemoteSource
import io.reactivex.Single
import javax.inject.Inject

class PaymentRepositoryImpl @Inject constructor(
    private val sessionLocalSource: SessionLocalSource,
    private val paymentRemoteSource: PaymentRemoteSource
) : PaymentRepository {
    override fun createEphemeralKey(): Single<String> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                paymentRemoteSource
                    .createEphemeralKey(
                        session.accessToken
                    )
            }
    }
}
