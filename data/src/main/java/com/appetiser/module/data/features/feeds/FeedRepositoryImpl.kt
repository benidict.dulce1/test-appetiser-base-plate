package com.appetiser.module.data.features.feeds

import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.feed.Feed
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.feed.FeedRemoteSource
import io.reactivex.Single
import javax.inject.Inject

class FeedRepositoryImpl @Inject constructor(
    private val sessionLocalSource: SessionLocalSource,
    private val feedRemoteSource: FeedRemoteSource
) : FeedRepository {

    override fun getFeeds(page: Int): Single<Paging<Feed>> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                feedRemoteSource.getFeeds(
                    session.accessToken,
                    page
                ).map { feedPages ->
                    Paging(
                        feedPages.list.map {
                            val feed = it.copy(isUserFeedOwner = session.user.id == it.author.id)
                            feed
                        },
                        feedPages.nextPage
                    )
                }
            }
    }

    override fun getFeedDetails(id: Long): Single<Feed> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                feedRemoteSource
                    .getFeedDetails(
                        session.accessToken,
                        id
                    )
                    .map {
                        val feed = it.copy(isUserFeedOwner = session.user.id == it.author.id)
                        feed
                    }
            }
    }

    override fun favorite(id: Long): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap {
                feedRemoteSource
                    .favorite(
                        it.accessToken,
                        id
                    )
            }
    }

    override fun unFavorite(id: Long): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap {
                feedRemoteSource
                    .unFavorite(
                        it.accessToken,
                        id
                    )
            }
    }

    override fun postFeed(body: String, filePath: String): Single<Feed> {
        return sessionLocalSource
            .getSession()
            .flatMap {
                feedRemoteSource
                    .postFeed(
                        it.accessToken,
                        body,
                        filePath
                    )
            }
    }

    override fun deleteFeed(id: Long): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap {
                feedRemoteSource
                    .deleteFeed(
                        it.accessToken,
                        id
                    )
            }
    }

    override fun reportFeed(postId: Long, reasonId: Long, description: String, attachmentPathList: List<String>): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap {
                feedRemoteSource
                    .reportFeed(
                        it.accessToken, postId, reasonId, description, attachmentPathList
                    )
            }
    }
}
