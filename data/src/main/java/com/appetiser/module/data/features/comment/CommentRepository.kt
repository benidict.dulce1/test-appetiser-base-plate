package com.appetiser.module.data.features.comment

import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.feed.comment.Comment
import io.reactivex.Single

interface CommentRepository {

    fun getComments(feedId: Long, page: Int): Single<Paging<Comment>>

    fun getCommentDetails(commentId: Long): Single<Comment>

    fun postComment(postId: Long, body: String): Single<Comment>
}
