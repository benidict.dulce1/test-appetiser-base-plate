package com.appetiser.module.data.features.auth

import com.appetiser.module.domain.models.Session
import com.appetiser.module.domain.models.auth.CountryCode
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.auth.AuthRemoteSource
import com.appetiser.module.network.features.user.UserRemoteSource
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val remote: AuthRemoteSource,
    private val sessionLocalSource: SessionLocalSource,
    private val userRemoteSource: UserRemoteSource
) : AuthRepository {

    override fun checkUsername(username: String): Single<Boolean> = remote.checkUsername(username)

    override fun login(username: String, password: String): Single<Session> {
        return remote
            .login(username, password)
            .flatMap(::saveAuthDataToSession)
    }

    /**
     * Call this method to login via social media.
     *
     * @param accessToken
     * @param accessTokenProvider provider of access token ["facebook", "google"].
     */
    override fun socialLogin(
        accessToken: String,
        accessTokenProvider: String
    ): Single<Session> {
        return remote
            .socialLogin(accessToken, accessTokenProvider)
            .flatMap(::saveAuthDataToSession)
    }

    override fun register(
        email: String,
        phone: String,
        password: String,
        confirmPassword: String,
        firstName: String,
        lastName: String
    ): Single<Session> {
        return remote
            .register(
                email,
                phone,
                password,
                confirmPassword,
                firstName,
                lastName
            )
            .flatMap(::saveAuthDataToSession)
    }

    private fun saveAuthDataToSession(pair: Pair<User, AccessToken>): Single<Session> {
        val (user, accessToken) = pair

        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                session.user = user
                session.accessToken = accessToken

                sessionLocalSource
                    .saveSession(session)
            }
    }

    override fun verifyAccountEmail(code: String): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                remote
                    .verifyAccountEmail(
                        session.accessToken,
                        code
                    )
            }
            .flatMap { (isSuccessful, user) ->
                if (isSuccessful) {
                    // If verification succeeds. Save user to database.
                    sessionLocalSource
                        .getSession()
                        .flatMap { session ->
                            session.user = user

                            sessionLocalSource
                                .saveSession(session)
                        }
                        .map { isSuccessful }
                } else {
                    Single.just(isSuccessful)
                }
            }
    }

    override fun verifyAccountMobilePhone(code: String): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                remote
                    .verifyAccountMobilePhone(
                        session.accessToken,
                        code
                    )
            }
            .flatMap { (isSuccessful, user) ->
                if (isSuccessful) {
                    // If verification succeeds. Save user to database.
                    sessionLocalSource
                        .getSession()
                        .flatMap { session ->
                            session.user = user

                            sessionLocalSource
                                .saveSession(session)
                        }
                        .map { isSuccessful }
                } else {
                    Single.just(isSuccessful)
                }
            }
    }

    override fun resendEmailVerificationCode(): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                remote
                    .resendEmailVerificationCode(
                        session.accessToken
                    )
            }
    }

    override fun resendMobilePhoneVerificationCode(): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                remote
                    .resendMobilePhoneVerificationCode(
                        session.accessToken
                    )
            }
    }

    override fun forgotPassword(username: String): Single<Boolean> {
        return remote.forgotPassword(username)
    }

    override fun forgotPasswordCheckCode(username: String, code: String): Single<Boolean> {
        return remote.forgotPasswordCheckCode(username, code)
    }

    override fun newPassword(
        username: String,
        token: String,
        password: String,
        confirmPassword: String
    ): Single<Boolean> {
        return remote.newPassword(username, token, password, confirmPassword)
    }

    override fun changePassword(
        oldPassword: String,
        newPassword: String,
        newPasswordConfirm: String
    ): Single<Boolean> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                remote.changePassword(
                    session.accessToken,
                    oldPassword,
                    newPassword,
                    newPasswordConfirm
                )
            }
    }

    override fun verifyPassword(password: String): Single<String> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                remote
                    .verifyPassword(
                        session.accessToken,
                        password
                    )
            }
    }

    override fun requestChangeEmail(
        verificationToken: String,
        newEmail: String
    ): Completable {
        return sessionLocalSource
            .getSession()
            .flatMapCompletable { session ->
                remote
                    .requestChangeEmail(
                        session.accessToken,
                        verificationToken,
                        newEmail
                    )
            }
    }

    override fun verifyChangeEmail(
        verificationToken: String,
        code: String
    ): Completable {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                remote
                    .verifyChangeEmail(
                        session.accessToken,
                        verificationToken,
                        code
                    )
                    .andThen(Single.just(session))
            }
            .flatMap { session ->
                // Get updated user after successfully changing email.
                userRemoteSource
                    .getUser(
                        session.accessToken
                    )
                    .flatMap { user ->
                        session.user = user
                        sessionLocalSource.saveSession(session)
                    }
            }
            .ignoreElement()
    }

    override fun requestChangePhone(
        verificationToken: String,
        newPhoneNumber: String
    ): Completable {
        return sessionLocalSource
            .getSession()
            .flatMapCompletable { session ->
                remote
                    .requestChangePhone(
                        session.accessToken,
                        verificationToken,
                        newPhoneNumber
                    )
            }
    }

    override fun verifyChangePhone(verificationToken: String, code: String): Completable {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                remote
                    .verifyChangePhone(
                        session.accessToken,
                        verificationToken,
                        code
                    )
                    .andThen(Single.just(session))
            }
            .flatMap { session ->
                // Get updated user after successfully changing phone number.
                userRemoteSource
                    .getUser(
                        session.accessToken
                    )
                    .flatMap { user ->
                        session.user = user
                        sessionLocalSource.saveSession(session)
                    }
            }
            .ignoreElement()
    }

    override fun logout(): Completable {
        return sessionLocalSource
            .getSession()
            .flatMapCompletable { session ->
                remote
                    .logout(session.accessToken)
            }
            .andThen(sessionLocalSource.clearSession())
    }

    override fun deleteAccount(verificationToken: String): Completable {
        return sessionLocalSource
            .getSession()
            .flatMapCompletable { session ->
                remote
                    .deleteAccount(
                        session.accessToken,
                        verificationToken
                    )
            }
            .andThen(sessionLocalSource.clearSession())
    }

    override fun getCountryCode(countryCodes: List<Int>): Single<List<CountryCode>> {
        return remote.getCountryCode(countryCodes)
    }
}
