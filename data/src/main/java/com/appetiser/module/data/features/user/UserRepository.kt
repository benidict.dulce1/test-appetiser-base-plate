package com.appetiser.module.data.features.user

import com.appetiser.module.domain.models.user.User
import io.reactivex.Single

interface UserRepository {
    fun updateUser(user: User): Single<User>

    fun uploadPhoto(filePath: String): Single<User>
}
