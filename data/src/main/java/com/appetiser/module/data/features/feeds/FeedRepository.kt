package com.appetiser.module.data.features.feeds

import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.feed.Feed
import io.reactivex.Single

interface FeedRepository {

    fun getFeeds(page: Int): Single<Paging<Feed>>

    fun getFeedDetails(id: Long): Single<Feed>

    fun favorite(id: Long): Single<Boolean>

    fun unFavorite(id: Long): Single<Boolean>

    fun postFeed(body: String, filePath: String): Single<Feed>

    fun deleteFeed(id: Long): Single<Boolean>

    fun reportFeed(
        postId: Long,
        reasonId: Long,
        description: String,
        attachmentPathList: List<String>
    ): Single<Boolean>
}
