package com.appetiser.module.data.features.user

import com.appetiser.module.domain.models.user.User
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.user.UserRemoteSource
import io.reactivex.Single
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val sessionLocalSource: SessionLocalSource,
    private val userRemoteSource: UserRemoteSource
) : UserRepository {
    override fun updateUser(user: User): Single<User> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                userRemoteSource
                    .updateUser(
                        session.accessToken,
                        user
                    )
            }
            .flatMap { updatedUser ->
                sessionLocalSource
                    .getSession()
                    .flatMap { session ->
                        session.user = updatedUser

                        sessionLocalSource
                            .saveSession(session)
                    }
                    .map { updatedUser }
            }
    }

    override fun uploadPhoto(filePath: String): Single<User> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                userRemoteSource
                    .uploadPhoto(
                        session.accessToken,
                        filePath
                    )
                    .map { Pair(session, it) }
            }
            .flatMap { (session, mediaFile) ->
                session.user.avatar = mediaFile
                sessionLocalSource
                    .saveSession(session)
                    .map { it.user }
            }
    }
}
