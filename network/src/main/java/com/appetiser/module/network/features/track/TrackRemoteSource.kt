package com.appetiser.module.network.features.track

import com.appetiser.module.domain.models.track.Track
import io.reactivex.Single

interface TrackRemoteSource {
    fun loadTrackList(): Single<List<Track>>
}