package com.appetiser.module.network

import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.auth.AuthRemoteSource
import com.appetiser.module.network.features.auth.AuthRemoteSourceImpl
import com.appetiser.module.network.features.comment.CommentRemoteSource
import com.appetiser.module.network.features.comment.CommentRemoteSourceImpl
import com.appetiser.module.network.features.feed.FeedRemoteSource
import com.appetiser.module.network.features.feed.FeedRemoteSourceImpl
import com.appetiser.module.network.features.miscellaneous.MiscellaneousRemoteSource
import com.appetiser.module.network.features.miscellaneous.MiscellaneousRemoteSourceImpl
import com.appetiser.module.network.features.notification.NotificationRemoteSource
import com.appetiser.module.network.features.notification.NotificationRemoteSourceImpl
import com.appetiser.module.network.features.payment.PaymentRemoteSource
import com.appetiser.module.network.features.payment.PaymentRemoteSourceImpl
import com.appetiser.module.network.features.track.TrackRemoteSource
import com.appetiser.module.network.features.track.TrackRemoteSourceImpl
import com.appetiser.module.network.features.user.UserRemoteSource
import com.appetiser.module.network.features.user.UserRemoteSourceImpl
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteSourceModule {

    @Provides
    @Singleton
    fun providesAuthRemoteSource(
        apiServices: BaseplateApiServices,
        gson: Gson
    ): AuthRemoteSource = AuthRemoteSourceImpl(apiServices, gson)

    @Provides
    @Singleton
    fun providesUserRemoteSource(
        apiServices: BaseplateApiServices
    ): UserRemoteSource = UserRemoteSourceImpl(apiServices)

    @Provides
    @Singleton
    fun providesNotificationRemoteSource(
        apiServices: BaseplateApiServices
    ): NotificationRemoteSource = NotificationRemoteSourceImpl(apiServices)

    @Provides
    @Singleton
    fun providesFeedRemoteSource(
        apiServices: BaseplateApiServices
    ): FeedRemoteSource = FeedRemoteSourceImpl(apiServices)

    @Provides
    @Singleton
    fun providesCommentSource(
        apiServices: BaseplateApiServices
    ): CommentRemoteSource = CommentRemoteSourceImpl(apiServices)

    @Provides
    fun providesMiscellaneousRemoteSource(
        apiServices: BaseplateApiServices
    ): MiscellaneousRemoteSource = MiscellaneousRemoteSourceImpl(apiServices)

    @Provides
    fun providesPaymentRemoteSource(
        apiServices: BaseplateApiServices,
        gson: Gson
    ): PaymentRemoteSource = PaymentRemoteSourceImpl(apiServices, gson)

    @Provides
    @Singleton
    fun provideTrackSource(
        apiServices: BaseplateApiServices
    ): TrackRemoteSource = TrackRemoteSourceImpl(apiServices)

}
