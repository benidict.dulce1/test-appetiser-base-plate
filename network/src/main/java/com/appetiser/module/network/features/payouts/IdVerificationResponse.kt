package com.appetiser.module.network.features.payouts

data class IdVerificationResponse(
    val data: Data,
    val http_status: Int,
    val success: Boolean
) {
    data class Data(
        val id: String,
        val `object`: String,
        val created: Int,
        val filename: String,
        val links: Links,
        val purpose: String,
        val size: Int,
        val title: Any,
        val type: String,
        val url: Any
    ) {
        data class Links(
            val `object`: String,
            val data: List<Any>,
            val has_more: Boolean,
            val url: String
        )
    }
}
