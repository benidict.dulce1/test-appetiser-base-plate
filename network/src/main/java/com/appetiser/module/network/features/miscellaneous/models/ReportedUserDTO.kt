package com.appetiser.module.network.features.miscellaneous.models

import com.appetiser.module.domain.models.miscellaneous.ReportedUser
import com.appetiser.module.network.features.user.models.UserDTO
import com.google.gson.annotations.SerializedName

class ReportedUserDTO(
    val id: Int,
    val description: String?,
    @SerializedName("report_type")
    val reportType: String,
    @SerializedName("reported_at")
    val reportedAt: String,
    @SerializedName("reported_by")
    val reportedBy: Int,
    @SerializedName("reason_id")
    val reasonId: String,
    val reported: UserDTO
) {
    companion object {

        fun toDomain(reportedUserDTO: ReportedUserDTO): ReportedUser {
            return with(reportedUserDTO) {
                ReportedUser(
                    id = id,
                    description = description.orEmpty(),
                    reportType = reportType,
                    reportedAt = reportedAt,
                    reportedBy = reportedBy,
                    reasonId = reasonId,
                    reported = UserDTO.toDomain(reported)
                )
            }
        }
    }
}
