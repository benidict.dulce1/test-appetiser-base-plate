package com.appetiser.module.network.features.feed.models

import com.appetiser.module.domain.models.feed.Feed
import com.appetiser.module.network.features.user.models.MediaFileDTO
import com.appetiser.module.network.features.user.models.UserDTO

data class FeedDTO(
    val id: Long? = 0,
    val body: String? = "",
    val author_id: Long? = 0,
    val created_at: String? = "",
    val updated_at: String? = "",
    val is_favorite: Boolean? = false,
    val comments_count: Long? = 0,
    val favorites_count: Long? = 0,
    val author: UserDTO? = null,
    val photo: MediaFileDTO? = null
) {
    companion object {
        fun toDomain(feed: FeedDTO): Feed {
            return with(feed) {
                Feed(
                    id = id ?: -1,
                    body = body.orEmpty(),
                    favoritesCount = favorites_count ?: 0,
                    commentsCount = comments_count ?: 0,
                    authorId = author_id ?: 0,
                    isFavorite = is_favorite ?: false,
                    createdAt = created_at.orEmpty(),
                    updatedAt = updated_at.orEmpty(),
                    author = UserDTO.toDomain(author),
                    photo = MediaFileDTO.toDomain(photo)
                )
            }
        }

        fun mapFeedDTOsToFeeds(feeds: List<FeedDTO>): List<Feed> {
            return feeds.map { toDomain(it) }
        }
    }
}
