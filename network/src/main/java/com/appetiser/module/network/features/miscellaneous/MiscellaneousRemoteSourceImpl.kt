package com.appetiser.module.network.features.miscellaneous

import com.appetiser.module.domain.models.miscellaneous.ReportCategory
import com.appetiser.module.domain.models.miscellaneous.ReportedUser
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.network.ext.convertAttachmentsToMultiPartBody
import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.miscellaneous.models.ReportCategoryDTO
import com.appetiser.module.network.features.miscellaneous.models.ReportedUserDTO
import io.reactivex.Single
import javax.inject.Inject

class MiscellaneousRemoteSourceImpl @Inject constructor(
    private val apiServices: BaseplateApiServices
) : MiscellaneousRemoteSource {
    override fun getReportCategories(accessToken: AccessToken): Single<List<ReportCategory>> {
        return apiServices
            .getReportCategories(accessToken.bearerToken)
            .map { it.data }
            .map { list ->
                list.map { ReportCategoryDTO.toDomain(it) }
            }
    }

    override fun reportUser(
        accessToken: String,
        reportedUserId: String,
        reasonId: String,
        description: String,
        attachmentPathList: List<String>
    ): Single<ReportedUser> {
        return if (attachmentPathList.isEmpty()) {
            apiServices
                .reportUser(
                    accessToken,
                    reportedUserId,
                    reasonId,
                    description
                )
                .map {
                    ReportedUserDTO.toDomain(it.data)
                }
        } else {
            apiServices
                .reportUser(
                    accessToken,
                    reportedUserId,
                    reasonId,
                    description,
                    attachmentPathList.convertAttachmentsToMultiPartBody()
                )
                .map {
                    ReportedUserDTO.toDomain(it.data)
                }
        }
    }
}
