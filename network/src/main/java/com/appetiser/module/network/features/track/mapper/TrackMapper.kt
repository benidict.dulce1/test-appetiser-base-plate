package com.appetiser.module.network.features.track.mapper

import com.appetiser.module.domain.models.track.Track
import com.appetiser.module.network.features.track.model.TrackResponse
import io.reactivex.Observable
import io.reactivex.Single

fun List<TrackResponse>.remoteToDomain(): Single<List<Track>>
    = Observable.fromIterable(this)
        .map {
            Track(
                dataId = it.dataId,
                artistId = it.artistId,
                wrapperType = it.wrapperType,
                kind = it.kind,
                collectionId = it.collectionId,
                trackId = it.trackId,
                artistName = it.artistName,
                collectionName = it.collectionName,
                trackName = it.trackName,
                collectionCensoredName = it.collectionCensoredName,
                trackCensoredName = it.trackCensoredName,
                collectionTrackId = it.collectionTrackId,
                collectionTrackViewUrl = it.collectionTrackViewUrl,
                collectionViewUrl = it.collectionViewUrl,
                trackViewUrl = it.trackViewUrl,
                previewUrl = it.previewUrl,
                artworkUrl30 = it.artworkUrl30,
                artworkUrl60 = it.artworkUrl60,
                artworkUrl100 = it.artworkUrl100,
                collectionPrice = it.collectionPrice,
                trackPrice = it.trackPrice,
                collectionHdPrice = it.collectionHdPrice,
                trackHdPrice = it.trackHdPrice,
                trackHdRentalPrice = it.trackHdRentalPrice,
                releaseDate = it.releaseDate,
                collectionExplicitness = it.collectionExplicitness,
                trackExplicitness = it.trackExplicitness,
                discCount = it.discCount,
                discNumber = it.discNumber,
                trackCount = it.trackCount,
                trackNumber = it.trackNumber,
                trackTimeMillis = it.trackTimeMillis,
                country = it.country,
                currency = it.currency,
                primaryGenreName = it.primaryGenreName,
                contentAdvisoryRating = it.contentAdvisoryRating,
                shortDescription = it.shortDescription,
                longDescription = it.longDescription,
                hasITunesExtras = it.hasITunesExtras
            )
        }.toList()
