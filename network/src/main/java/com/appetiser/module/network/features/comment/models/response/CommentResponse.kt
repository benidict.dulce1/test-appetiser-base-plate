package com.appetiser.module.network.features.comment.models.response

import com.appetiser.module.network.features.comment.models.CommentDTO

data class CommentResponse(val data: CommentDTO)
