package com.appetiser.module.network.features.comment

import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.feed.comment.Comment
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.comment.models.CommentDTO
import com.google.gson.JsonObject
import io.reactivex.Single
import javax.inject.Inject

class CommentRemoteSourceImpl @Inject constructor(
    private val apiServices: BaseplateApiServices
) : BaseRemoteSource(), CommentRemoteSource {

    override fun getComments(accessToken: AccessToken, feedId: Long, page: Int): Single<Paging<Comment>> {
        val query = hashMapOf<String, Any>()
        query["include[0]"] = "author"
        query["page"] = page

        return apiServices.getComments(accessToken.bearerToken, feedId, query)
            .map { response ->
                Paging(
                    response.data.map {
                        CommentDTO.toDomain(it)
                    },
                    if (response.meta.currentPage >= response.meta.lastPage) {
                        null
                    } else {
                        response.meta.currentPage + 1
                    }
                )
            }
    }

    override fun getCommentDetails(accessToken: AccessToken, commentId: Long): Single<Comment> {
        return apiServices.getComment(accessToken.bearerToken, commentId)
            .map { it.data }
            .map { CommentDTO.toDomain(it) }
            .firstOrError()
    }

    override fun postComment(accessToken: AccessToken, postId: Long, body: String): Single<Comment> {
        val json = JsonObject()
            .apply {
                addProperty("body", body)
            }
        val requestBody = getJsonRequestBody(json.toString())

        return apiServices.postComment(accessToken.bearerToken, postId, requestBody)
            .map { CommentDTO.toDomain(it.data) }
    }
}
