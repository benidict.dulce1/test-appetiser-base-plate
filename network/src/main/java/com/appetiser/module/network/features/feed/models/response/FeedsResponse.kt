package com.appetiser.module.network.features.feed.models.response

import com.appetiser.module.network.base.response.PagingMeta
import com.appetiser.module.network.features.feed.models.FeedDTO

data class FeedsResponse(
    val data: List<FeedDTO>,
    val meta: PagingMeta
)
