package com.appetiser.module.network.features.user.models

import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.MediaFile
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.network.features.auth.models.response.AuthDataResponse
import com.google.gson.annotations.SerializedName

class UserDTO(
    @field:SerializedName("phone_number")
    val phoneNumber: String? = "",
    @field:SerializedName("email_verified")
    val emailVerified: Boolean = false,
    @field:SerializedName("phone_number_verified")
    val phoneNumberVerified: Boolean = false,
    val verified: Boolean = false,
    val email: String? = ""
) : BaseUserDTO() {
    companion object {
        fun toDomain(user: UserDTO?): User {
            return if (user == null) {
                User.empty()
            } else {
                with(user) {
                    User(
                        phoneNumber = phoneNumber.orEmpty(),
                        emailVerified = emailVerified,
                        phoneNumberVerified = phoneNumberVerified,
                        verified = verified,
                        email = email.orEmpty(),
                        lastName = lastName.orEmpty(),
                        firstName = firstName.orEmpty(),
                        fullName = fullName.orEmpty(),
                        avatarPermanentUrl = avatarPermanentUrl.orEmpty(),
                        avatarPermanentThumbUrl = avatarPermanentThumbUrl.orEmpty(),
                        id = id,
                        avatar = if (user.avatar != null) {
                            MediaFileDTO.toDomain(user.avatar)
                        } else {
                            MediaFile.empty()
                        },
                        description = description.orEmpty(),
                        birthDate = birthDate.orEmpty()
                    )
                }
            }
        }

        fun mapAuthDataResponse(from: AuthDataResponse): Pair<User, AccessToken> {
            val user = from.data.user
            val userData = from.data

            return Pair(
                toDomain(user),
                AccessToken(
                    token = userData.accessToken,
                    refresh = "",
                    tokenType = userData.tokenType.orEmpty(),
                    expiresIn = userData.expiresIn.orEmpty()
                )
            )
        }
    }
}
