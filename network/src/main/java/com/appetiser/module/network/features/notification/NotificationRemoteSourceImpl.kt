package com.appetiser.module.network.features.notification

import com.appetiser.module.domain.models.notification.Notification
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.notification.models.NotificationDTO
import com.google.gson.JsonObject
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class NotificationRemoteSourceImpl @Inject constructor(
    private val apiServices: BaseplateApiServices
) : BaseRemoteSource(), NotificationRemoteSource {

    override fun registerDeviceToken(
        accessToken: AccessToken,
        deviceToken: String,
        deviceId: String
    ): Single<Boolean> {
        val json = JsonObject()
            .apply {
                addProperty("device_token", deviceToken)
                addProperty("device_id", deviceId)
            }
        val requestBody = getJsonRequestBody(json.toString())

        return apiServices.registerToken(accessToken.bearerToken, requestBody)
            .map { it.success }
    }

    override fun unRegisterDeviceToken(accessToken: AccessToken, deviceToken: String, deviceId: String): Single<Boolean> {
        val json = JsonObject()
            .apply {
                addProperty("device_token", deviceToken)
                addProperty("device_id", deviceId)
            }
        val requestBody = getJsonRequestBody(json.toString())

        return apiServices.unRegisterToken(accessToken.bearerToken, requestBody)
            .map { it.success }
    }

    override fun getNotifications(accessToken: AccessToken, type: String): Observable<List<Notification>> {
        val include = "notifiable.avatar,actor.avatar"

        return apiServices.getNotifications(accessToken.bearerToken, type, include)
            .flatMapIterable { it.data }
            .map { NotificationDTO.toDomain(it) }
            .toList()
            .toObservable()
    }
}
