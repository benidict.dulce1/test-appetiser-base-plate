package com.appetiser.module.network.features.payouts

import com.google.gson.annotations.SerializedName

data class ExternalAccountRequest(
    @Transient val id: String,
    @SerializedName("account_holder_name") val accountHolderName: String
)
