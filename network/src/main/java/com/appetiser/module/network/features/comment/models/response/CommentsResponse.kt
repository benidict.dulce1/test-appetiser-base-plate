package com.appetiser.module.network.features.comment.models.response

import com.appetiser.module.network.base.response.PagingMeta
import com.appetiser.module.network.features.comment.models.CommentDTO

data class CommentsResponse(
    val data: List<CommentDTO>,
    val meta: PagingMeta
)
