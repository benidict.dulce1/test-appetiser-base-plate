package com.appetiser.module.network.features.payouts

import com.google.gson.annotations.SerializedName

data class PayoutDetailsRequest(
    @SerializedName("external_account") val externalAccount: ExternalAccount? = null,
    val individual: Individual
) {
    data class ExternalAccount(
        val `object`: String = "bank_account",
        val country: String? = null,
        val currency: String = "AUD",
        @SerializedName("account_holder_name") val accountHolderName: String,
        @SerializedName("account_holder_type") val accountHolderType: String = "individual",
        @SerializedName("routing_number") val routingNumber: String? = null,
        @SerializedName("account_number") val accountNumber: String? = null
    )

    data class Individual(
        @SerializedName("first_name") val firstName: String,
        @SerializedName("last_name") val lastName: String,
        val dob: DateOfBirth,
        val address: Address,
        val verification: Verification? = null
    ) {
        data class DateOfBirth(
            val day: Int,
            val month: Int,
            val year: Int
        )
    }

    data class Address(
        val city: String,
        val line1: String,
        @SerializedName("postal_code") val postCode: String,
        val state: String,
        val country: String
    )

    data class Verification(val document: Document) {
        data class Document(
            val back: String,
            val front: String
        )
    }
}
