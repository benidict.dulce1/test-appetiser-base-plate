package com.appetiser.module.network.features.comment.models

import com.appetiser.module.domain.models.feed.comment.Comment
import com.appetiser.module.network.features.user.models.UserDTO

data class CommentDTO(
    val id: Long? = 0,
    val body: String? = "",
    val author_id: Long? = 0,
    val created_at: String? = "",
    val updated_at: String? = "",
    val author: UserDTO? = null
) {
    companion object {
        fun toDomain(comment: CommentDTO): Comment {
            return with(comment) {
                Comment(
                    id = id ?: -1,
                    body = body.orEmpty(),
                    authorId = author_id ?: 0,
                    createdAt = created_at.orEmpty(),
                    updatedAt = updated_at.orEmpty(),
                    author = UserDTO.toDomain(author)
                )
            }
        }
    }
}
