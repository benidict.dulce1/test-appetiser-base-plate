package com.appetiser.module.network.features.feed

import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.feed.Feed
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.ext.convertAttachmentsToMultiPartBody
import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.feed.models.FeedDTO
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import javax.inject.Inject

class FeedRemoteSourceImpl @Inject constructor(
    private val apiServices: BaseplateApiServices
) : BaseRemoteSource(), FeedRemoteSource {

    override fun getFeeds(accessToken: AccessToken, page: Int): Single<Paging<Feed>> {
        val query = hashMapOf<String, Any>()
        query["include[0]"] = "photo"
        query["include[1]"] = "author"
        query["include[2]"] = "favorites_count"
        query["include[3]"] = "comments_count"
        query["page"] = page

        return apiServices.getFeeds(accessToken.bearerToken, query)
            .map { response ->
                Paging(
                    response.data.map {
                        FeedDTO.toDomain(it)
                    },
                    if (response.meta.currentPage >= response.meta.lastPage) {
                        null
                    } else {
                        response.meta.currentPage + 1
                    }
                )
            }
    }

    override fun getFeedDetails(accessToken: AccessToken, id: Long): Single<Feed> {
        return apiServices.getFeedDetails(accessToken.bearerToken, id)
            .map { it.data }
            .map { FeedDTO.toDomain(it) }
            .firstOrError()
    }

    override fun favorite(accessToken: AccessToken, id: Long): Single<Boolean> {
        return apiServices.markFavorite(accessToken.bearerToken, id)
            .map { it.success }
    }

    override fun unFavorite(accessToken: AccessToken, id: Long): Single<Boolean> {
        return apiServices.unMarkFavorite(accessToken.bearerToken, id)
            .map { it.success }
    }

    override fun postFeed(accessToken: AccessToken, body: String, filePath: String): Single<Feed> {
        val imageFile = File(filePath)
        val requestImageBody =
            MultipartBody.Part.createFormData(
                "photo",
                "image_${imageFile.name}",
                imageFile.asRequestBody("image/*".toMediaTypeOrNull())
            )

        val requestBody: RequestBody = body.toRequestBody("multipart/form-data".toMediaTypeOrNull())

        return apiServices
            .postFeed(
                accessToken.bearerToken,
                requestImageBody,
                requestBody
            )
            .map { it.data }
    }

    override fun deleteFeed(accessToken: AccessToken, id: Long): Single<Boolean> {
        return apiServices.deleteAccount(accessToken.bearerToken, id)
            .map { it.success }
    }

    override fun reportFeed(
        accessToken: AccessToken,
        postId: Long,
        reasonId: Long,
        description: String,
        attachmentPathList: List<String>
    ): Single<Boolean> {
        return if (attachmentPathList.isEmpty()) {
            apiServices
                .reportFeed(
                    accessToken.bearerToken,
                    postId,
                    reasonId,
                    description
                )
                .map {
                    it.success
                }
        } else {
            apiServices
                .reportFeed(
                    accessToken.bearerToken,
                    postId,
                    reasonId,
                    description,
                    attachmentPathList.convertAttachmentsToMultiPartBody()
                )
                .map {
                    it.success
                }
        }
    }
}
