package com.appetiser.module.network.features.miscellaneous.models

import com.appetiser.module.domain.models.miscellaneous.ReportCategory

class ReportCategoryDTO(
    val id: Int,
    val label: String
) {
    companion object {
        fun toDomain(reportCategoryDTO: ReportCategoryDTO): ReportCategory {
            return with(reportCategoryDTO) {
                ReportCategory(
                    id = id,
                    label = label
                )
            }
        }
    }
}
