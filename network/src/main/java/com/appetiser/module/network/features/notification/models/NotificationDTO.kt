package com.appetiser.module.network.features.notification.models

import com.appetiser.module.domain.models.notification.Notification
import com.appetiser.module.domain.models.notification.NotificationType
import com.appetiser.module.network.features.user.models.UserDTO

data class NotificationDTO(
    val id: String? = "",
    val type: String? = "",
    val notifiable_id: Long? = 0,
    val actor_id: Long? = 0,
    val message: String? = "",
    val read_at: String? = "",
    val read: Boolean? = false,
    val created_at: String? = "",
    val notifiable: UserDTO? = null,
    val actor: UserDTO? = null

) {
    companion object {
        fun toDomain(mediaFileDTO: NotificationDTO): Notification {
            return with(mediaFileDTO) {
                Notification(
                    id = id.orEmpty(),
                    type = toNotificationType(type.orEmpty()) ?: NotificationType.LIKE,
                    notifiableId = notifiable_id ?: 0,
                    actorId = actor_id ?: 0,
                    message = message.orEmpty(),
                    readAt = read_at.orEmpty(),
                    read = read ?: false,
                    created_at = created_at.orEmpty(),
                    notifiable = UserDTO.toDomain(notifiable),
                    actor = UserDTO.toDomain(actor)
                )
            }
        }

        private fun toNotificationType(type: String): NotificationType? {
            return NotificationType.values().singleOrNull { it.type == type }
        }
    }
}

enum class NotificationGroups(val type: String) {
    TODAY("today"), THIS_WEEK("this-week")
}
