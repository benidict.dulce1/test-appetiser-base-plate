package com.appetiser.module.network.features.user

import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.MediaFile
import com.appetiser.module.domain.models.user.User
import io.reactivex.Single

interface UserRemoteSource {
    fun getUser(
        accessToken: AccessToken
    ): Single<User>

    fun updateUser(
        accessToken: AccessToken,
        user: User
    ): Single<User>

    fun uploadPhoto(
        accessToken: AccessToken,
        filePath: String
    ): Single<MediaFile>
}
