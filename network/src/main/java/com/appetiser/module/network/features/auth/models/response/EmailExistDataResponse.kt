package com.appetiser.module.network.features.auth.models.response

import com.google.gson.annotations.SerializedName

data class EmailExistDataResponse(@field:SerializedName("email_exists") val isEmailExists: Boolean)
