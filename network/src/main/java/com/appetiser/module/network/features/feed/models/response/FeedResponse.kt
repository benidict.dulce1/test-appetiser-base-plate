package com.appetiser.module.network.features.feed.models.response

import com.appetiser.module.network.features.feed.models.FeedDTO

data class FeedResponse(val data: FeedDTO)
