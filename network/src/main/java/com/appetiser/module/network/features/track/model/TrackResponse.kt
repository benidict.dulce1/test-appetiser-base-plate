package com.appetiser.module.network.features.track.model

import com.google.gson.annotations.SerializedName

data class TrackResponse(
    @SerializedName("dataId")
    val dataId: Int = 0,
    @SerializedName("artistId")
    val artistId: String?= null,
    @SerializedName("wrapperType")
    val wrapperType: String?= null,
    @SerializedName("kind")
    val kind: String?= null,
    @SerializedName("collectionId")
    val collectionId: String?= null,
    @SerializedName("trackId")
    val trackId: String?= null,
    @SerializedName("artistName")
    val artistName: String?= null,
    @SerializedName("collectionName")
    val collectionName: String?= null,
    @SerializedName("trackName")
    val trackName: String?= null,
    @SerializedName("collectionCensoredName")
    val collectionCensoredName: String?= null,
    @SerializedName("trackCensoredName")
    val trackCensoredName: String?= null,
    @SerializedName("collectionTrackId")
    val collectionTrackId: String?= null,
    @SerializedName("collectionTrackViewUrl")
    val collectionTrackViewUrl: String?= null,
    @SerializedName("collectionViewUrl")
    val collectionViewUrl: String?= null,
    @SerializedName("trackViewUrl")
    val trackViewUrl: String?= null,
    @SerializedName("previewUrl")
    val previewUrl: String?= null,
    @SerializedName("artworkUrl30")
    val artworkUrl30: String?= null,
    @SerializedName("artworkUrl60")
    val artworkUrl60: String?= null,
    @SerializedName("artworkUrl100")
    val artworkUrl100: String?= null,
    @SerializedName("collectionPrice")
    val collectionPrice: String?= null,
    @SerializedName("trackPrice")
    val trackPrice: String?= null,
    @SerializedName("trackRentalPrice")
    val trackRentalPrice: String?= null,
    @SerializedName("collectionHdPrice")
    val collectionHdPrice: String?= null,
    @SerializedName("trackHdPrice")
    val trackHdPrice: String?= null,
    @SerializedName("trackHdRentalPrice")
    val trackHdRentalPrice: String?= null,
    @SerializedName("releaseDate")
    val releaseDate: String?= null,
    @SerializedName("collectionExplicitness")
    val collectionExplicitness: String?= null,
    @SerializedName("trackExplicitness")
    val trackExplicitness: String?= null,
    @SerializedName("discCount")
    val discCount: String?= null,
    @SerializedName("discNumber")
    val discNumber: String?= null,
    @SerializedName("trackCount")
    val trackCount: String?= null,
    @SerializedName("trackNumber")
    val trackNumber: String?= null,
    @SerializedName("trackTimeMillis")
    val trackTimeMillis: String?= null,
    @SerializedName("country")
    val country: String?= null,
    @SerializedName("currency")
    val currency: String?= null,
    @SerializedName("primaryGenreName")
    val primaryGenreName: String?= null,
    @SerializedName("contentAdvisoryRating")
    val contentAdvisoryRating: String?= null,
    @SerializedName("shortDescription")
    val shortDescription: String?= null,
    @SerializedName("longDescription")
    val longDescription: String?= null,
    @SerializedName("hasITunesExtras")
    val hasITunesExtras: String?= null,
)