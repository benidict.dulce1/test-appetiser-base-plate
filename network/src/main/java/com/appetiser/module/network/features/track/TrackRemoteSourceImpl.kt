package com.appetiser.module.network.features.track

import com.appetiser.module.domain.models.track.Track
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.track.mapper.remoteToDomain
import io.reactivex.Single
import javax.inject.Inject

class TrackRemoteSourceImpl @Inject constructor(
    private val apiServices: BaseplateApiServices
): BaseRemoteSource(), TrackRemoteSource{
    override fun loadTrackList(): Single<List<Track>>
        = apiServices.loadTrackList().flatMap {
        it.results.remoteToDomain()
    }

}