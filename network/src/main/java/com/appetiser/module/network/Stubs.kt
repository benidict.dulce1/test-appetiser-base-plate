package com.appetiser.module.network

import com.appetiser.module.domain.models.auth.CountryCode

object Stubs {
    val countries =
        mutableListOf(
            CountryCode(1, "PH", "63", "/images/flags/PH.png"),
            CountryCode(2, "AU", "61", "/images/flags/AU.png")
        )
}
