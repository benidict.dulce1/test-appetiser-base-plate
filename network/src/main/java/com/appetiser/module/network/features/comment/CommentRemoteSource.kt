package com.appetiser.module.network.features.comment

import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.feed.comment.Comment
import com.appetiser.module.domain.models.token.AccessToken
import io.reactivex.Single

interface CommentRemoteSource {

    fun getComments(accessToken: AccessToken, feedId: Long, page: Int): Single<Paging<Comment>>

    fun getCommentDetails(accessToken: AccessToken, commentId: Long): Single<Comment>

    fun postComment(accessToken: AccessToken, postId: Long, body: String): Single<Comment>
}
