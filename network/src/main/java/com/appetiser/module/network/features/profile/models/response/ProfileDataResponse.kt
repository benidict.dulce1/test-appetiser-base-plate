package com.appetiser.module.network.features.profile.models.response

import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.user.models.UserDTO

data class ProfileDataResponse(val data: UserDTO) : BaseResponse()
