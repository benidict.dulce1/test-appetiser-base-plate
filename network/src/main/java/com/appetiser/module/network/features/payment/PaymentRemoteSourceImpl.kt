package com.appetiser.module.network.features.payment

import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.features.BaseplateApiServices
import com.google.gson.Gson
import io.reactivex.Single
import javax.inject.Inject

class PaymentRemoteSourceImpl @Inject constructor(
    private val apiService: BaseplateApiServices,
    private val gson: Gson
) : BaseRemoteSource(), PaymentRemoteSource {
    override fun createEphemeralKey(
        accessToken: AccessToken
    ): Single<String> {
        return apiService
            .createEphemeralKey(
                accessToken.bearerToken
            )
            .map {
                gson.toJson(it.data)
            }
    }
}
