package com.appetiser.module.network.features.feed

import com.appetiser.module.domain.models.Paging
import com.appetiser.module.domain.models.feed.Feed
import com.appetiser.module.domain.models.token.AccessToken
import io.reactivex.Single

interface FeedRemoteSource {

    fun getFeeds(accessToken: AccessToken, page: Int): Single<Paging<Feed>>

    fun getFeedDetails(accessToken: AccessToken, id: Long): Single<Feed>

    fun favorite(accessToken: AccessToken, id: Long): Single<Boolean>

    fun unFavorite(accessToken: AccessToken, id: Long): Single<Boolean>

    fun postFeed(accessToken: AccessToken, body: String, filePath: String): Single<Feed>

    fun deleteFeed(accessToken: AccessToken, id: Long): Single<Boolean>

    fun reportFeed(
        accessToken: AccessToken,
        postId: Long,
        reasonId: Long,
        description: String,
        attachmentPathList: List<String>
    ): Single<Boolean>
}
