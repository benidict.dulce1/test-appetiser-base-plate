package com.appetiser.module.network.features.track.model

import com.google.gson.annotations.SerializedName

data class CommonResponse<T>(
    @SerializedName("resultCount")
    val resultCount: Int = 0,
    @SerializedName("results")
    val results: T
)