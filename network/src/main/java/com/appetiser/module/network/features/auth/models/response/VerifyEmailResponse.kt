package com.appetiser.module.network.features.auth.models.response

import com.appetiser.module.network.base.response.BaseResponse
import com.appetiser.module.network.features.user.models.UserDTO
import com.google.gson.annotations.SerializedName

data class VerifyEmailResponse(
    @SerializedName("data") val user: UserDTO
) : BaseResponse()
