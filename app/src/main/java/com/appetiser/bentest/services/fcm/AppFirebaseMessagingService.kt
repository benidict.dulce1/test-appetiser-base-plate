package com.appetiser.bentest.services.fcm

import com.appetiser.module.data.features.notification.NotificationRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.notification.fcm.BaseplateFirebaseMessagingService
import dagger.android.AndroidInjection
import io.reactivex.Completable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

class AppFirebaseMessagingService() : BaseplateFirebaseMessagingService() {

    @Inject
    lateinit var repository: NotificationRepository

    @Inject
    lateinit var sessionRepository: SessionRepository

    @Inject
    lateinit var resourceManager: ResourceManager

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onMessageReceived(data: Map<String, String>) = Unit

    override fun onNewToken(token: String) {
        super.onNewToken(token)

        sessionRepository
            .getSession()
            .flatMapCompletable { session ->
                // Only register device token if there's logged in user.
                if (session.user.isNotEmpty()) {
                    repository
                        .registerDeviceToken(
                            token,
                            resourceManager.getDeviceId()
                        )
                        .ignoreElement()
                } else {
                    Completable.complete()
                }
            }
            .subscribeBy(
                onComplete = {},
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }
}
