package com.appetiser.bentest.utils

import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.webkit.MimeTypeMap
import com.appetiser.bentest.BuildConfig
import com.bumptech.glide.Glide
import io.reactivex.Single
import timber.log.Timber
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import java.util.concurrent.ExecutionException

object FileUtils {

    fun getUrlForResource(resourceId: Int): String {
        return Uri.parse("android.resource://" + BuildConfig.APPLICATION_ID + "/" + resourceId).toString()
    }

    fun saveBitmap(context: Context, bitmap: Bitmap, filename: String, recycle: Boolean): Uri {
        return saveBitmap(context, bitmap, filename, 100, recycle)
    }

    fun saveBitmap(context: Context, bitmap: Bitmap, filename: String?, quality: Int, recycle: Boolean): Uri {
        val fileOutputStream: FileOutputStream
        val photo = File(context.filesDir.path, filename!!)
        val mimeType = getMimeType(context, Uri.parse(photo.absolutePath))
        Timber.d("mimeType: %s", mimeType)

        try {
            fileOutputStream = FileOutputStream(photo)

            val bos = BufferedOutputStream(fileOutputStream)
            var compressFormat: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG

            if (!mimeType.isEmpty()) {
                if (mimeType.toLowerCase().contains("png")) {
                    compressFormat = Bitmap.CompressFormat.PNG
                } else if (mimeType.toLowerCase().contains("webp")) {
                    compressFormat = Bitmap.CompressFormat.WEBP
                }
            }

            bitmap.compress(compressFormat, quality, bos)
            Timber.d("photo saved: %s", photo.toString())

            try {
                bos.flush()
                bos.close()
                fileOutputStream.flush()
                fileOutputStream.close()
            } catch (e: IOException) {
                Timber.e(e, "Error in cleaning up streams: %s", filename)
            }
        } catch (e: IOException) {
            Timber.e(e, "IOException in saving bitmap: %s", filename)
        } catch (e: Exception) {
            Timber.e(e, "Error in saving bitmap: %s", filename)
        }

        if (recycle) {
            bitmap.recycle()
        }

        return Uri.parse("file:" + photo.absolutePath)
    }

    fun deleteFile(fileUri: Uri): Boolean {
        val path = fileUri.toString().replaceFirst("[^:]+:".toRegex(), "")
        var success = false

        Timber.d("Deleting file: %s", path)

        try {
            val fileToDelete = File(path)
            success = fileToDelete.delete()
        } catch (e: Exception) {
            Timber.e(e, "Error in deleting file: %s", path)
        }

        if (success) {
            Timber.d("File successfully deleted: %s", path)
        } else {
            Timber.d("Unable to delete file: %s", path)
        }

        return success
    }

    fun isFileExists(fileUri: Uri): Boolean {
        val path = fileUri.toString().replaceFirst("[^:]+:".toRegex(), "")
        return File(path).exists()
    }

    fun scaleDownBitmap(bitmap: Bitmap, maxImageSize: Float, filter: Boolean): Bitmap {
        if (bitmap.width < maxImageSize && bitmap.height < maxImageSize) {
            return bitmap
        }

        val ratio = Math.min(maxImageSize / bitmap.width, maxImageSize / bitmap.height)
        val width = Math.round(ratio * bitmap.width)
        val height = Math.round(ratio * bitmap.height)

        return Bitmap.createScaledBitmap(bitmap, width, height, filter)
    }

    @Throws(ExecutionException::class, InterruptedException::class)
    fun resizePhotos(context: Context, paths: List<String>): Single<List<Uri>> {
        return Single.create { emitter ->
            val imageUris = ArrayList<Uri>()

            for (path in paths) {
                Timber.d("resizePhotos start --> path: %s", path)
                val photoPathUri = Uri.parse(path)
                val futureTarget = Glide.with(context)
                    .asBitmap()
                    .load(path)
                    .submit()

                val bitmap = futureTarget.get()
                val scaleDown = scaleDownBitmap(bitmap, PHOTO_MAX_DIMENSION, true)
                val uri = saveBitmap(
                    context,
                    scaleDown,
                    photoPathUri.lastPathSegment,
                    PHOTO_COMPRESSION_QUALITY,
                    false
                )
                imageUris.add(uri)
                Timber.d("resizePhotos end --> path: %s", path)
                Glide.with(context).clear(futureTarget)
            }

            emitter.onSuccess(imageUris)
        }
    }

    fun createFileFromCacheDir(context: Context): File {
        val folder = File(context.cacheDir, "/tempfiles/")

        if (!folder.exists()) {
            if (!folder.mkdir()) {
                Timber.e(Throwable("Cannot create a directory!"))
            } else {
                folder.mkdirs()
            }
        }

        return File(folder, "${UUID.randomUUID()}.jpg")
    }

    private fun getMimeType(context: Context, uri: Uri): String {
        var mimeType: String?

        if (ContentResolver.SCHEME_CONTENT == uri.scheme) {
            val cr = context.contentResolver
            mimeType = cr.getType(uri)
        } else {
            val fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri.toString())
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                fileExtension.toLowerCase()
            )

            if (mimeType.isNullOrEmpty()) {
                mimeType = if ("json" == fileExtension) {
                    "application/json"
                } else {
                    "text/$fileExtension"
                }
            }
        }

        return mimeType!!
    }
}
