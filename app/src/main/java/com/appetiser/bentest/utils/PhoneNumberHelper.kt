package com.appetiser.bentest.utils

import android.content.Context
import com.appetiser.bentest.di.scopes.ApplicationContext
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class PhoneNumberHelper @Inject constructor(
    @ApplicationContext private val context: Context
) {
    /**
     * Returns phone number in international format.
     *
     * @param phoneNumber phone number to format
     * @param countryNameCode country name code. (i.e. "au", "ph", "cn")
     */
    fun getInternationalFormattedPhoneNumber(phoneNumber: String, countryNameCode: String): String {
        val phoneNumberUtil: PhoneNumberUtil = PhoneNumberUtil.getInstance()
        val numberProto: Phonenumber.PhoneNumber = phoneNumberUtil.parse(
            phoneNumber,
            countryNameCode.toUpperCase(Locale.getDefault())
        )
        return try {
            phoneNumberUtil
                .format(
                    numberProto,
                    PhoneNumberUtil.PhoneNumberFormat.E164
                )
        } catch (e: Exception) {
            Timber.e(e)
            phoneNumber
        }
    }

    /**
     * Validates phone number format as per [country].
     */
    fun validatePhoneNumber(phoneNumber: String, country: String): Boolean {
        return try {
            val phoneNumberUtils = PhoneNumberUtil.getInstance()
            val numberProto =
                phoneNumberUtils
                    .parse(
                        phoneNumber,
                        country.toUpperCase(Locale.getDefault())
                    )
            phoneNumberUtils.isValidNumber(numberProto)
        } catch (e: Exception) {
            return false
        }
    }
}
