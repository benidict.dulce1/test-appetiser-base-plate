package com.appetiser.bentest.utils

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

object DateUtils {
    /**
     * Formats date to short date format based on device's locale.
     */
    fun formatDate(date: LocalDate): String {
        val formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)
        return formatter.format(date)
    }

    /**
     * Parses date of birth of this format "yyyy-mm-d" to [LocalDate].
     */
    fun parseBirthDate(birthDate: String): LocalDate {
        require(birthDate.isNotEmpty()) {
            "Required parameter birthDate was empty."
        }

        val split = birthDate.split("-")

        return LocalDate
            .of(
                split[0].toInt(),
                split[1].toInt(),
                split[2].toInt()
            )
    }
}
