package com.appetiser.bentest.utils

const val PAGE_STEP_1_POSITION = 0
const val PAGE_STEP_2_POSITION = 1
const val PAGE_STEP_3_POSITION = 2
const val PAGE_STEP_4_POSITION = 3
const val PAGE_INITIAL = PAGE_STEP_1_POSITION
const val PHOTO_MAX_DIMENSION = 1080f
const val PHOTO_COMPRESSION_QUALITY = 100

const val DEFAULT_DOB_SUBTRACTION = 18L
