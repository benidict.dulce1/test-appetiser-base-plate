package com.appetiser.bentest.features.account.changepassword.newpassword

import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.appetiser.module.domain.utils.PASSWORD_MIN_LENGTH
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class ChangeNewPasswordViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private lateinit var oldPassword: String
    private lateinit var verificationToken: String

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    private val _state by lazy {
        PublishSubject.create<ChangeNewPasswordState>()
    }

    val state: Observable<ChangeNewPasswordState> = _state

    fun setOldPasswordAndToken(oldPassword: String, verificationToken: String) {
        this.oldPassword = oldPassword
        this.verificationToken = verificationToken
    }

    fun onPasswordTextChanged(password: String) {
        if (validatePasswordNotEmpty(password)) {
            _state
                .onNext(
                    ChangeNewPasswordState.EnableButton
                )
        } else {
            _state
                .onNext(
                    ChangeNewPasswordState.DisableButton
                )
        }
    }

    fun changePassword(password: String) {
        if (!validatePassword(password)) return

        repository
            .changePassword(oldPassword, password, password)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state
                    .onNext(
                        ChangeNewPasswordState.ShowProgressLoading
                    )
            }
            .doOnSuccess {
                _state
                    .onNext(
                        ChangeNewPasswordState.HideProgressLoading
                    )
            }
            .doOnError {
                _state
                    .onNext(
                        ChangeNewPasswordState.HideProgressLoading
                    )
            }
            .subscribeBy(
                onSuccess = {
                    _state.onNext(ChangeNewPasswordState.Success)
                },
                onError = { error ->
                    Timber.e(error)
                    if (error is HttpException && error.code() == 400) {
                        _state
                            .onNext(
                                ChangeNewPasswordState.InvalidPassword
                            )
                    } else {
                        showGenericError()
                    }
                }
            )
            .addTo(disposables)
    }

    private fun showGenericError() {
        _state
            .onNext(
                ChangeNewPasswordState.Error(
                    resourceManager
                        .getString(
                            R.string.generic_error_short
                        )
                )
            )
    }

    private fun validatePassword(password: String): Boolean {
        if (!validatePasswordNotEmpty(password)) return false

        if (password.length < PASSWORD_MIN_LENGTH) {
            _state
                .onNext(
                    ChangeNewPasswordState.PasswordBelowMinLength
                )
            return false
        }

        if (password.length > PASSWORD_MAX_LENGTH) {
            _state
                .onNext(
                    ChangeNewPasswordState.PasswordExceedsMaxLength
                )
            return false
        }
        return true
    }

    private fun validatePasswordNotEmpty(password: String): Boolean {
        return password.isNotEmpty() && password.length >= PASSWORD_MIN_LENGTH
    }
}
