package com.appetiser.bentest.features.auth.walkthrough

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.bentest.R
import com.appetiser.bentest.utils.PAGE_STEP_1_POSITION
import com.appetiser.bentest.utils.PAGE_STEP_2_POSITION
import com.appetiser.bentest.utils.PAGE_STEP_3_POSITION
import com.appetiser.bentest.utils.PAGE_STEP_4_POSITION

class WalkthroughAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val VIEW_TYPE_STEP_1 = 1
        private const val VIEW_TYPE_STEP_2 = 2
        private const val VIEW_TYPE_STEP_3 = 3
        private const val VIEW_TYPE_STEP_4 = 4
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            VIEW_TYPE_STEP_1 -> {
                Walkthrough1ViewHolder(
                    inflater
                        .inflate(
                            R.layout.item_walkthrough_1,
                            parent,
                            false
                        )
                )
            }
            VIEW_TYPE_STEP_2 -> {
                Walkthrough2ViewHolder(
                    inflater
                        .inflate(
                            R.layout.item_walkthrough_2,
                            parent,
                            false
                        )
                )
            }
            VIEW_TYPE_STEP_3 -> {
                Walkthrough3ViewHolder(
                    inflater
                        .inflate(
                            R.layout.item_walkthrough_3,
                            parent,
                            false
                        )
                )
            }
            VIEW_TYPE_STEP_4 -> {
                Walkthrough4ViewHolder(
                    inflater
                        .inflate(
                            R.layout.item_walkthrough_4,
                            parent,
                            false
                        )
                )
            }
            else -> throw IllegalArgumentException("Invalid viewType $viewType!")
        }
    }

    override fun getItemViewType(position: Int): Int =
        when (position) {
            PAGE_STEP_1_POSITION -> VIEW_TYPE_STEP_1
            PAGE_STEP_2_POSITION -> VIEW_TYPE_STEP_2
            PAGE_STEP_3_POSITION -> VIEW_TYPE_STEP_3
            PAGE_STEP_4_POSITION -> VIEW_TYPE_STEP_4
            else -> throw IllegalArgumentException("Invalid position $position!")
        }

    override fun getItemCount(): Int = 4

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = Unit

    class Walkthrough1ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    class Walkthrough2ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    class Walkthrough3ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    class Walkthrough4ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
