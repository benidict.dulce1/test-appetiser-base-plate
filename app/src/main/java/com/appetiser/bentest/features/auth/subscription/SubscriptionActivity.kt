package com.appetiser.bentest.features.auth.subscription

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.method.LinkMovementMethod
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivitySubscriptionBinding
import com.appetiser.bentest.features.main.MainActivity
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.spannableString
import com.appetiser.module.common.toast
import io.reactivex.rxkotlin.addTo

class SubscriptionActivity : BaseViewModelActivity<ActivitySubscriptionBinding, SubscriptionViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, SubscriptionActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_subscription

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupViews()
    }

    private fun setupViews() {
        setupButtons()
        setupTermsAndPrivacy()
    }

    private fun setupButtons() {
        binding
            .btnContinue
            .ninjaTap {
                startMainActivity()
            }
            .addTo(disposables)

        binding
            .btnClose
            .ninjaTap {
                startMainActivity()
            }
    }

    private fun startMainActivity() {
        MainActivity.openActivity(this)
        finish()
    }

    private fun setupTermsAndPrivacy() {
        binding.txtTermsOfService.movementMethod = LinkMovementMethod.getInstance()
        binding.txtTermsOfService.text = getString(R.string.terms_of_service)
            .spannableString(
                this,
                null,
                R.font.inter_regular,
                Color.BLUE,
                getString(R.string.terms_of_service),
                clickable = {
                    toast("Clicked $it")
                }
            )

        binding.txtPrivacyPolicy.movementMethod = LinkMovementMethod.getInstance()
        binding.txtPrivacyPolicy.text = getString(R.string.privacy_policy)
            .spannableString(
                this,
                null,
                R.font.inter_regular,
                Color.BLUE,
                getString(R.string.privacy_policy),
                clickable = {
                    toast("Clicked $it")
                }
            )
    }
}
