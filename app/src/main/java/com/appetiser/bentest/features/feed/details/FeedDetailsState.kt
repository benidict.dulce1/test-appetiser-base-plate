package com.appetiser.bentest.features.feed.details

import androidx.paging.PagingData
import com.appetiser.module.domain.models.feed.Feed
import com.appetiser.module.domain.models.feed.comment.Comment

sealed class FeedDetailsState {

    data class GetFeed(val item: Feed) : FeedDetailsState()

    data class UpdateFeed(val item: Feed) : FeedDetailsState()

    data class Error(val throwable: Throwable) : FeedDetailsState()
}

sealed class CommentState {

    data class GetComments(val items: PagingData<Comment>) : CommentState()

    data class Error(val throwable: Throwable) : CommentState()

    object ShowLastCommentItem : CommentState()

    object ShowComment : CommentState()

    object AddingComment : CommentState()

    data class AddCommentSuccess(val item: Comment) : CommentState()
}
