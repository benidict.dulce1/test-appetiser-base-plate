package com.appetiser.bentest.features.auth

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.appetiser.bentest.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes
import com.google.android.gms.common.api.ApiException
import timber.log.Timber

/**
 * Activity to receive `onActivityResult`.
 */
class GoogleSignInManager(private val activity: Activity) {

    companion object {
        const val REQUEST_CODE_GOOGLE_SIGN_IN = 1000

        fun isSignedIn(context: Context) = GoogleSignIn.getLastSignedInAccount(context) != null
    }

    private val googleSignInClient by lazy {
        val googleSigninOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(activity.getString(R.string.google_web_client_id))
            .requestEmail()
            .build()

        GoogleSignIn.getClient(activity, googleSigninOptions)
    }

    private var onSuccessListener: ((GoogleSignInAccount) -> Unit)? = null
    private var onCancelListener: (() -> Unit)? = null
    private var onErrorListener: ((ApiException) -> Unit)? = null

    fun isSignedIn() = Companion.isSignedIn(activity)

    /**
     * Calls google sign in client.
     */
    fun signIn() {
        activity.startActivityForResult(
            googleSignInClient.signInIntent,
            REQUEST_CODE_GOOGLE_SIGN_IN
        )
    }

    fun signOut() {
        googleSignInClient.signOut()
    }

    /**
     * Call after activity receives result in `onActivityResult()` from [signIn].
     */
    fun sendSignInResult(data: Intent?) {
        val signedInResult = GoogleSignIn.getSignedInAccountFromIntent(data)

        try {
            val account = signedInResult.getResult(ApiException::class.java)

            onSuccessListener?.invoke(account!!)
        } catch (e: ApiException) {
            if (e.statusCode == GoogleSignInStatusCodes.SIGN_IN_CANCELLED) {
                Timber.d("Sign in canceled")
                onCancelListener?.invoke()
            } else {
                Timber.e(e)
                onErrorListener?.invoke(e)
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    /**
     * Invoked when sign in is successful.
     */
    fun setSignInSuccessListener(listener: (GoogleSignInAccount) -> Unit) {
        onSuccessListener = listener
    }

    /**
     * Invoked when sign in is cancelled.
     */
    fun getSignInCancelListener(listener: () -> Unit) {
        onCancelListener = listener
    }

    /**
     * Invoked when sign in throws an error.
     */
    fun setSignInErrorListener(listener: (ApiException) -> Unit) {
        onErrorListener = listener
    }

    fun clearListeners() {
        onSuccessListener = null
        onErrorListener = null
    }
}
