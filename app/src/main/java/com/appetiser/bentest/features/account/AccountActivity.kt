package com.appetiser.bentest.features.account

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityAccountBinding
import com.appetiser.bentest.features.account.about.AboutActivity
import com.appetiser.bentest.features.account.settings.AccountSettingsActivity
import com.appetiser.bentest.features.profile.editprofile.EditProfileActivity
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.setVisible
import com.appetiser.module.common.showGenericErrorSnackBar
import com.appetiser.module.payments.features.payments.PaymentsActivity
import com.appetiser.module.payouts.PayoutsActivity
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class AccountActivity : BaseViewModelActivity<ActivityAccountBinding, AccountViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    AccountActivity::class.java
                )
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_account

    override fun canBack(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.vm = viewModel

        setupToolbar()
        setupViews()
        setupVmObservers()
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadAccount()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: AccountState) {
        when (state) {
            AccountState.ShowLoading -> {
                binding.loading setVisible true
            }
            AccountState.HideLoading -> {
                binding.loading setVisible false
            }
            is AccountState.NavigateToPaymentsScreen -> {
                PaymentsActivity
                    .openActivity(
                        this,
                        state.ephemeralKeyJson
                    )
            }
            is AccountState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun setupViews() {
        binding
            .layoutEditProfile
            .ninjaTap {
                EditProfileActivity.openActivity(this)
            }
            .addTo(disposables)

        binding
            .layoutPayments
            .ninjaTap {
                viewModel.onPaymentsClick()
            }
            .addTo(disposables)

        binding
            .layoutPayouts
            .ninjaTap {
                this.startActivity(Intent(this, PayoutsActivity::class.java))
            }

        binding
            .layoutAccountSettings
            .ninjaTap {
                AccountSettingsActivity.openActivity(this)
            }
            .addTo(disposables)

        binding
            .layoutAbout
            .ninjaTap {
                AboutActivity.openActivity(this)
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        setToolbarTitle(
            getString(R.string.account)
        )
    }
}
