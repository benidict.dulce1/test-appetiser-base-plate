package com.appetiser.bentest.features.account.deleteaccount

sealed class DeleteAccountPasswordState {
    object EnableButton : DeleteAccountPasswordState()

    object DisableButton : DeleteAccountPasswordState()

    object ShowLoading : DeleteAccountPasswordState()

    object HideLoading : DeleteAccountPasswordState()

    object InvalidPassword : DeleteAccountPasswordState()

    object NavigateToWalkthroughScreen : DeleteAccountPasswordState()

    object LogoutSocialLogins : DeleteAccountPasswordState()

    data class Error(val message: String) : DeleteAccountPasswordState()
}
