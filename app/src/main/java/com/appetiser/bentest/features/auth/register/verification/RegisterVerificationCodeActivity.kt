package com.appetiser.bentest.features.auth.register.verification

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityRegisterVerificationCodeBinding
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.features.auth.register.details.InputNameActivity
import com.appetiser.bentest.features.auth.register.phonenumber.PhoneNumberInputActivity
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class RegisterVerificationCodeActivity : BaseViewModelActivity<ActivityRegisterVerificationCodeBinding, RegisterVerificationViewModel>() {

    companion object {
        fun openActivity(context: Context, email: String = "", phone: String = "") {
            val intent = Intent(context, RegisterVerificationCodeActivity::class.java)
            intent.putExtra(KEY_EMAIL, email)
            intent.putExtra(KEY_PHONE, phone)
            context.startActivity(intent)
        }

        const val KEY_EMAIL = "email"
        const val KEY_PHONE = "phone"
    }

    override fun getLayoutId(): Int = R.layout.activity_register_verification_code

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val email = intent.extras?.getString(KEY_EMAIL) ?: ""
        val phone = intent.extras?.getString(KEY_PHONE) ?: ""

        check(email.isNotEmpty() || phone.isNotEmpty()) {
            "Required parameter email or phone not found!"
        }

        setupViews()
        setupToolbar()
        setupViewModels()
        observeInputViews()

        viewModel.setEmailOrPhone(email, phone)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupViews() {
        binding.inputCode.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    verifyCode()
                    true
                } else {
                    false
                }
            }
        }

        binding.noCode.ninjaTap {
            viewModel.resendToken()
        }
    }

    private fun verifyCode() {
        viewModel.sendCode(binding.inputCode.text.toString())
    }

    private fun observeInputViews() {
        binding.inputCode.textChangeEvents()
            .skipInitialValue()
            .observeOn(scheduler.ui())
            .map { it.text }
            .map {
                it.isNotEmpty() && it.length >= 5
            }
            .subscribeBy(
                onNext = {
                    if (it) {
                        viewModel.sendCode(binding.inputCode.text.toString())
                    }
                },
                onError = {
                    Timber.e(it)
                }
            ).apply { disposables.add(this) }
    }

    private fun setupViewModels() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e("Error $it")
                }
            ).addTo(disposables)
    }

    private fun handleState(state: RegisterVerificationState) {
        when (state) {
            is RegisterVerificationState.FillUsername -> {
                binding.tvEmail.text = state.username
            }
            is RegisterVerificationState.ResendCodeSuccess -> {
                toast("Request new code sent!")
            }
            is RegisterVerificationState.StartPhoneNumberInputScreen -> {
                PhoneNumberInputActivity.openActivity(this)
                finishAffinity()
            }
            is RegisterVerificationState.StartUserDetailsScreen -> {
                InputNameActivity.openActivity(this)
                finishAffinity()
            }
            is RegisterVerificationState.Error -> {
                toast(state.throwable.getThrowableError())
            }
            is RegisterVerificationState.ShowProgressLoading -> {
                toast("Sending request")
            }
            is RegisterVerificationState.HideProgressLoading -> {
            }
        }
    }
}
