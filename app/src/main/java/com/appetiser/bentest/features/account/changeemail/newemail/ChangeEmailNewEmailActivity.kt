package com.appetiser.bentest.features.account.changeemail.newemail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityChangeEmailNewEmailBinding
import com.appetiser.bentest.features.account.changeemail.verifycode.ChangeEmailVerifyCodeActivity
import com.appetiser.module.common.*
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangeEmailNewEmailActivity : BaseViewModelActivity<ActivityChangeEmailNewEmailBinding, ChangeEmailNewEmailViewModel>() {

    companion object {
        private const val EXTRA_VERIFICATION_TOKEN = "EXTRA_VERIFICATION_TOKEN"

        fun openActivity(context: Context, verificationToken: String) {
            context.startActivity(
                Intent(
                    context,
                    ChangeEmailNewEmailActivity::class.java
                ).apply {
                    putExtra(EXTRA_VERIFICATION_TOKEN, verificationToken)
                }
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_change_email_new_email

    override fun canBack(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        require(
            intent.extras != null &&
                intent.extras!!.getString(EXTRA_VERIFICATION_TOKEN, "").isNotEmpty()
        ) {
            "Required intent extra `verificationToken` is missing!"
        }

        viewModel
            .setVerificationToken(
                intent.extras!!.getString(EXTRA_VERIFICATION_TOKEN, "")
            )

        setupToolbar()
        setupViews()
        setupVmObservers()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: ChangeEmailNewEmailState) {
        when (state) {
            ChangeEmailNewEmailState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            ChangeEmailNewEmailState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            ChangeEmailNewEmailState.InvalidEmail -> {
                binding
                    .inputLayoutEmail
                    .error = getString(R.string.invalid_email)
            }
            ChangeEmailNewEmailState.ShowLoading -> {
                binding.loading setVisible true
            }
            ChangeEmailNewEmailState.HideLoading -> {
                binding.loading setVisible false
            }
            is ChangeEmailNewEmailState.EmailAlreadyTaken -> {
                binding
                    .inputLayoutEmail
                    .error = state.message
            }
            is ChangeEmailNewEmailState.ChangeEmailSuccess -> {
                ChangeEmailVerifyCodeActivity
                    .openActivity(
                        this,
                        state.email,
                        state.verificationToken
                    )
            }
            is ChangeEmailNewEmailState.Error -> {

                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun setupViews() {
        binding
            .inputEmail
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel.onEmailTextChanged(text.toString())
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnContinue
            .ninjaTap {
                binding.inputLayoutEmail.error = ""
                binding.root.hideKeyboard()
                viewModel
                    .changeEmail(
                        binding.inputEmail.text.toString().trim()
                    )
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }
}
