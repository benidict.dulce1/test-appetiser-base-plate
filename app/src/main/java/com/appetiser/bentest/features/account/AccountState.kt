package com.appetiser.bentest.features.account

sealed class AccountState {
    object ShowLoading : AccountState()

    object HideLoading : AccountState()

    data class NavigateToPaymentsScreen(val ephemeralKeyJson: String) : AccountState()

    data class Error(val message: String) : AccountState()
}
