package com.appetiser.bentest.features.report.adapter

import androidx.recyclerview.widget.DiffUtil

class ReportAttachmentsDiffCallback : DiffUtil.ItemCallback<ReportAttachmentsViewTypeObject>() {

    override fun areItemsTheSame(oldItem: ReportAttachmentsViewTypeObject, newItem: ReportAttachmentsViewTypeObject): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: ReportAttachmentsViewTypeObject, newItem: ReportAttachmentsViewTypeObject): Boolean {
        return oldItem.type == newItem.type && oldItem.imagePath == newItem.imagePath
    }
}
