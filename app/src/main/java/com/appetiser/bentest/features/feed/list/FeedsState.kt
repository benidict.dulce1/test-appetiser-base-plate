package com.appetiser.bentest.features.feed.list

import androidx.paging.PagingData
import com.appetiser.module.domain.models.feed.Feed

sealed class FeedsState {

    data class Error(val throwable: Throwable) : FeedsState()

    data class UpdateFeed(val item: Feed) : FeedsState()
    data class RemoveFeed(val item: Feed, val position: Int) : FeedsState()

    object RefreshFeeds : FeedsState()

    data class FeedsFetched(val items: PagingData<Feed>) : FeedsState()

    object ShowProgressLoading : FeedsState()

    object HideProgressLoading : FeedsState()
}
