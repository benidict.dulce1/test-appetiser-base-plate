package com.appetiser.bentest.features.track.trackdetails

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelFragment
import com.appetiser.bentest.databinding.FragmentTrackDetailsBinding
import com.appetiser.bentest.features.track.TrackDetailsState
import com.appetiser.bentest.utils.loadImage
import com.appetiser.module.domain.models.track.Track
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.include_toolbar.*

class TrackDetailsFragment : BaseViewModelFragment<FragmentTrackDetailsBinding, TrackDetailsViewModel>(){

    override fun getLayoutId(): Int = R.layout.fragment_track_details

    override fun setActivityAsViewModelProvider(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel
        setupViewModel()
        setupToolbar()
    }

    private fun setupToolbar(){
        imgBack.setOnClickListener { viewModel.deleteTrackDetails() }
    }

    override fun canBack(): Boolean = true

    override fun onResume() {
        super.onResume()
        viewModel.loadTrackDetails()
    }

    private fun setupViewModel(){
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }


    private fun handleState(state: TrackDetailsState){
        when(state){
            is TrackDetailsState.TrackDetails -> {
                populateTrackDetails(state.track)
            }
            is TrackDetailsState.TrackDetailsError -> {

            }
            is TrackDetailsState.TrackDetailsDeleted -> {
                    findNavController().navigate(R.id.action_track_detials_to_main_list)
             }
        }
    }

    private fun populateTrackDetails(it: Track){
        toolbarTitle.run {
            text = it.trackName
        }
        binding.ivArtWork.loadImage(it.artworkUrl100)
        binding.tvDescription.text = if(it.longDescription.isNullOrEmpty()) it.shortDescription else it.longDescription
        binding.tvGenre.text = it.primaryGenreName
        binding.tvPrice.text = if (it.trackPrice.isNullOrEmpty()) "$0.00" else "$${it.trackPrice}"
        binding.tvTrackName.text = it.trackName
    }
}