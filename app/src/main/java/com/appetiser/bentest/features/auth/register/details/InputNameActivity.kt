package com.appetiser.bentest.features.auth.register.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityInputNameBinding
import com.appetiser.bentest.ext.disabledWithAlpha
import com.appetiser.bentest.ext.enabledWithAlpha
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.features.auth.register.profile.UploadProfilePhotoActivity
import com.appetiser.module.common.*
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class InputNameActivity : BaseViewModelActivity<ActivityInputNameBinding, InputNameViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, InputNameActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_input_name

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setUpViews()
        observeInputViews()
        setupViewModel()
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setUpViews() {
        binding.etName.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    if (binding.btnContinue.isEnabled) {
                        sendUserDetails()
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding
            .btnContinue
            .ninjaTap {
                sendUserDetails()
            }
            .addTo(disposables)
    }

    private fun sendUserDetails() {
        viewModel
            .sendUserDetails(
                binding.etName.text.toString().trim()
            )
    }

    private fun observeInputViews() {
        val firstNameObservable = binding.etName.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() }

        disposables.add(
            firstNameObservable
                .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribeBy(
                    onNext = {
                        if (it) {
                            binding.btnContinue.enabledWithAlpha()
                        } else {
                            binding.btnContinue.disabledWithAlpha()
                        }
                    },
                    onError = {
                        Timber.e("Error $it")
                    }
                )
        )
    }

    private fun setupViewModel() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    handleState(state)
                },
                onError = {
                    Timber.e(it)
                }
            ).apply { disposables.add(this) }
    }

    private fun handleState(state: InputNameState) {
        when (state) {
            is InputNameState.UserDetailsUpdated -> {
                UploadProfilePhotoActivity.openActivity(this)
                finish()
            }
            is InputNameState.Error -> {
                toast(state.throwable.getThrowableError())
            }
            is InputNameState.ShowProgressLoading -> {
                toast("Sending request")
            }
        }
    }
}
