package com.appetiser.bentest.features.account.changephone.newphone

import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.utils.PhoneNumberHelper
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class ChangePhoneNewPhoneViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val resourceManager: ResourceManager,
    private val phoneNumberHelper: PhoneNumberHelper
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<ChangePhoneNewPhoneState>()
    }

    val state: Observable<ChangePhoneNewPhoneState> = _state

    private lateinit var verificationToken: String

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun setVerificationToken(verificationToken: String) {
        this.verificationToken = verificationToken
    }

    fun onPhoneTextChanged(phoneNumber: String) {
        if (validatePhoneNotEmpty(phoneNumber)) {
            _state
                .onNext(
                    ChangePhoneNewPhoneState.EnableButton
                )
        } else {
            _state
                .onNext(
                    ChangePhoneNewPhoneState.DisableButton
                )
        }
    }

    fun changePhone(
        countryCode: String,
        countryNameCode: String,
        phoneNumber: String
    ) {
        if (!validatePhoneNotEmpty(phoneNumber)) return

        val formattedPhoneNumber =
            phoneNumberHelper
                .getInternationalFormattedPhoneNumber(
                    "$countryCode$phoneNumber",
                    countryNameCode
                )

        authRepository
            .requestChangePhone(
                verificationToken,
                formattedPhoneNumber
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state
                    .onNext(
                        ChangePhoneNewPhoneState.ShowLoading
                    )
            }
            .doOnComplete {
                _state
                    .onNext(
                        ChangePhoneNewPhoneState.HideLoading
                    )
            }
            .doOnError {
                _state
                    .onNext(
                        ChangePhoneNewPhoneState.HideLoading
                    )
            }
            .subscribeBy(
                onComplete = {
                    _state
                        .onNext(
                            ChangePhoneNewPhoneState.ChangePhoneSuccess(
                                formattedPhoneNumber,
                                verificationToken
                            )
                        )
                },
                onError = { error ->
                    Timber.e(error)
                    handleError(error)
                }
            )
            .addTo(disposables)
    }

    private fun handleError(error: Throwable) {
        if (error is HttpException && error.code() == 422) {
            _state
                .onNext(
                    ChangePhoneNewPhoneState.PhoneNumberError(
                        error.getThrowableError()
                    )
                )
        } else {
            showGenericError()
        }
    }

    private fun showGenericError() {
        _state
            .onNext(
                ChangePhoneNewPhoneState.Error(
                    resourceManager
                        .getString(
                            R.string.generic_error_short
                        )
                )
            )
    }

    private fun validatePhoneNotEmpty(phoneNumber: String): Boolean {
        return phoneNumber.isNotEmpty()
    }
}
