package com.appetiser.bentest.features.report.adapter

interface OnReportListener {

    fun addPhoto()

    fun deletePhoto(item: ReportAttachmentsViewTypeObject)
}
