package com.appetiser.bentest.features.splash

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelFragment
import com.appetiser.bentest.databinding.FragmentSplashBinding
import com.appetiser.bentest.features.track.TrackDetailsState
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class SplashFragment : BaseViewModelFragment<FragmentSplashBinding, SplashViewModel>(){

    override fun getLayoutId(): Int = R.layout.fragment_splash

    override fun setActivityAsViewModelProvider(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel
        setupViewModel()
    }

    private fun setupViewModel(){
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadTrackDetails()
    }

    private fun handleState(state: TrackDetailsState){
        when(state){
            is TrackDetailsState.TrackDetails -> {
                findNavController().navigate(if(state.track.artistId.isNullOrEmpty()) R.id.action_splash_to_tracklist else R.id.action_splash_to_track_details)
            }
            is TrackDetailsState.TrackDetailsError -> {
                findNavController().navigate(R.id.action_splash_to_tracklist)
            }
            is TrackDetailsState.TrackDetailsDeleted -> {}
        }
    }


}