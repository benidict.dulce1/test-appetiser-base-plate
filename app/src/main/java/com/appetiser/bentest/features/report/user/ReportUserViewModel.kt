package com.appetiser.bentest.features.report.user

import android.content.Context
import android.net.Uri
import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.features.report.adapter.ReportAttachmentsViewType
import com.appetiser.bentest.features.report.adapter.ReportAttachmentsViewTypeObject
import com.appetiser.bentest.utils.FileUtils
import com.appetiser.module.data.features.miscellaneous.MiscellaneousRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class ReportUserViewModel @Inject constructor(
    private val miscellaneousRepository: MiscellaneousRepository
) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        // TODO:: Retrieve real user id
        getReportCategories()
    }

    private val _state by lazy {
        PublishSubject.create<ReportUserState>()
    }

    val state: Observable<ReportUserState> = _state

    private var pathList: ArrayList<String> = arrayListOf()

    fun addPhoto(imagePath: String) {
        this.pathList.add(imagePath)
        _state.onNext(ReportUserState.AddPhoto(imagePath))
    }

    fun removePhoto(item: ReportAttachmentsViewTypeObject) {
        this.pathList.remove(item.imagePath)
        _state.onNext(ReportUserState.RemovePhoto(item))
    }

    fun displayAddPhotoButton(item: ReportAttachmentsViewType) {
        _state.onNext(ReportUserState.DisplayAddButton(item))
    }

    private fun getReportCategories() {
        miscellaneousRepository
            .getReportCategories()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { categories ->
                    _state.onNext(ReportUserState.ReportCategoriesFetched(categories))
                },
                onError = { error ->
                    _state.onNext(ReportUserState.Error(error))
                }
            )
            .addTo(disposables)
    }

    fun reportUser(
        context: Context,
        reportedUserId: String,
        reasonId: String,
        description: String
    ) {
        var list = listOf<Uri>()
        FileUtils
            .resizePhotos(context, this.pathList)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSuccess { uris ->
                list = uris
            }
            .flatMap { uris ->
                val imagePathList = uris.map { it.path.orEmpty() }

                miscellaneousRepository
                    .reportUser(
                        reportedUserId,
                        reasonId,
                        description,
                        imagePathList
                    )
                    .subscribeOn(schedulers.io())
            }
            .doOnSubscribe {
                _state.onNext(ReportUserState.ShowProgressLoading)
            }
            .doOnSuccess {
                deleteResizedPhotos(list)
                _state.onNext(ReportUserState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(ReportUserState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state.onNext(ReportUserState.ReportSuccess(it))
                },
                onError = { error ->
                    _state.onNext(ReportUserState.Error(error))
                }
            )
            .apply { disposables.add(this) }
    }

    private fun deleteResizedPhotos(uris: List<Uri>) {
        uris.forEach { uri ->
            FileUtils.deleteFile(uri)
        }
    }
}
