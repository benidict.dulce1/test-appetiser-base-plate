package com.appetiser.bentest.features.auth.phonenumbercheck

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityPhoneNumberCheckBinding
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.features.auth.login.LoginActivity
import com.appetiser.bentest.features.auth.register.createpassword.CreatePasswordActivity
import com.appetiser.module.common.TEXT_WATCHER_DEBOUNCE_TIME
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class PhoneNumberCheckActivity : BaseViewModelActivity<ActivityPhoneNumberCheckBinding, PhoneNumberCheckViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    PhoneNumberCheckActivity::class.java
                )
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_phone_number_check

    override fun canBack(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupToolbar()
        setupViews()
        setupVmObservers()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: PhoneNumberCheckState) {
        when (state) {
            PhoneNumberCheckState.EnableButton -> {
                binding
                    .btnContinue
                    .isEnabled = true
            }
            PhoneNumberCheckState.DisableButton -> {
                binding
                    .btnContinue
                    .isEnabled = false
            }
            PhoneNumberCheckState.ShowProgressLoading -> {
                disableFields()
            }
            PhoneNumberCheckState.HideProgressLoading -> {
                enableFields()
            }
            is PhoneNumberCheckState.Error -> {
                toast(state.throwable.getThrowableError())
                enableFields()
            }
            is PhoneNumberCheckState.PhoneNumberExists -> {
                LoginActivity
                    .openActivity(
                        this,
                        email = "",
                        phone = state.phoneNumber
                    )
            }
            is PhoneNumberCheckState.PhoneNumberDoesNotExist -> {
                // TODO 2020-05-27 Change extra to username
                CreatePasswordActivity
                    .openActivity(
                        this,
                        phone = state.phoneNumber
                    )
            }
        }
    }

    private fun enableFields() {
        binding
            .inputPhone
            .isEnabled = true
        binding
            .countryCodePicker
            .isEnabled = true
        binding
            .btnContinue
            .isEnabled = true
    }

    private fun disableFields() {
        binding
            .inputPhone
            .isEnabled = false
        binding
            .countryCodePicker
            .isEnabled = false
        binding
            .btnContinue
            .isEnabled = false
    }

    private fun setupViews() {
        binding
            .inputPhone
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribe {
                viewModel
                    .onPhoneNumberTextChange(it.toString())
            }
            .addTo(disposables)

        binding
            .btnContinue
            .ninjaTap {
                val countryCode = binding.countryCodePicker.selectedCountryCode
                val countryNameCode = binding.countryCodePicker.selectedCountryNameCode
                val phoneNumber = binding.inputPhone.text.toString()

                viewModel
                    .checkPhoneNumber(
                        countryCode,
                        countryNameCode,
                        phoneNumber
                    )
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }
}
