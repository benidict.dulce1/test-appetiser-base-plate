package com.appetiser.bentest.features.account.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityAccountSettingsBinding
import com.appetiser.bentest.features.account.changeemail.verifypassword.ChangeEmailVerifyPasswordActivity
import com.appetiser.bentest.features.account.changepassword.currentpassword.ChangeCurrentPasswordActivity
import com.appetiser.bentest.features.account.changephone.verifypassword.ChangePhoneVerifyPasswordActivity
import com.appetiser.bentest.features.account.deleteaccount.DeleteAccountPasswordActivity
import com.appetiser.bentest.features.auth.FacebookLoginManager
import com.appetiser.bentest.features.auth.GoogleSignInManager
import com.appetiser.bentest.features.auth.emailcheck.EmailCheckActivity
import com.appetiser.module.common.*
import com.appetiser.module.payments.utils.PaymentsHelper
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class AccountSettingsActivity : BaseViewModelActivity<ActivityAccountSettingsBinding, AccountSettingsViewModel>() {

    companion object {
        const val EXTRA_FROM_CHANGE_EMAIL = "EXTRA_FROM_CHANGE_EMAIL"
        const val EXTRA_FROM_CHANGE_PHONE = "EXTRA_FROM_CHANGE_PHONE"
        const val EXTRA_FROM_CHANGE_PASSWORD = "EXTRA_FROM_CHANGE_PASSWORD"

        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    AccountSettingsActivity::class.java
                )
            )
        }

        fun openActivity(context: Context, extras: Bundle, vararg flags: Int) {
            context.startActivity(
                Intent(
                    context,
                    AccountSettingsActivity::class.java
                ).apply {
                    flags.forEach {
                        addFlags(it)
                    }
                    putExtras(extras)
                }
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_account_settings

    override fun canBack(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupToolbar()
        setupViewModel()
        setupViews()

        val isFromChangeEmail =
            intent
                ?.extras
                ?.getBoolean(
                    EXTRA_FROM_CHANGE_EMAIL,
                    false
                ) ?: false

        val isFromChangePhone =
            intent
                ?.extras
                ?.getBoolean(
                    EXTRA_FROM_CHANGE_PHONE,
                    false
                ) ?: false

        val isFromChangePassword =
            intent
                ?.extras
                ?.getBoolean(
                    EXTRA_FROM_CHANGE_PASSWORD,
                    false
                ) ?: false

        viewModel.setFromChangeEmail(isFromChangeEmail)
        viewModel.setFromChangePhone(isFromChangePhone)
        viewModel.setFromChangePassword(isFromChangePassword)
    }

    private fun setupViews() {
        binding
            .btnChangeEmail
            .ninjaTap {
                ChangeEmailVerifyPasswordActivity
                    .openActivity(this)
            }
            .addTo(disposables)

        binding
            .btnChangeMobileNumber
            .ninjaTap {
                ChangePhoneVerifyPasswordActivity
                    .openActivity(this)
            }
            .addTo(disposables)

        binding
            .layoutLogout
            .ninjaTap {
                showLogoutConfirmationDialog()
            }
            .addTo(disposables)

        binding
            .btnDeleteAccount
            .ninjaTap {
                showDeleteAccountConfirmationDialog()
            }
            .addTo(disposables)

        binding
            .btnChangePassword
            .ninjaTap {
                ChangeCurrentPasswordActivity
                    .openActivity(this)
            }
    }

    private fun showLogoutConfirmationDialog() {
        showConfirmDialog(
            body = getString(R.string.logout_message),
            negativeButtonText = getString(R.string.cancel),
            positiveButtonText = getString(R.string.yes),
            positiveClickListener = {
                viewModel
                    .logout {
                        logoutSocialLogins()
                    }
            }
        )
    }

    private fun showDeleteAccountConfirmationDialog() {
        showConfirmDialog(
            title = getString(R.string.delete_account_confirmation_title),
            body = getString(R.string.delete_account_confirmation_body),
            negativeButtonText = getString(R.string.cancel),
            positiveButtonText = getString(R.string.yes),
            positiveClickListener = {
                DeleteAccountPasswordActivity
                    .openActivity(this)
            }
        )
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarTitle(getString(R.string.account_settings))
    }

    private fun setupViewModel() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                },
                onError = { Timber.e("Error $it ") }
            )
            .addTo(disposables)
    }

    private fun handleState(state: AccountSettingsState) {
        when (state) {
            AccountSettingsState.ShowEmailChangeSuccess -> {
                showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.your_email_has_been_updated)
                )
            }
            AccountSettingsState.ShowPhoneChangeSuccess -> {
                showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.your_mobile_number_has_been_updated)
                )
            }
            AccountSettingsState.ShowChangePasswordSuccess -> {
                showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.your_password_has_been_updated)
                )
            }
            AccountSettingsState.ShowLoading -> {
                binding.loading setVisible true
            }
            AccountSettingsState.HideLoading -> {
                binding.loading setVisible false
            }
            is AccountSettingsState.DisplayDetails -> {
                binding.txtEmailAddress.text = state.email
                binding.txtMobileNumber.text = state.phoneNumber
            }
            is AccountSettingsState.NavigateToEmailCheck -> {
                EmailCheckActivity.openActivity(this)
                finishAffinity()
            }
            is AccountSettingsState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun logoutSocialLogins() {
        // Sign out google if it's signed in.
        if (GoogleSignInManager.isSignedIn(this)) {
            GoogleSignInManager(this)
                .run {
                    signOut()
                }
        }

        // Logout facebook if it's logged in.
        if (FacebookLoginManager.isLoggedIn()) {
            FacebookLoginManager.logout()
        }

        // Logout payments.
        PaymentsHelper.logout()
    }
}
