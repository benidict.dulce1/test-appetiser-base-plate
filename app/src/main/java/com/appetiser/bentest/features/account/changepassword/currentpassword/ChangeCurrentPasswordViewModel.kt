package com.appetiser.bentest.features.account.changepassword.currentpassword

import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.appetiser.module.domain.utils.PASSWORD_MIN_LENGTH
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class ChangeCurrentPasswordViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    private val _state by lazy {
        PublishSubject.create<ChangeCurrentPasswordState>()
    }

    val state: Observable<ChangeCurrentPasswordState> = _state

    fun onPasswordTextChanged(password: String) {
        if (validatePasswordNotEmpty(password)) {
            _state
                .onNext(
                    ChangeCurrentPasswordState.EnableButton
                )
        } else {
            _state
                .onNext(
                    ChangeCurrentPasswordState.DisableButton
                )
        }
    }

    fun verifyPassword(password: String) {
        if (!validatePassword(password)) return

        repository
            .verifyPassword(password)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state
                    .onNext(
                        ChangeCurrentPasswordState.ShowLoading
                    )
            }
            .doOnSuccess {
                _state
                    .onNext(
                        ChangeCurrentPasswordState.HideLoading
                    )
            }
            .doOnError {
                _state
                    .onNext(
                        ChangeCurrentPasswordState.HideLoading
                    )
            }
            .subscribeBy(
                onSuccess = { verificationToken ->
                    _state
                        .onNext(
                            ChangeCurrentPasswordState.VerificationSuccess(
                                verificationToken
                            )
                        )
                },
                onError = { error ->
                    Timber.e(error)
                    if (error is HttpException && error.code() == 400) {
                        _state
                            .onNext(
                                ChangeCurrentPasswordState.InvalidPassword
                            )
                    } else {
                        showGenericError()
                    }
                }
            )
            .addTo(disposables)
    }

    private fun showGenericError() {
        _state
            .onNext(
                ChangeCurrentPasswordState.Error(
                    resourceManager
                        .getString(
                            R.string.generic_error_short
                        )
                )
            )
    }

    private fun validatePassword(password: String): Boolean {
        if (!validatePasswordNotEmpty(password)) return false

        if (password.length < PASSWORD_MIN_LENGTH) {
            _state
                .onNext(
                    ChangeCurrentPasswordState.PasswordBelowMinLength
                )
            return false
        }

        if (password.length > PASSWORD_MAX_LENGTH) {
            _state
                .onNext(
                    ChangeCurrentPasswordState.PasswordExceedsMaxLength
                )
            return false
        }
        return true
    }

    private fun validatePasswordNotEmpty(password: String): Boolean {
        return password.isNotEmpty() && password.length >= PASSWORD_MIN_LENGTH
    }
}
