package com.appetiser.bentest.features.track

import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityBaseMainHostBinding


class BaseMainHostActivity: BaseViewModelActivity<ActivityBaseMainHostBinding, BaseMainHostViewModel>(){
    override fun getLayoutId(): Int = R.layout.activity_base_main_host
}