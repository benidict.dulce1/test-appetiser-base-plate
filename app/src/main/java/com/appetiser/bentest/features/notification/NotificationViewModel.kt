package com.appetiser.bentest.features.notification

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.module.data.features.notification.NotificationRepository
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class NotificationViewModel @Inject constructor(
    private val notificationRepository: NotificationRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<NotificationState>()
    }

    val state: Observable<NotificationState> = _state

    private val _loadingVisibility by lazy {
        MutableLiveData<Boolean>()
    }

    val loadingVisibility: LiveData<Boolean> = _loadingVisibility

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        Observable.combineLatest<List<NotificationViewTypeItem>, List<NotificationViewTypeItem>, List<NotificationViewTypeItem>>(
            getNotificationByGroupings(NotificationGroupings.TODAY),
            getNotificationByGroupings(NotificationGroupings.WEEK),
            BiFunction { today, week ->
                val items = mutableListOf<NotificationViewTypeItem>()

                if (today.isNotEmpty()) {
                    items.add(NotificationViewTypeItem(NotificationType.HEADER, null, "Today"))
                    items.addAll(today)
                }

                if (week.isNotEmpty()) {
                    items.add(NotificationViewTypeItem(NotificationType.HEADER, null, "This Week"))
                    items.addAll(week)
                }

                return@BiFunction items
            }
        )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _loadingVisibility.value = true
            }
            .subscribeBy(
                onNext = {
                    _loadingVisibility.value = false
                    _state.onNext(NotificationState.FetchNotificationItems(it))
                },
                onError = {
                    _loadingVisibility.value = false
                    _state.onNext(NotificationState.Error(it))
                }
            )
            .addTo(disposables)
    }

    private fun getNotificationByGroupings(groupings: NotificationGroupings): Observable<List<NotificationViewTypeItem>> {
        return notificationRepository.getNotifications(groupings.type)
            .flatMapIterable { it }
            .map { NotificationViewTypeItem(NotificationType.NOTIFICATION, it, null) }
            .toList()
            .toObservable()
            .subscribeOn(schedulers.io())
            .doOnError {
                Timber.e("Error $it")
            }
    }

    enum class NotificationGroupings(val type: String) {
        TODAY("today"), WEEK("this-week")
    }
}
