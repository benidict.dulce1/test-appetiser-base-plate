package com.appetiser.bentest.features.auth.forgotpassword

sealed class ForgotPasswordState {

    data class GetUsername(val username: String) : ForgotPasswordState()

    object Success : ForgotPasswordState()

    data class Error(val throwable: Throwable) : ForgotPasswordState()

    object ShowProgressLoading : ForgotPasswordState()

    object HideProgressLoading : ForgotPasswordState()
}
