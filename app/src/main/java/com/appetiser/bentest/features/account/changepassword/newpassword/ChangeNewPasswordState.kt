package com.appetiser.bentest.features.account.changepassword.newpassword

sealed class ChangeNewPasswordState {

    object Success : ChangeNewPasswordState()

    data class Error(val message: String) : ChangeNewPasswordState()

    object PasswordBelowMinLength : ChangeNewPasswordState()

    object PasswordExceedsMaxLength : ChangeNewPasswordState()

    object EnableButton : ChangeNewPasswordState()

    object DisableButton : ChangeNewPasswordState()

    object InvalidPassword : ChangeNewPasswordState()

    object ShowProgressLoading : ChangeNewPasswordState()

    object HideProgressLoading : ChangeNewPasswordState()
}
