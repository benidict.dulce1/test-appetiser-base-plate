package com.appetiser.bentest.features.splash

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.features.track.TrackDetailsState
import com.appetiser.module.data.features.track.TrackRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    private val repository: TrackRepository
): BaseViewModel(){

    private val _state by lazy {
        PublishSubject.create<TrackDetailsState>()
    }
    val state: Observable<TrackDetailsState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit
    fun loadTrackDetails(){
        repository.loadTrackDetails()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .doOnError {
                _state.onNext(TrackDetailsState.TrackDetailsError(it))
            }
            .subscribeBy (
                onSuccess = {
                    _state.onNext(TrackDetailsState.TrackDetails(it))
                },
                onError = {
                    _state.onNext(TrackDetailsState.TrackDetailsError(it))
                }
            ).addTo(disposables)
    }

}