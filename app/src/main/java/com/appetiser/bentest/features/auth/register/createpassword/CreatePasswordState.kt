package com.appetiser.bentest.features.auth.register.createpassword

sealed class CreatePasswordState {
    data class FillUsername(
        val username: String
    ) : CreatePasswordState()

    data class StartVerificationCodeScreen(
        val email: String,
        val phone: String
    ) : CreatePasswordState()

    object PasswordBelowMinLength : CreatePasswordState()

    object PasswordExceedsMaxLength : CreatePasswordState()

    object ShowLoading : CreatePasswordState()

    object HideLoading : CreatePasswordState()

    object EnableButton : CreatePasswordState()

    object DisableButton : CreatePasswordState()

    data class Error(val message: String) : CreatePasswordState()
}
