package com.appetiser.bentest.features.notification

import android.annotation.SuppressLint
import android.content.Context
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseListAdapter
import com.appetiser.bentest.databinding.ItemNotificationBinding
import com.appetiser.bentest.databinding.ItemNotificationHeaderBinding
import com.appetiser.bentest.ext.loadAvatarUrl
import com.appetiser.module.common.getTimeSpanString
import com.appetiser.module.common.widget.CustomTypefaceSpan

class NotificationAdapter(val context: Context) : BaseListAdapter<NotificationViewTypeItem, BaseListAdapter.BaseViewViewHolder<NotificationViewTypeItem>>(NotificationDiffCallback()) {

    companion object {
        const val ITEM_HEADER = 0
        const val ITEM_LIKE = 1
        const val ITEM_FOLLOW = 2
        const val ITEM_COMMENT = 3
    }

    val regularTypeface = ResourcesCompat.getFont(context, R.font.inter_regular)!!

    override fun getItemViewType(position: Int): Int {
        val notificationType = items[position]
        return when (notificationType.type) {
            NotificationType.HEADER -> ITEM_HEADER
            else -> {
                val notification = notificationType.notification!!
                return when (notification.type) {
                    com.appetiser.module.domain.models.notification.NotificationType.COMMENT -> {
                        ITEM_COMMENT
                    }
                    com.appetiser.module.domain.models.notification.NotificationType.FOLLOW -> {
                        ITEM_FOLLOW
                    }
                    else -> {
                        ITEM_LIKE
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewViewHolder<NotificationViewTypeItem> {
        return when (viewType) {
            // TODO need separate item layout concerns
            ITEM_FOLLOW -> createNotificationViewHolder(parent)
            ITEM_LIKE -> createNotificationViewHolder(parent)
            ITEM_COMMENT -> createNotificationViewHolder(parent)
            else -> createNotificationTitleViewHolder(parent)
        }
    }

    override fun onBindViewHolder(holder: BaseViewViewHolder<NotificationViewTypeItem>, position: Int) {
        holder.bind(items[position])
    }

    private fun createNotificationTitleViewHolder(parent: ViewGroup): NotificationTitleItemViewHolder {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_notification_header, parent, false)

        val binding = ItemNotificationHeaderBinding.bind(view)

        return NotificationTitleItemViewHolder(binding)
    }

    private fun createNotificationViewHolder(parent: ViewGroup): NotificationItemViewHolder {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_notification, parent, false)

        val binding = ItemNotificationBinding.bind(view)

        return NotificationItemViewHolder(binding)
    }

    inner class NotificationTitleItemViewHolder(binding: ItemNotificationHeaderBinding) : BaseViewViewHolder<NotificationViewTypeItem>(binding)

    inner class NotificationItemViewHolder(override val binding: ItemNotificationBinding) : BaseViewViewHolder<NotificationViewTypeItem>(binding) {

        @SuppressLint("SetTextI18n")
        override fun bind(item: NotificationViewTypeItem) {

            binding.name.movementMethod = LinkMovementMethod.getInstance()

            val time = item.notification?.created_at?.getTimeSpanString().toString()

            val data = "${item.notification?.message} $time"
            val user = item.notification?.actor?.fullName.orEmpty()

            val ssb = SpannableStringBuilder(data)

            // user message
            if (data.contains(user, false)) {
                ssb.setSpan(
                    CustomTypefaceSpan(regularTypeface),
                    item.usernameLength + 1,
                    item.messageLength + 1,
                    Spanned.SPAN_EXCLUSIVE_INCLUSIVE
                )

                ssb.setSpan(
                    RelativeSizeSpan(0.9f),
                    item.usernameLength + 1,
                    item.messageLength + 1,
                    Spanned.SPAN_EXCLUSIVE_INCLUSIVE
                )
            } else {
                ssb.setSpan(
                    CustomTypefaceSpan(regularTypeface),
                    0,
                    item.messageLength + 1,
                    Spanned.SPAN_EXCLUSIVE_INCLUSIVE
                )

                ssb.setSpan(
                    RelativeSizeSpan(0.9f),
                    0,
                    item.messageLength + 1,
                    Spanned.SPAN_EXCLUSIVE_INCLUSIVE
                )
            }

            // user time
            ssb.setSpan(
                CustomTypefaceSpan(regularTypeface),
                item.messageLength + 1,
                item.messageLength + time.length + 1,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )

            ssb.setSpan(
                RelativeSizeSpan(0.7f),
                item.messageLength + 1,
                item.messageLength + time.length + 1,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )

            binding.name.text = ssb
            binding.avatar.loadAvatarUrl(item.notification?.actor?.avatarPermanentThumbUrl)
        }
    }
}
