package com.appetiser.bentest.features.report.user

import com.appetiser.bentest.features.report.adapter.ReportAttachmentsViewType
import com.appetiser.bentest.features.report.adapter.ReportAttachmentsViewTypeObject
import com.appetiser.module.domain.models.miscellaneous.ReportCategory
import com.appetiser.module.domain.models.miscellaneous.ReportedUser

sealed class ReportUserState {

    data class AddPhoto(val imagePath: String) : ReportUserState()

    data class RemovePhoto(val item: ReportAttachmentsViewTypeObject) : ReportUserState()

    data class DisplayAddButton(val item: ReportAttachmentsViewType) : ReportUserState()

    data class ReportCategoriesFetched(val reportCategories: List<ReportCategory>) : ReportUserState()

    data class ReportSuccess(val reportedUser: ReportedUser) : ReportUserState()

    data class Error(val throwable: Throwable) : ReportUserState()

    object ShowProgressLoading : ReportUserState()

    object HideProgressLoading : ReportUserState()
}
