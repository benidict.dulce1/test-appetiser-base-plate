package com.appetiser.bentest.features.feed.create

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.module.data.features.feeds.FeedRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CreateFeedViewModel @Inject constructor(
    private val repository: FeedRepository
) : BaseViewModel() {

    var imagePath = ""

    private val _state by lazy {
        PublishSubject.create<CreateFeedState>()
    }

    val state: Observable<CreateFeedState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun createFeed(description: String) {
        if (imagePath.isEmpty()) {
            _state.onNext(CreateFeedState.Error(Throwable("No image found!")))
            return
        }

        if (description.isEmpty()) {
            _state.onNext(CreateFeedState.Error(Throwable("Description must not be empty")))
            return
        }

        repository.postFeed(body = description, filePath = imagePath)
            .subscribeOn(schedulers.io())
            .delay(1000, TimeUnit.MILLISECONDS)
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(CreateFeedState.ShowProgressLoading)
            }
            .doFinally {
                _state.onNext(CreateFeedState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state.onNext(CreateFeedState.CreatedFeedSuccess(it))
                },
                onError = {
                    _state.onNext(CreateFeedState.Error(it))
                }
            )
            .addTo(disposables)
    }
}
