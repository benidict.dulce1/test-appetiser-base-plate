package com.appetiser.bentest.features.feed.create

import com.appetiser.module.domain.models.feed.Feed

sealed class CreateFeedState {

    data class CreatedFeedSuccess(val item: Feed) : CreateFeedState()

    data class Error(val throwable: Throwable) : CreateFeedState()

    object ShowProgressLoading : CreateFeedState()

    object HideProgressLoading : CreateFeedState()
}
