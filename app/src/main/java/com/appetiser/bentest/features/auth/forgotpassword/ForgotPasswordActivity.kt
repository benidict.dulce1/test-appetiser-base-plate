package com.appetiser.bentest.features.auth.forgotpassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityForgotPasswordBinding
import com.appetiser.bentest.ext.enableWithAplhaWhen
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.features.auth.forgotpassword.verification.ForgotPasswordVerificationActivity
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class ForgotPasswordActivity : BaseViewModelActivity<ActivityForgotPasswordBinding, ForgotPasswordViewModel>() {

    companion object {
        const val KEY_USERNAME = "username"

        fun openActivity(context: Context, username: String) {
            val intent = Intent(context, ForgotPasswordActivity::class.java)
            intent.putExtra(KEY_USERNAME, username)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_forgot_password

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        intent?.extras?.run {
            val username = getString(KEY_USERNAME, "").orEmpty()
            viewModel.setUsername(username)
        }

        setupViews()
        setupToolbar()
        setupViewModels()
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupViews() {
        binding.etUsername.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    if (binding.btnContinue.isEnabled) {
                        viewModel.forgotPassword(binding.etUsername.text.toString())
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding
            .btnContinue
            .enableWithAplhaWhen(binding.etUsername) {
                it.isNotEmpty()
            }

        disposables.add(
            binding.btnContinue.ninjaTap {
                viewModel.forgotPassword(binding.etUsername.text.toString())
            }
        )
    }

    private fun setupViewModels() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: ForgotPasswordState) {
        when (state) {
            is ForgotPasswordState.GetUsername -> {
                binding.etUsername.apply {
                    setText(state.username)
                    setSelection(state.username.length)
                }
            }
            ForgotPasswordState.Success -> {
                ForgotPasswordVerificationActivity
                    .openActivity(
                        this,
                        binding.etUsername.text.toString()
                    )
            }
            is ForgotPasswordState.Error -> {
                toast(state.throwable.getThrowableError())
            }
            is ForgotPasswordState.ShowProgressLoading -> {
                toast("Sending request")
            }
        }
    }
}
