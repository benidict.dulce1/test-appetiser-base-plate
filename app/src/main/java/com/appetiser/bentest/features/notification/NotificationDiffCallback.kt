package com.appetiser.bentest.features.notification

import androidx.recyclerview.widget.DiffUtil

class NotificationDiffCallback : DiffUtil.ItemCallback<NotificationViewTypeItem>() {

    override fun areItemsTheSame(oldItem: NotificationViewTypeItem, newItem: NotificationViewTypeItem): Boolean {
        return oldItem.notification == newItem.notification && oldItem.type == newItem.type
    }

    override fun areContentsTheSame(oldItem: NotificationViewTypeItem, newItem: NotificationViewTypeItem): Boolean {
        return oldItem.notification == newItem.notification && oldItem.type == newItem.type && oldItem.title == newItem.title
    }
}
