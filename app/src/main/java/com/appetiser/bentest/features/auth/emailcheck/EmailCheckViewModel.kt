package com.appetiser.bentest.features.auth.emailcheck

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import javax.inject.Inject

class EmailCheckViewModel @Inject constructor(
    private val authRepository: AuthRepository
) : BaseViewModel() {

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    private val _state by lazy {
        PublishSubject.create<EmailCheckState>()
    }

    val state: Observable<EmailCheckState> = _state

    fun checkEmail(email: String) {
        authRepository
            .checkUsername(email)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(EmailCheckState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(EmailCheckState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(EmailCheckState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state.onNext(EmailCheckState.EmailExists(email))
                },
                onError = { error ->
                    when (error) {
                        is HttpException -> {
                            if (error.code() == 404) {
                                _state.onNext(EmailCheckState.EmailDoesNotExist(email))
                            } else {
                                _state.onNext(EmailCheckState.Error(error))
                            }
                        }
                        else -> {
                            _state.onNext(EmailCheckState.Error(error))
                        }
                    }
                }
            )
            .apply { disposables.add(this) }
    }
}
