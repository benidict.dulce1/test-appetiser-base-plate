package com.appetiser.bentest.features.auth.phonenumbercheck

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.utils.PhoneNumberHelper
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import javax.inject.Inject

class PhoneNumberCheckViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val phoneNumberHelper: PhoneNumberHelper
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<PhoneNumberCheckState>()
    }

    val state: Observable<PhoneNumberCheckState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun checkPhoneNumber(
        countryCode: String,
        countryNameCode: String,
        phoneNumber: String
    ) {
        val formattedPhoneNumber =
            phoneNumberHelper
                .getInternationalFormattedPhoneNumber(
                    "$countryCode$phoneNumber",
                    countryNameCode
                )

        authRepository
            .checkUsername(formattedPhoneNumber)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(PhoneNumberCheckState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(PhoneNumberCheckState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(PhoneNumberCheckState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state
                        .onNext(
                            PhoneNumberCheckState.PhoneNumberExists(
                                formattedPhoneNumber
                            )
                        )
                },
                onError = { error ->
                    when (error) {
                        is HttpException -> {
                            if (error.code() == 404) {
                                _state
                                    .onNext(
                                        PhoneNumberCheckState.PhoneNumberDoesNotExist(
                                            formattedPhoneNumber
                                        )
                                    )
                            } else {
                                _state
                                    .onNext(
                                        PhoneNumberCheckState.Error(error)
                                    )
                            }
                        }
                        else -> {
                            _state
                                .onNext(
                                    PhoneNumberCheckState.Error(error)
                                )
                        }
                    }
                }
            )
            .apply { disposables.add(this) }
    }

    fun onPhoneNumberTextChange(phoneNumber: String) {
        if (validatePhoneNumberNotEmpty(phoneNumber)) {
            _state
                .onNext(
                    PhoneNumberCheckState
                        .EnableButton
                )
        } else {
            _state
                .onNext(
                    PhoneNumberCheckState
                        .DisableButton
                )
        }
    }

    private fun validatePhoneNumberNotEmpty(phoneNumber: String): Boolean {
        return phoneNumber.isNotEmpty()
    }
}
