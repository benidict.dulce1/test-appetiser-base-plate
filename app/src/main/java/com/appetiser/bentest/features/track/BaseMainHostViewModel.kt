package com.appetiser.bentest.features.track

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import javax.inject.Inject

class BaseMainHostViewModel  @Inject constructor(): BaseViewModel(){
    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit
}