package com.appetiser.bentest.features.auth.forgotpassword.newpassword

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.module.common.toObservable
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.appetiser.module.domain.utils.PASSWORD_MIN_LENGTH
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class NewPasswordViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    private lateinit var username: String
    private lateinit var token: String

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        username = bundle?.getString(NewPasswordActivity.KEY_USERNAME, "").orEmpty()
        token = bundle?.getString(NewPasswordActivity.KEY_TOKEN, "").orEmpty()

        username.toObservable {
            _state.onNext(NewPasswordState.GetEmail(it))
        }
    }

    private val _state by lazy {
        PublishSubject.create<NewPasswordState>()
    }

    val state: Observable<NewPasswordState> = _state

    fun sendNewPassword(password: String) {
        if (!validatePassword(password)) return

        _state.onNext(NewPasswordState.ShowProgressLoading)
        disposables.add(
            repository.newPassword(username, token, password, password)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doOnSubscribe {
                    _state.onNext(NewPasswordState.ShowProgressLoading)
                }
                .doOnSuccess {
                    _state.onNext(NewPasswordState.HideProgressLoading)
                }
                .doOnError {
                    _state.onNext(NewPasswordState.HideProgressLoading)
                }
                .subscribeBy(
                    onSuccess = {
                        _state.onNext(NewPasswordState.Success(username))
                    },
                    onError = {
                        _state.onNext(NewPasswordState.Error(it))
                    }
                )
        )
    }

    private fun validatePassword(password: String): Boolean {
        if (password.length < PASSWORD_MIN_LENGTH) {
            _state
                .onNext(
                    NewPasswordState.PasswordBelowMinLength
                )
            return false
        }

        if (password.length > PASSWORD_MAX_LENGTH) {
            _state
                .onNext(
                    NewPasswordState.PasswordExceedsMaxLength
                )
            return false
        }

        return true
    }
}
