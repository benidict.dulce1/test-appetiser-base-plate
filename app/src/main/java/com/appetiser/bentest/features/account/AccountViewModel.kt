package com.appetiser.bentest.features.account

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.payment.PaymentRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.models.user.User
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class AccountViewModel @Inject constructor(
    private val sessionRepository: SessionRepository,
    private val paymentsRepository: PaymentRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<AccountState>()
    }

    val state: Observable<AccountState> = _state

    private val _user by lazy {
        MutableLiveData<User>()
    }

    val user: LiveData<User> = _user

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun loadAccount() {
        sessionRepository
            .getSession()
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    _user.value = session.user
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    fun onPaymentsClick() {
        paymentsRepository
            .createEphemeralKey()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(AccountState.ShowLoading)
            }
            .doOnSuccess {
                _state.onNext(AccountState.HideLoading)
            }
            .doOnError {
                _state.onNext(AccountState.HideLoading)
            }
            .subscribeBy(
                onSuccess = { ephemeralKeyJson ->
                    _state
                        .onNext(
                            AccountState.NavigateToPaymentsScreen(
                                ephemeralKeyJson
                            )
                        )
                },
                onError = {
                    Timber.e(it)
                    _state
                        .onNext(
                            AccountState.Error(
                                resourceManager.getString(
                                    R.string.generic_error_short
                                )
                            )
                        )
                }
            )
            .addTo(disposables)
    }
}
