package com.appetiser.bentest.features.account.about

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseActivity
import com.appetiser.bentest.databinding.ActivityAboutBinding
import com.appetiser.module.common.ninjaTap
import io.reactivex.rxkotlin.addTo

class AboutActivity : BaseActivity<ActivityAboutBinding>() {

    companion object {

        const val TERMS_AND_CONDITION_URL = "https://baseplate-api.appetiserdev.tech"
        const val PRIVACY_POLICY_URL = "https://appetiser.com.au"

        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    AboutActivity::class.java
                )
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_about

    override fun canBack() = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()

        binding.layoutTerms.ninjaTap {
            WebViewActivity.openActivity(this, TERMS_AND_CONDITION_URL)
        }.addTo(disposables)

        binding.layoutPrivacy.ninjaTap {
            WebViewActivity.openActivity(this, PRIVACY_POLICY_URL)
        }.addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarTitle(getString(R.string.about))
    }
}
