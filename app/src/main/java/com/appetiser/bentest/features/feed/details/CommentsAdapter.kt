package com.appetiser.bentest.features.feed.details

import android.content.Context
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BasePagingAdapter
import com.appetiser.bentest.databinding.ItemCommentBinding
import com.appetiser.module.common.getTimeSpanString
import com.appetiser.module.common.widget.CustomTypefaceSpan
import com.appetiser.module.domain.models.feed.comment.Comment
import com.google.gson.Gson

class CommentsAdapter constructor(private val context: Context, gson: Gson) : BasePagingAdapter<Comment, BasePagingAdapter.BaseViewHolder<Comment>>(CommentDiffCallback(gson)) {

    val regularTypeface = ResourcesCompat.getFont(context, R.font.inter_regular)!!

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Comment> {
        return createCommentViewHolder(parent)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Comment>, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    private fun createCommentViewHolder(parent: ViewGroup): CommentItemViewHolder {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_comment, parent, false)

        val binding = ItemCommentBinding.bind(view)

        return CommentItemViewHolder(binding)
    }

    inner class CommentItemViewHolder(override val binding: ItemCommentBinding) : BaseViewHolder<Comment>(binding) {
        override fun bind(item: Comment) {

            binding.name.movementMethod = LinkMovementMethod.getInstance()
            val time = item.createdAt?.getTimeSpanString().toString()

            val data = "${item.author.fullName} $time"

            val ssb = SpannableStringBuilder(data)

            // user time
            ssb.setSpan(
                CustomTypefaceSpan(regularTypeface),
                item.author.fullName.length + 1,
                data.length,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )

            ssb.setSpan(
                RelativeSizeSpan(0.7f),
                item.author.fullName.length + 1,
                data.length,
                Spanned.SPAN_EXCLUSIVE_INCLUSIVE
            )

            binding.name.text = ssb

            super.bind(item)
        }
    }
}
