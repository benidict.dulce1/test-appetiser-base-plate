package com.appetiser.bentest.features.auth.register.phonenumber

import com.appetiser.module.domain.models.user.User

sealed class PhoneNumberInputState {
    data class Error(val throwable: Throwable) : PhoneNumberInputState()

    data class UserDetailsUpdated(val user: User) : PhoneNumberInputState()

    object ShowProgressLoading : PhoneNumberInputState()

    object HideProgressLoading : PhoneNumberInputState()
}
