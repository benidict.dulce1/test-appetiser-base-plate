package com.appetiser.bentest.features.auth.register

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import com.appetiser.bentest.R
import com.appetiser.bentest.ext.loadImageUrl
import com.appetiser.module.domain.models.auth.CountryCode
import com.appetiser.module.network.BASE_URL

class CountryAdapter(
    private val ctx: Context,
    private val res: Int,
    textViewId: Int,
    private val list: List<CountryCode>
) : ArrayAdapter<CountryCode>(ctx, res, textViewId, list) {

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View? {

        var item = convertView

        if (item == null) {
            val inflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            item = inflater.inflate(res, parent, false)
        }

        val countryFlag = item?.findViewById<AppCompatImageView>(R.id.flag)
        val countryCode = item?.findViewById<TextView>(R.id.countryCode)

        val flag = list[position].flag
        countryFlag?.loadImageUrl(list[position].getFlagUrl())
        if (flag.isNotEmpty()) {
            countryFlag?.loadImageUrl("http://159.89.203.124/$flag")
        }

        countryCode?.text = list[position].callingCode

        return item
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var item = convertView

        if (item == null) {
            val inflater = ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            item = inflater.inflate(res, parent, false)
        }

        val countryFlag = item?.findViewById<AppCompatImageView>(R.id.flag)
        val countryCode = item?.findViewById<TextView>(R.id.countryCode)

        val flag = list[position].flag
        countryFlag?.loadImageUrl(list[position].getFlagUrl())
        if (flag.isNotEmpty()) {
            countryFlag?.loadImageUrl("https://baseplate-api.appetiserdev.tech/$flag")
        }

        countryCode?.text = list[position].callingCode

        return item!!
    }
}

fun CountryCode.getFlagUrl(): String {
    return "$BASE_URL$flag"
}
