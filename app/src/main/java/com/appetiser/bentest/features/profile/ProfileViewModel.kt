package com.appetiser.bentest.features.profile

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.module.data.features.session.SessionRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    private val sessionRepository: SessionRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<ProfileState>()
    }

    val state: Observable<ProfileState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun loadAccount() {
        sessionRepository
            .getSession()
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    _state.onNext(ProfileState.UserProfileSession(session.user))
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }
}
