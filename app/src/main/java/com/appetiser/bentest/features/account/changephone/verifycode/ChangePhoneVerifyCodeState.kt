package com.appetiser.bentest.features.account.changephone.verifycode

sealed class ChangePhoneVerifyCodeState {
    object HideKeyboard : ChangePhoneVerifyCodeState()

    object ShowLoading : ChangePhoneVerifyCodeState()

    object HideLoading : ChangePhoneVerifyCodeState()

    object ResendCodeSuccess : ChangePhoneVerifyCodeState()

    object VerificationSuccess : ChangePhoneVerifyCodeState()

    data class DisplayPhone(val phoneNumber: String) : ChangePhoneVerifyCodeState()

    data class InvalidVerificationCode(val message: String) : ChangePhoneVerifyCodeState()

    data class Error(val message: String) : ChangePhoneVerifyCodeState()
}
