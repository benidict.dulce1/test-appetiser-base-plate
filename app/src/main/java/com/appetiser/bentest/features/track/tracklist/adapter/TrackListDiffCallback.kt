package com.appetiser.bentest.features.track.tracklist.adapter

import androidx.recyclerview.widget.DiffUtil
import com.appetiser.module.domain.models.track.Track

class TrackListDiffCallback  : DiffUtil.ItemCallback<Track>(){
    override fun areItemsTheSame(oldItem: Track, newItem: Track): Boolean
        = oldItem.artistId == newItem.artistId


    override fun areContentsTheSame(oldItem: Track, newItem: Track): Boolean
        = oldItem.artistId == newItem.artistId

}