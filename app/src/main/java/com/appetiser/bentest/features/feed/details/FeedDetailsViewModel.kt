package com.appetiser.bentest.features.feed.details

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.rxjava2.cachedIn
import androidx.paging.rxjava2.observable
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.features.feed.details.FeedDetailsActivity.Companion.KEY_FEED_ID
import com.appetiser.bentest.features.feed.details.paging.CommentsPagingSource
import com.appetiser.module.data.features.comment.CommentRepository
import com.appetiser.module.data.features.feeds.FeedRepository
import com.appetiser.module.domain.models.feed.Feed
import com.appetiser.module.domain.models.feed.comment.Comment
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class FeedDetailsViewModel @Inject constructor(
    private val repository: FeedRepository,
    private val commentRepository: CommentRepository
) : BaseViewModel() {

    private var feedId: Long = 0
    private val _feedDetailsState by lazy {
        PublishSubject.create<FeedDetailsState>()
    }

    val feedState: Observable<FeedDetailsState> = _feedDetailsState

    private val _commentState by lazy {
        PublishSubject.create<CommentState>()
    }

    val commentState: Observable<CommentState> = _commentState

    private val _loadingVisibility by lazy {
        MutableLiveData<Boolean>()
    }

    val loadingVisibility: LiveData<Boolean> = _loadingVisibility
    private lateinit var feed: Feed

    private val pager by lazy {
        Pager(
            config = PagingConfig(
                pageSize = 20,
                prefetchDistance = 5,
                initialLoadSize = 20
            ),
            pagingSourceFactory = {
                CommentsPagingSource(commentRepository, schedulers, feedId)
            }
        )
    }

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        val isCommentClicked = bundle?.getBoolean(FeedDetailsActivity.KEY_COMMENT_CLICKED, false)
            ?: false
        feedId = bundle?.getLong(KEY_FEED_ID, -1) ?: -1
        fetchFeed()
        fetchComments(isCommentClicked)
    }

    private fun fetchFeed() {
        repository.getFeedDetails(feedId).toObservable()
            .delay(1000, TimeUnit.MILLISECONDS)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _loadingVisibility.value = true
            }
            .subscribeBy(
                onNext = {
                    feed = it
                    _loadingVisibility.value = false
                    _feedDetailsState.onNext(FeedDetailsState.GetFeed(it))
                },
                onError = {
                    _loadingVisibility.value = false
                    _feedDetailsState.onNext(FeedDetailsState.Error(it))
                }
            )
            .addTo(disposables)
    }

    private fun fetchComments(isCommentClicked: Boolean) {
        pager.observable.cachedIn(viewModelScope)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onNext = {
                    _commentState.onNext(CommentState.GetComments(it))

                    if (isCommentClicked) {
                        _commentState.onNext(CommentState.ShowComment)
                    }
                },
                onError = {
                    _commentState.onNext(CommentState.Error(it))
                }
            )
            .addTo(disposables)
    }

    fun toggleFavorite(hapticFeedbackEnabled: Boolean = true) {
        val observable = if (feed.isFavorite) repository.unFavorite(feed.id) else repository.favorite(feed.id)

        observable
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                if (hapticFeedbackEnabled) {
                    val isFeedFavorite = !feed.isFavorite
                    val favoriteCount = if (isFeedFavorite) {
                        feed.favoritesCount + 1
                    } else {
                        if (feed.favoritesCount < 0) {
                            feed.favoritesCount
                        } else {
                            feed.favoritesCount - 1
                        }
                    }

                    val updateFeed = feed.copy(isFavorite = isFeedFavorite, favoritesCount = favoriteCount)
                    _feedDetailsState.onNext(FeedDetailsState.UpdateFeed(updateFeed))
                }
            }
            .subscribeBy(
                onSuccess = {
                    Timber.d("Error $it")
                },
                onError = {
                    Timber.e("Error $it")
                    _feedDetailsState.onNext(FeedDetailsState.Error(it))
                }
            )
            .addTo(disposables)
    }

    fun postComment(text: String) {
        if (text.isEmpty()) {
            return
        }

        Observable.combineLatest<Comment, Feed, Pair<Feed, Comment>>(
            commentRepository
                .postComment(feed.id, text).toObservable(),
            repository.getFeedDetails(feed.id).toObservable(),
            BiFunction { comment, feed ->
                return@BiFunction Pair(feed, comment)
            }
        )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _commentState.onNext(CommentState.AddingComment)
            }
            .subscribeBy(
                onNext = {
                    feed = it.first
                    _feedDetailsState.onNext(FeedDetailsState.UpdateFeed(feed))

                    _commentState.onNext(CommentState.AddCommentSuccess(it.second))
                    _commentState.onNext(CommentState.ShowComment)
                },
                onError = {
                    _commentState.onNext(CommentState.Error(it))
                }
            )
            .addTo(disposables)
    }
}
