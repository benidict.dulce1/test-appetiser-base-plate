package com.appetiser.bentest.features.track

import com.appetiser.module.domain.models.track.Track

sealed class TrackDetailsState{
    data class TrackDetails(val track: Track): TrackDetailsState()
    object TrackDetailsDeleted: TrackDetailsState()
    data class TrackDetailsError(val throwable: Throwable): TrackDetailsState()
}