package com.appetiser.bentest.features.main

sealed class MainState {

    object ShowProgressLoading : MainState()

    object HideProgressLoading : MainState()
}
