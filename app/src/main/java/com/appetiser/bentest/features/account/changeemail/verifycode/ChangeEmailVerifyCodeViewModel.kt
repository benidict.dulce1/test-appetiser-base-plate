package com.appetiser.bentest.features.account.changeemail.verifycode

import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class ChangeEmailVerifyCodeViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<ChangeEmailVerifyCodeState>()
    }

    val state: Observable<ChangeEmailVerifyCodeState> = _state

    private lateinit var email: String
    private lateinit var verificationToken: String

    /**
     * Copy of previously failed code.
     *
     * This is so we don't repetitively send wrong code when user inputs the same one.
     */
    private var failedCode: String = ""

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun setEmailAndVerificationToken(
        email: String,
        verificationToken: String
    ) {
        this.email = email
        this.verificationToken = verificationToken

        _state
            .onNext(
                ChangeEmailVerifyCodeState.DisplayEmail(
                    email
                )
            )
    }

    fun onCodeTextChanged(code: String) {
        if (code.length < 5 || code == failedCode) return

        authRepository
            .verifyChangeEmail(
                verificationToken,
                code
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state
                    .onNext(
                        ChangeEmailVerifyCodeState.HideKeyboard
                    )
                showLoading()
            }
            .doOnComplete {
                hideLoading()
            }
            .doOnError {
                hideLoading()
            }
            .subscribeBy(
                onComplete = {
                    _state
                        .onNext(
                            ChangeEmailVerifyCodeState.VerificationSuccess
                        )
                },
                onError = { error ->
                    Timber.e(error)
                    handleVerificationError(error, code)
                }
            )
            .addTo(disposables)
    }

    private fun handleVerificationError(error: Throwable, code: String) {
        if (error is HttpException && error.code() == 400) {
            failedCode = code
            _state
                .onNext(
                    ChangeEmailVerifyCodeState.InvalidVerificationCode(
                        error.getThrowableError()
                    )
                )
        } else {
            showGenericError()
        }
    }

    fun resendCode() {
        authRepository
            .requestChangeEmail(
                verificationToken,
                email
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                showLoading()
            }
            .doOnComplete {
                hideLoading()
            }
            .doOnError {
                hideLoading()
            }
            .subscribeBy(
                onComplete = {
                    _state
                        .onNext(
                            ChangeEmailVerifyCodeState.ResendCodeSuccess
                        )
                },
                onError = {
                    Timber.e(it)
                    showGenericError()
                }
            )
            .addTo(disposables)
    }

    private fun showLoading() {
        _state
            .onNext(
                ChangeEmailVerifyCodeState.ShowLoading
            )
    }

    private fun hideLoading() {
        _state
            .onNext(
                ChangeEmailVerifyCodeState.HideLoading
            )
    }

    private fun showGenericError() {
        _state
            .onNext(
                ChangeEmailVerifyCodeState.Error(
                    resourceManager.getString(
                        R.string.generic_error_short
                    )
                )
            )
    }
}
