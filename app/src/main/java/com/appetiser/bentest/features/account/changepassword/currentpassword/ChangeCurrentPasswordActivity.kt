package com.appetiser.bentest.features.account.changepassword.currentpassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityChangeCurrentPasswordBinding
import com.appetiser.bentest.features.account.changepassword.newpassword.ChangeNewPasswordActivity
import com.appetiser.module.common.*
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangeCurrentPasswordActivity : BaseViewModelActivity<ActivityChangeCurrentPasswordBinding, ChangeCurrentPasswordViewModel>() {

    companion object {

        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    ChangeCurrentPasswordActivity::class.java
                )
            )
        }
    }

    override fun canBack(): Boolean = true

    override fun getLayoutId() = R.layout.activity_change_current_password

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setupViews()
        setupVmObservers()
    }

    private fun setupViews() {
        binding.passwordMaxLength = PASSWORD_MAX_LENGTH

        binding
            .inputPassword
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel.onPasswordTextChanged(text.toString())
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnContinue
            .ninjaTap {
                binding.inputLayoutPassword.error = ""
                binding.root.hideKeyboard()
                viewModel
                    .verifyPassword(
                        binding.inputPassword.text.toString().trim()
                    )
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: ChangeCurrentPasswordState) {
        when (state) {
            ChangeCurrentPasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            ChangeCurrentPasswordState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            ChangeCurrentPasswordState.ShowLoading -> {
                binding.loading setVisible true
            }
            ChangeCurrentPasswordState.HideLoading -> {
                binding.loading setVisible false
            }
            ChangeCurrentPasswordState.PasswordBelowMinLength -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.password_below_minimum_format)
            }
            ChangeCurrentPasswordState.PasswordExceedsMaxLength -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.password_exceeds_maximum_format)
            }
            ChangeCurrentPasswordState.InvalidPassword -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.invalid_password)
            }
            is ChangeCurrentPasswordState.VerificationSuccess -> {
                ChangeNewPasswordActivity
                    .openActivity(
                        this,
                        Bundle().apply {
                            putString(
                                ChangeNewPasswordActivity.EXTRA_VERIFICATION_TOKEN,
                                state.verificationToken
                            )
                            putString(
                                ChangeNewPasswordActivity.EXTRA_OLD_PASSWORD,
                                binding.inputPassword.text.toString().trim()
                            )
                        }
                    )
            }
            is ChangeCurrentPasswordState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }
}
