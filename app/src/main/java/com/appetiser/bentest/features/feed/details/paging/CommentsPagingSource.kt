package com.appetiser.bentest.features.feed.details.paging

import androidx.paging.rxjava2.RxPagingSource
import com.appetiser.bentest.utils.schedulers.BaseSchedulerProvider
import com.appetiser.module.data.features.comment.CommentRepository
import com.appetiser.module.domain.models.feed.comment.Comment
import io.reactivex.Single
import javax.inject.Inject

class CommentsPagingSource @Inject constructor(
    private val commentRepository: CommentRepository,
    private val schedulers: BaseSchedulerProvider,
    private val feedId: Long
) : RxPagingSource<Int, Comment>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, Comment>> {
        val page = params.key ?: 1
        return commentRepository.getComments(feedId, page)
            .subscribeOn(schedulers.io())
            .map {
                LoadResult.Page(
                    it.list,
                    null,
                    it.nextPage
                )
            }
    }
}
