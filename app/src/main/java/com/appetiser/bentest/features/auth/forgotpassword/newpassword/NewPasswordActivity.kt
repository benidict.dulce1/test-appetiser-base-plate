package com.appetiser.bentest.features.auth.forgotpassword.newpassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityNewPasswordBinding
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.features.auth.login.LoginActivity
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.common.widget.CustomPasswordTransformation
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.appetiser.module.domain.utils.PASSWORD_MIN_LENGTH
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class NewPasswordActivity : BaseViewModelActivity<ActivityNewPasswordBinding, NewPasswordViewModel>() {

    companion object {
        fun openActivity(context: Context, username: String, token: String) {
            val intent = Intent(context, NewPasswordActivity::class.java)
            intent.putExtra(KEY_USERNAME, username)
            intent.putExtra(KEY_TOKEN, token)
            context.startActivity(intent)
        }

        const val KEY_USERNAME = "username"
        const val KEY_TOKEN = "token"
    }

    override fun getLayoutId(): Int = R.layout.activity_new_password

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViews()
        setupToolbar()
        setupViewModels()
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupViews() {
        binding.passwordMaxLength = PASSWORD_MAX_LENGTH
        binding.etPassword.apply {
            transformationMethod = CustomPasswordTransformation()
        }

        binding.etPassword.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    if (binding.btnContinue.isEnabled) {
                        viewModel.sendNewPassword(text.toString())
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding
            .btnContinue
            .ninjaTap {
                viewModel
                    .sendNewPassword(
                        binding.etPassword.text.toString().trim()
                    )
            }
            .addTo(disposables)
    }

    private fun setupViewModels() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    when (state) {

                        is NewPasswordState.GetEmail -> {
                            binding.tvEmail.apply {
                                text = state.email
                            }
                        }

                        is NewPasswordState.Success -> {
                            toast("Password successfully changed!")
                            LoginActivity.openActivity(this, state.email)
                            finishAffinity()
                        }

                        is NewPasswordState.Error -> {
                            toast(state.throwable.getThrowableError())
                        }

                        is NewPasswordState.ShowProgressLoading -> {
                            toast("Sending request")
                        }

                        is NewPasswordState.HideProgressLoading -> {
                        }

                        NewPasswordState.PasswordBelowMinLength -> {
                            binding
                                .etPasswordLayout
                                .error = getString(R.string.password_below_minimum_format, PASSWORD_MIN_LENGTH)
                        }
                        NewPasswordState.PasswordExceedsMaxLength -> {
                            binding
                                .etPasswordLayout
                                .error = getString(R.string.password_exceeds_maximum_format, PASSWORD_MAX_LENGTH)
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                }
            ).apply { disposables.add(this) }
    }
}
