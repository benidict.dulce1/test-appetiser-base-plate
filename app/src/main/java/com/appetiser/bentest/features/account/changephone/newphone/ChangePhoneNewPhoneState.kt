package com.appetiser.bentest.features.account.changephone.newphone

sealed class ChangePhoneNewPhoneState {
    object EnableButton : ChangePhoneNewPhoneState()

    object DisableButton : ChangePhoneNewPhoneState()

    object ShowLoading : ChangePhoneNewPhoneState()

    object HideLoading : ChangePhoneNewPhoneState()

    data class ChangePhoneSuccess(
        val phoneNumber: String,
        val verificationToken: String
    ) : ChangePhoneNewPhoneState()

    data class PhoneNumberError(val message: String) : ChangePhoneNewPhoneState()

    data class Error(val message: String) : ChangePhoneNewPhoneState()
}
