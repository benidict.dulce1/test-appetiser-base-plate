package com.appetiser.bentest.features.track.tracklist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseListAdapter
import com.appetiser.bentest.databinding.ItemTrackBinding
import com.appetiser.bentest.utils.loadImage
import com.appetiser.module.domain.models.track.Track

class TrackListAdapter (
    val context: Context,
    private val actionItem: (Track) -> Unit
): BaseListAdapter<Track, BaseListAdapter.BaseViewViewHolder<Track>>(TrackListDiffCallback()){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewViewHolder<Track> {
        return createTrackViewHolder(parent)
    }

    override fun onBindViewHolder(holder: BaseViewViewHolder<Track>, position: Int) {
        holder.bind(items[position])
    }

    private fun createTrackViewHolder(parent: ViewGroup): TrackItemViewHolder{
        val binding = ItemTrackBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_track, parent, false))
        return TrackItemViewHolder(binding)
    }

    inner class TrackItemViewHolder(override val binding: ItemTrackBinding): BaseViewViewHolder<Track>(binding){
        override fun bind(item: Track) {
            super.bind(item)
            binding.tvTrackName.text = if(item.trackName.isNullOrEmpty()) "N/A" else item.trackName
            binding.tvGenre.text = item.primaryGenreName
            if (!item.trackPrice.isNullOrEmpty()){
                binding.tvPrice.text = "$ ${item.trackPrice}"
            }else{
                binding.tvPrice.text = "$ 0.00"
            }
            binding.ivArtWork.loadImage(item.artworkUrl60)
            binding.rootLayout.setOnClickListener {
                actionItem(item)
            }
        }
    }
}