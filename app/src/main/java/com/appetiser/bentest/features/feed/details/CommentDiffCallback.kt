package com.appetiser.bentest.features.feed.details

import androidx.recyclerview.widget.DiffUtil
import com.appetiser.module.domain.models.feed.comment.Comment
import com.google.gson.Gson

class CommentDiffCallback(private val gson: Gson) : DiffUtil.ItemCallback<Comment>() {

    override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return gson.toJson(oldItem) == gson.toJson(newItem)
    }
}
