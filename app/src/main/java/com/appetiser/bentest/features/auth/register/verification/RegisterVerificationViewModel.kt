package com.appetiser.bentest.features.auth.register.verification

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class RegisterVerificationViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<RegisterVerificationState>()
    }

    val state: Observable<RegisterVerificationState> = _state

    private lateinit var email: String
    private lateinit var phone: String

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun setEmailOrPhone(email: String, phone: String) {
        this.email = email
        this.phone = phone

        val username = if (email.isNotEmpty()) {
            email
        } else {
            phone
        }

        _state
            .onNext(
                RegisterVerificationState
                    .FillUsername(username)
            )
    }

    fun resendToken() {
        if (email.isNotEmpty()) {
            resendEmailVerification()
        } else {
            resendPhoneVerification()
        }
    }

    private fun resendEmailVerification() {
        repository
            .resendEmailVerificationCode()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(RegisterVerificationState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(RegisterVerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(RegisterVerificationState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state.onNext(RegisterVerificationState.ResendCodeSuccess)
                },
                onError = {
                    _state.onNext(RegisterVerificationState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }

    private fun resendPhoneVerification() {
        repository
            .resendMobilePhoneVerificationCode()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(RegisterVerificationState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(RegisterVerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(RegisterVerificationState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state.onNext(RegisterVerificationState.ResendCodeSuccess)
                },
                onError = {
                    _state.onNext(RegisterVerificationState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }

    fun sendCode(code: String) {
        if (email.isNotEmpty()) {
            verifyAccountEmail(code)
        } else {
            verifyAccountPhone(code)
        }
    }

    private fun verifyAccountEmail(code: String) {
        repository
            .verifyAccountEmail(code)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(RegisterVerificationState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(RegisterVerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(RegisterVerificationState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state.onNext(RegisterVerificationState.StartUserDetailsScreen)
                },
                onError = {
                    _state.onNext(RegisterVerificationState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }

    private fun verifyAccountPhone(code: String) {
        repository
            .verifyAccountMobilePhone(code)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(RegisterVerificationState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(RegisterVerificationState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(RegisterVerificationState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state.onNext(RegisterVerificationState.StartUserDetailsScreen)
                },
                onError = {
                    _state.onNext(RegisterVerificationState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }
}
