package com.appetiser.bentest.features.profile.editprofile

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.DatePicker
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseCameraActivity
import com.appetiser.bentest.databinding.ActivityEditProfileBinding
import com.appetiser.bentest.ext.loadAvatarUrl
import com.appetiser.bentest.utils.DateUtils
import com.appetiser.bentest.utils.FileUtils
import com.appetiser.module.common.*
import com.appetiser.module.domain.utils.PROFILE_PHOTO_MAX_DIMENSION_PX
import com.jakewharton.rxbinding3.widget.textChanges
import com.yalantis.ucrop.UCrop
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.io.File
import java.time.LocalDate
import java.util.concurrent.TimeUnit

class EditProfileActivity : BaseCameraActivity<ActivityEditProfileBinding, EditProfileViewModel>(), DatePickerDialog.OnDateSetListener {

    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    EditProfileActivity::class.java
                )
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_edit_profile

    override fun canBack(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupToolbar()
        setupViews()
        setupVmObservers()

        viewModel.loadProfile()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: EditProfileState) {
        when (state) {
            is EditProfileState.PreFillFields -> {
                preFillFields(state)
            }
            EditProfileState.EnableSaveButton -> {
                binding
                    .toolbar
                    .toolbarButton
                    .isEnabled = true
            }
            EditProfileState.DisableSaveButton -> {
                binding
                    .toolbar
                    .toolbarButton
                    .isEnabled = false
            }
            is EditProfileState.ShowDatePicker -> {
                showDatePickerDialog(
                    state.year,
                    state.month,
                    state.day
                )
            }
            is EditProfileState.DescriptionExceedsMaxLength -> {
                getString(
                    R.string.description_exceeds_maximum_format,
                    state.maxLength
                ).run {
                    binding
                        .inputLayoutDescription
                        .error = this
                }
            }
            EditProfileState.ShowLoading -> {
                binding.loading.setVisible(true)
            }
            EditProfileState.HideLoading -> {
                binding.loading.setVisible(false)
            }
            is EditProfileState.DisplayNewImage -> {
                binding
                    .imgProfile
                    .loadAvatarUrl(
                        state.imageLocalPath
                    )
            }
            is EditProfileState.DisplayDob -> {
                displayNewDob(
                    state.year,
                    state.month,
                    state.day
                )
            }
            EditProfileState.NavigateUp -> {
                finish()
            }
            EditProfileState.ShowUnsavedChangesDialog -> {
                showUnsavedChangesDialog()
            }
            EditProfileState.UpdateProfileSuccess -> {
                showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.your_changes_has_been_saved)
                )
            }
            is EditProfileState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun preFillFields(state: EditProfileState.PreFillFields) {
        binding
            .imgProfile
            .loadAvatarUrl(
                state.imageUrl
            )

        binding
            .inputName
            .apply {
                setText(
                    state.fullName
                )
                setSelection(
                    state.fullName.length
                )
            }

        binding
            .inputDescription
            .setText(
                state.description
            )
    }

    private fun displayNewDob(
        year: Int,
        month: Int,
        day: Int
    ) {
        val dateOfBirth = DateUtils
            .formatDate(
                LocalDate.of(
                    year,
                    month,
                    day
                )
            )

        binding
            .inputDob
            .setText(
                dateOfBirth
            )
    }

    private fun setupViews() {
        binding
            .viewDobClickArea
            .ninjaTap {
                viewModel.onDatePickerClick()
            }
            .addTo(disposables)

        binding
            .inputName
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel.onNameTextChanged(text.toString())
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnCamera
            .ninjaTap {
                showImagePicker()
            }
            .addTo(disposables)
    }

    private fun showImagePicker() {
        rxPermissions
            .requestEachCombined(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .doOnNext {
                when {
                    it.granted -> {
                        showPickerDialog()
                    }
                    else -> {
                        hidePickerDialog()
                    }
                }
            }
            .subscribe()
            .addTo(disposables)
    }

    private fun showDatePickerDialog(
        year: Int,
        month: Int,
        day: Int
    ) {
        val datePicker = DatePickerDialog(
            this,
            this,
            year,
            month - 1,
            day
        )

        datePicker.show()
    }

    private fun showUnsavedChangesDialog() {
        showConfirmDialog(
            getString(R.string.unsaved_changes_title),
            getString(R.string.unsaved_changes_body),
            getString(R.string.discard),
            getString(R.string.back),
            positiveClickListener = {
                super.onBackPressed()
            }
        )
    }

    override fun onDateSet(
        datePicker: DatePicker,
        year: Int,
        monthOfYear: Int,
        dayOfMonth: Int
    ) {
        viewModel
            .onDobSelected(
                year,
                monthOfYear + 1,
                dayOfMonth
            )
    }

    override fun setImagePathUrl(captureCameraPath: String) {
        val destinationUri =
            Uri.fromFile(
                FileUtils.createFileFromCacheDir(this)
            )
        UCrop
            .of(
                Uri.fromFile(File(captureCameraPath)),
                destinationUri
            )
            .withAspectRatio(1f, 1f)
            .withMaxResultSize(PROFILE_PHOTO_MAX_DIMENSION_PX, PROFILE_PHOTO_MAX_DIMENSION_PX)
            .start(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when {
            resultCode == Activity.RESULT_OK && requestCode == UCrop.REQUEST_CROP -> {
                viewModel
                    .onProfilePhotoChanged(
                        UCrop.getOutput(data!!)!!.path!!
                    )
            }
            resultCode == UCrop.RESULT_ERROR -> {
                if (data != null) {
                    toast(
                        UCrop.getError(data)?.message ?: getString(R.string.generic_error)
                    )
                } else {
                    toast(
                        getString(R.string.generic_error)
                    )
                }
            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun setupToolbar() {
        setToolbarTitle(
            getString(R.string.edit_profile)
        )

        binding
            .toolbar
            .toolbarButton
            .apply {
                text = getString(R.string.save)

                ninjaTap {
                    clearErrors()
                    binding
                        .root
                        .hideKeyboard()

                    viewModel
                        .saveProfile(
                            binding.inputName.text.toString().trim(),
                            binding.inputDescription.text.toString().trim()
                        )
                }.addTo(disposables)
            }
    }

    private fun clearErrors() {
        binding
            .inputLayoutName
            .error = ""

        binding
            .inputLayoutDescription
            .error = ""
    }

    override fun onBackPressed() {
        viewModel
            .onBackPressed(
                binding.inputName.text.toString().trim(),
                binding.inputDescription.text.toString().trim()
            )
    }
}
