package com.appetiser.bentest.features.account.changephone.verifypassword

import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class ChangePhoneVerifyPasswordViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<ChangePhoneVerifyPasswordState>()
    }

    val state: Observable<ChangePhoneVerifyPasswordState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun onPasswordTextChanged(password: String) {
        if (validatePasswordNotEmpty(password)) {
            _state
                .onNext(
                    ChangePhoneVerifyPasswordState.EnableButton
                )
        } else {
            _state
                .onNext(
                    ChangePhoneVerifyPasswordState.DisableButton
                )
        }
    }

    fun verifyPassword(password: String) {
        if (!validatePasswordNotEmpty(password)) return

        authRepository
            .verifyPassword(password)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state
                    .onNext(
                        ChangePhoneVerifyPasswordState.ShowLoading
                    )
            }
            .doOnSuccess {
                _state
                    .onNext(
                        ChangePhoneVerifyPasswordState.HideLoading
                    )
            }
            .doOnError {
                _state
                    .onNext(
                        ChangePhoneVerifyPasswordState.HideLoading
                    )
            }
            .subscribeBy(
                onSuccess = { verificationToken ->
                    _state
                        .onNext(
                            ChangePhoneVerifyPasswordState.VerificationSuccess(
                                verificationToken
                            )
                        )
                },
                onError = { error ->
                    Timber.e(error)
                    if (error is HttpException && error.code() == 400) {
                        _state
                            .onNext(
                                ChangePhoneVerifyPasswordState.InvalidPassword
                            )
                    } else {
                        showGenericError()
                    }
                }
            )
            .addTo(disposables)
    }

    private fun showGenericError() {
        _state
            .onNext(
                ChangePhoneVerifyPasswordState.Error(
                    resourceManager
                        .getString(
                            R.string.generic_error_short
                        )
                )
            )
    }

    private fun validatePasswordNotEmpty(password: String): Boolean {
        return password.isNotEmpty()
    }
}
