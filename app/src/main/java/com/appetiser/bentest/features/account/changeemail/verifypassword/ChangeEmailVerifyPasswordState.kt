package com.appetiser.bentest.features.account.changeemail.verifypassword

sealed class ChangeEmailVerifyPasswordState {
    object EnableButton : ChangeEmailVerifyPasswordState()

    object DisableButton : ChangeEmailVerifyPasswordState()

    object ShowLoading : ChangeEmailVerifyPasswordState()

    object HideLoading : ChangeEmailVerifyPasswordState()

    object InvalidPassword : ChangeEmailVerifyPasswordState()

    data class VerificationSuccess(
        val verificationToken: String
    ) : ChangeEmailVerifyPasswordState()

    data class Error(val message: String) : ChangeEmailVerifyPasswordState()
}
