package com.appetiser.bentest.features.account.about

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseActivity
import com.appetiser.bentest.databinding.ActivityWebViewBinding

class WebViewActivity : BaseActivity<ActivityWebViewBinding>() {

    companion object {

        const val KEY_ARGS_SELECTED = "args_selected"

        fun openActivity(context: Context, argsSelected: String) {
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra(KEY_ARGS_SELECTED, argsSelected)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId() = R.layout.activity_web_view

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val url = intent?.extras?.getString(KEY_ARGS_SELECTED, "").orEmpty()
        binding.aboutWebView.apply {
            settings.javaScriptEnabled = true
            loadUrl(url)
        }
    }
}
