package com.appetiser.bentest.features.auth.register.phonenumber

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityPhoneNumberInputBinding
import com.appetiser.bentest.ext.disabledWithAlpha
import com.appetiser.bentest.ext.enabledWithAlpha
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.features.auth.register.details.InputNameActivity
import com.appetiser.module.common.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class PhoneNumberInputActivity : BaseViewModelActivity<ActivityPhoneNumberInputBinding, PhoneNumberInputViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, PhoneNumberInputActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_phone_number_input
    }

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupViews()
        setupToolbar()
        setupViewModel()
        observeInputViews()
    }

    private fun observeInputViews() {
        val mobileObservable = binding.etMobile.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() }

        disposables.add(
            mobileObservable
                .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribe(
                    {
                        if (it) {
                            binding.btnContinue.enabledWithAlpha()
                        } else {
                            binding.btnContinue.disabledWithAlpha()
                        }
                    },
                    {
                        Timber.e(it)
                    }
                )
        )
    }

    private fun setupViewModel() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e("Error $it")
                }
            )
            .apply {
                disposables.add(this)
            }
    }

    private fun handleState(state: PhoneNumberInputState) {
        when (state) {
            is PhoneNumberInputState.UserDetailsUpdated -> {
                handleSuccessRegistration()
            }
            is PhoneNumberInputState.Error -> {
                toast(state.throwable.getThrowableError())
            }
            is PhoneNumberInputState.ShowProgressLoading -> {
                toast("Sending request")
            }
            is PhoneNumberInputState.HideProgressLoading -> {
            }
        }
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupViews() {
        binding.etMobile.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    savePhoneNumber()
                    true
                } else {
                    false
                }
            }
        }

        binding
            .btnContinue
            .ninjaTap {
                savePhoneNumber()
            }
            .addTo(disposables)
    }

    private fun savePhoneNumber() {
        val countryCode = binding.countryCodePicker.selectedCountryCode
        val countryNameCode = binding.countryCodePicker.selectedCountryNameCode
        val phoneNumber = binding.etMobile.text.toString()

        viewModel
            .savePhoneNumber(
                countryCode,
                countryNameCode,
                phoneNumber
            )
    }

    private fun handleSuccessRegistration() {
        InputNameActivity.openActivity(this)
        finish()
    }
}
