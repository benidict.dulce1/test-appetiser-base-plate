package com.appetiser.bentest.features.auth.register.verification

sealed class RegisterVerificationState {
    data class FillUsername(val username: String) : RegisterVerificationState()

    object StartUserDetailsScreen : RegisterVerificationState()

    @Deprecated("Not used for now.")
    object StartPhoneNumberInputScreen : RegisterVerificationState()

    object ResendCodeSuccess : RegisterVerificationState()

    data class Error(val throwable: Throwable) : RegisterVerificationState()

    object ShowProgressLoading : RegisterVerificationState()

    object HideProgressLoading : RegisterVerificationState()
}
