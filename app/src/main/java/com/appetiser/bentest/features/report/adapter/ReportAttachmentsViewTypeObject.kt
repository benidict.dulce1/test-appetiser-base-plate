package com.appetiser.bentest.features.report.adapter

data class ReportAttachmentsViewTypeObject(val type: ReportAttachmentsViewType, val imagePath: String)

enum class ReportAttachmentsViewType(val type: Int) {
    DISPLAY_ADD_BTN(0), DISPLAY_PHOTO(1)
}
