package com.appetiser.bentest.features.account.changepassword.newpassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityChangeNewPasswordBinding
import com.appetiser.bentest.features.account.settings.AccountSettingsActivity
import com.appetiser.module.common.*
import com.appetiser.module.common.widget.CustomPasswordTransformation
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangeNewPasswordActivity : BaseViewModelActivity<ActivityChangeNewPasswordBinding, ChangeNewPasswordViewModel>() {

    companion object {

        fun openActivity(context: Context, extras: Bundle) {
            context.startActivity(
                Intent(
                    context,
                    ChangeNewPasswordActivity::class.java
                ).apply {
                    putExtras(extras)
                }
            )
        }

        const val EXTRA_OLD_PASSWORD = "EXTRA_OLD_PASSWORD"
        const val EXTRA_VERIFICATION_TOKEN = "EXTRA_VERIFICATION_TOKEN"
    }

    override fun getLayoutId(): Int = R.layout.activity_change_new_password

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.setOldPasswordAndToken(
            intent.extras?.getString(ChangeNewPasswordActivity.EXTRA_OLD_PASSWORD, "").orEmpty(),
            intent.extras?.getString(ChangeNewPasswordActivity.EXTRA_VERIFICATION_TOKEN, "").orEmpty()
        )

        setupViews()
        setupToolbar()
        setupViewModels()
    }

    override fun canBack() = true

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupViews() {
        binding.passwordMaxLength = PASSWORD_MAX_LENGTH

        binding.inputPassword.apply {
            transformationMethod = CustomPasswordTransformation()
        }

        binding
            .inputPassword
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel.onPasswordTextChanged(text.toString())
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnContinue
            .ninjaTap {
                binding.inputLayoutPassword.error = ""
                binding.root.hideKeyboard()
                viewModel
                    .changePassword(
                        binding.inputPassword.text.toString().trim()
                    )
            }
            .addTo(disposables)
    }

    private fun setupViewModels() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            ).apply { disposables.add(this) }
    }

    private fun handleState(state: ChangeNewPasswordState) {
        when (state) {
            ChangeNewPasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            ChangeNewPasswordState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            ChangeNewPasswordState.ShowProgressLoading -> {
                binding.loading setVisible true
            }
            ChangeNewPasswordState.HideProgressLoading -> {
                binding.loading setVisible false
            }
            ChangeNewPasswordState.InvalidPassword -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.invalid_password)
            }
            is ChangeNewPasswordState.Success -> {
                AccountSettingsActivity
                    .openActivity(
                        this,
                        Bundle().apply {
                            putBoolean(
                                AccountSettingsActivity.EXTRA_FROM_CHANGE_PASSWORD,
                                true
                            )
                        },
                        Intent.FLAG_ACTIVITY_CLEAR_TOP
                    )
            }
            is ChangeNewPasswordState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }
}
