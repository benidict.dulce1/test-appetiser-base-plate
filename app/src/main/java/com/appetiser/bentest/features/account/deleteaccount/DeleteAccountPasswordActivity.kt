package com.appetiser.bentest.features.account.deleteaccount

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityDeleteAccountVerifyPasswordBinding
import com.appetiser.bentest.features.auth.FacebookLoginManager
import com.appetiser.bentest.features.auth.GoogleSignInManager
import com.appetiser.bentest.features.auth.walkthrough.WalkthroughActivity
import com.appetiser.module.common.*
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class DeleteAccountPasswordActivity : BaseViewModelActivity<ActivityDeleteAccountVerifyPasswordBinding, DeleteAccountPasswordViewModel>() {
    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    DeleteAccountPasswordActivity::class.java
                )
            )
        }
    }

    override fun canBack(): Boolean = true

    override fun getLayoutId(): Int = R.layout.activity_delete_account_verify_password

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.passwordMaxLength = PASSWORD_MAX_LENGTH

        setupToolbar()
        setupViews()
        setupVmObservers()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: DeleteAccountPasswordState) {
        when (state) {
            DeleteAccountPasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            DeleteAccountPasswordState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            DeleteAccountPasswordState.ShowLoading -> {
                binding.loading setVisible true
            }
            DeleteAccountPasswordState.HideLoading -> {
                binding.loading setVisible false
            }
            DeleteAccountPasswordState.InvalidPassword -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.invalid_password)
            }
            DeleteAccountPasswordState.LogoutSocialLogins -> {
                logoutSocialLogins()
            }
            is DeleteAccountPasswordState.NavigateToWalkthroughScreen -> {
                WalkthroughActivity.openActivity(this)
                finishAffinity()
            }
            is DeleteAccountPasswordState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun setupViews() {
        binding
            .inputPassword
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel.onPasswordTextChanged(text.toString())
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnContinue
            .ninjaTap {
                binding.inputLayoutPassword.error = ""
                binding.root.hideKeyboard()
                viewModel
                    .verifyPassword(
                        binding.inputPassword.text.toString().trim()
                    )
            }
            .addTo(disposables)
    }

    private fun logoutSocialLogins() {
        // Sign out google if it's signed in.
        if (GoogleSignInManager.isSignedIn(this)) {
            GoogleSignInManager(this)
                .run {
                    signOut()
                }
        }

        // Logout facebook if it's logged in.
        if (FacebookLoginManager.isLoggedIn()) {
            FacebookLoginManager.logout()
        }
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }
}
