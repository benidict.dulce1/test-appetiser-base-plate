package com.appetiser.bentest.features.notification

import com.appetiser.module.domain.models.notification.Notification

data class NotificationViewTypeItem(val type: NotificationType, val notification: Notification? = null, val title: String?) {

    val usernameLength: Int = notification?.actor?.fullName?.length ?: 0
    val messageLength: Int = notification?.message?.length ?: 0
    val timeLength: Int = notification?.created_at?.length ?: 0

    companion object {
        fun getNotificationType(type: String): NotificationType {
            return NotificationType.values().single { it.type == type }
        }
    }
}

enum class NotificationType(val type: String) {
    HEADER("header"), NOTIFICATION("notification")
}
