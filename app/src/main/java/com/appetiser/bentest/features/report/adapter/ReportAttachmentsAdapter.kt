package com.appetiser.bentest.features.report.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseListAdapter
import com.appetiser.bentest.databinding.ItemAddPhotoBtnBinding
import com.appetiser.bentest.databinding.ItemReportUserPhotoBinding
import com.appetiser.bentest.ext.loadImageUrl
import com.appetiser.module.common.ninjaTap

class ReportAttachmentsAdapter(
    val context: Context,
    val listener: OnReportListener
) : BaseListAdapter<ReportAttachmentsViewTypeObject, BaseListAdapter.BaseViewViewHolder<ReportAttachmentsViewTypeObject>>(ReportAttachmentsDiffCallback()) {

    companion object {
        private const val VIEW_TYPE_ADD_PHOTO_BTN = 0
        private const val VIEW_TYPE_DISPLAY_PHOTO = 1
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position].type) {
            ReportAttachmentsViewType.DISPLAY_ADD_BTN -> VIEW_TYPE_ADD_PHOTO_BTN
            ReportAttachmentsViewType.DISPLAY_PHOTO -> VIEW_TYPE_DISPLAY_PHOTO
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewViewHolder<ReportAttachmentsViewTypeObject> {
        return when (viewType) {
            VIEW_TYPE_ADD_PHOTO_BTN -> createAddPhotoHolder(parent)
            else -> createDisplayPhotoHolder(parent)
        }
    }

    override fun onBindViewHolder(holder: BaseViewViewHolder<ReportAttachmentsViewTypeObject>, position: Int) {
        holder.bind(items[position])
    }

    private fun createAddPhotoHolder(parent: ViewGroup): BaseViewViewHolder<ReportAttachmentsViewTypeObject> {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_add_photo_btn, parent, false)

        val binding = ItemAddPhotoBtnBinding.bind(view)

        return AddPhotoItemViewHolder(binding)
    }

    private fun createDisplayPhotoHolder(parent: ViewGroup): BaseViewViewHolder<ReportAttachmentsViewTypeObject> {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_report_user_photo, parent, false)

        val binding = ItemReportUserPhotoBinding.bind(view)

        return DisplayPhotoItemViewHolder(binding)
    }

    inner class AddPhotoItemViewHolder(override val binding: ItemAddPhotoBtnBinding) : BaseListAdapter.BaseViewViewHolder<ReportAttachmentsViewTypeObject>(binding) {
        override fun bind(item: ReportAttachmentsViewTypeObject) {
            super.bind(item)
            binding.addPhoto.ninjaTap {
                listener.addPhoto()
            }
        }
    }

    inner class DisplayPhotoItemViewHolder(override val binding: ItemReportUserPhotoBinding) : BaseListAdapter.BaseViewViewHolder<ReportAttachmentsViewTypeObject>(binding) {
        override fun bind(item: ReportAttachmentsViewTypeObject) {
            super.bind(item)
            binding.deletePhoto.ninjaTap {
                listener.deletePhoto(item)
            }

            binding.imageUserPhoto.loadImageUrl(item.imagePath)
        }
    }
}
