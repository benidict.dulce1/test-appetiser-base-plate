package com.appetiser.bentest.features.auth.emailcheck

sealed class EmailCheckState {

    data class EmailDoesNotExist(val email: String) : EmailCheckState()

    data class EmailExists(val email: String) : EmailCheckState()

    data class Error(val throwable: Throwable) : EmailCheckState()

    object ShowProgressLoading : EmailCheckState()

    object HideProgressLoading : EmailCheckState()
}
