package com.appetiser.bentest.features.feed.list.paging

import androidx.paging.rxjava2.RxPagingSource
import com.appetiser.bentest.utils.schedulers.BaseSchedulerProvider
import com.appetiser.module.data.features.feeds.FeedRepository
import com.appetiser.module.domain.models.feed.Feed
import io.reactivex.Single
import javax.inject.Inject

class FeedsPagingSource @Inject constructor(
    private val feedRepository: FeedRepository,
    private val schedulers: BaseSchedulerProvider
) : RxPagingSource<Int, Feed>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, Feed>> {
        val page = params.key ?: 1
        return feedRepository.getFeeds(page)
            .subscribeOn(schedulers.io())
            .map {
                LoadResult.Page(
                    it.list,
                    null,
                    it.nextPage
                )
            }
    }
}
