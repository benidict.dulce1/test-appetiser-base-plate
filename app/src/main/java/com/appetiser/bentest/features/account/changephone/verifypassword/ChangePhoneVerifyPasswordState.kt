package com.appetiser.bentest.features.account.changephone.verifypassword

sealed class ChangePhoneVerifyPasswordState {
    object EnableButton : ChangePhoneVerifyPasswordState()

    object DisableButton : ChangePhoneVerifyPasswordState()

    object ShowLoading : ChangePhoneVerifyPasswordState()

    object HideLoading : ChangePhoneVerifyPasswordState()

    object InvalidPassword : ChangePhoneVerifyPasswordState()

    data class VerificationSuccess(
        val verificationToken: String
    ) : ChangePhoneVerifyPasswordState()

    data class Error(val message: String) : ChangePhoneVerifyPasswordState()
}
