package com.appetiser.bentest.features.account.settings

import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.notification.NotificationRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.models.Session
import com.appetiser.module.notification.fcm.FirebaseRepository
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class AccountSettingsViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val sessionRepository: SessionRepository,
    private val notificationRepository: NotificationRepository,
    private val firebaseRepository: FirebaseRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<AccountSettingsState>()
    }

    val state: Observable<AccountSettingsState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        sessionRepository
            .getSession()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    handleSession(session)
                },
                onError = {}
            )
            .addTo(disposables)
    }

    private fun handleSession(session: Session) {
        _state
            .onNext(
                AccountSettingsState.DisplayDetails(
                    if (session.user.email.isNotEmpty()) {
                        session.user.email
                    } else {
                        resourceManager.getString(R.string.na)
                    },
                    if (session.user.phoneNumber.isNotEmpty()) {
                        "+${session.user.phoneNumber}"
                    } else {
                        resourceManager.getString(R.string.na)
                    }
                )
            )
    }

    fun setFromChangeEmail(fromChangeEmail: Boolean) {
        if (fromChangeEmail) {
            _state
                .onNext(
                    AccountSettingsState.ShowEmailChangeSuccess
                )
        }
    }

    fun setFromChangePhone(fromChangePhone: Boolean) {
        if (fromChangePhone) {
            _state
                .onNext(
                    AccountSettingsState.ShowPhoneChangeSuccess
                )
        }
    }

    fun setFromChangePassword(fromChangePassword: Boolean) {
        if (fromChangePassword) {
            _state.onNext(
                AccountSettingsState.ShowChangePasswordSuccess
            )
        }
    }

    fun logout(callback: () -> Unit) {
        firebaseRepository
            .getDeviceToken()
            .observeOn(schedulers.io())
            .flatMapCompletable {
                notificationRepository
                    .unRegisterDeviceToken(it, resourceManager.getDeviceId())
                    .flatMapCompletable {
                        authRepository.logout()
                    }
                    .andThen(
                        Completable.create { emitter ->
                            callback.invoke()
                            emitter.onComplete()
                        }
                    )
            }
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(AccountSettingsState.ShowLoading)
            }
            .doOnComplete {
                _state.onNext(AccountSettingsState.HideLoading)
            }
            .doOnError {
                _state.onNext(AccountSettingsState.HideLoading)
            }
            .subscribeBy(
                onComplete = {
                    _state
                        .onNext(
                            AccountSettingsState.NavigateToEmailCheck
                        )
                },
                onError = {
                    showGenericError()
                }
            )
            .addTo(disposables)
    }

    private fun showGenericError() {
        _state
            .onNext(
                AccountSettingsState.Error(
                    resourceManager
                        .getString(
                            R.string.generic_error_short
                        )
                )
            )
    }
}
