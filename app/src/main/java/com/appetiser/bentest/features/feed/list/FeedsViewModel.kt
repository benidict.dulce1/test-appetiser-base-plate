package com.appetiser.bentest.features.feed.list

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import androidx.paging.rxjava2.cachedIn
import androidx.paging.rxjava2.flowable
import androidx.paging.rxjava2.observable
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.features.feed.list.paging.FeedsPagingSource
import com.appetiser.module.data.features.feeds.FeedRepository
import com.appetiser.module.domain.models.feed.Feed
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class FeedsViewModel @Inject constructor(
    private val repository: FeedRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<FeedsState>()
    }

    val state: Observable<FeedsState> = _state

    private val _loadingVisibility by lazy {
        MutableLiveData<Boolean>()
    }

    val loadingVisibility: LiveData<Boolean> = _loadingVisibility

    private val pager by lazy {
        Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                FeedsPagingSource(repository, schedulers)
            }
        ).flowable
    }

    private lateinit var feedPagingData: PagingData<Feed>

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun fetchFeeds() {
        pager
            .cachedIn(viewModelScope)
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _loadingVisibility.value = true
            }
            .subscribeBy(
                onNext = {
                    _loadingVisibility.value = false
                    feedPagingData = it
                    _state.onNext(FeedsState.FeedsFetched(it))
                },
                onError = {
                    Timber.e("Error $it")
                    _state.onNext(FeedsState.Error(it))
                }
            )
            .addTo(disposables)
    }

    fun feedDetails(id: Long, position: Int) {
        repository.getFeedDetails(id)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = {
                    updateSingleFeed(it, position)
                },
                onError = {
                    _state.onNext(FeedsState.Error(it))
                }
            )
            .addTo(disposables)
    }

    fun toggleFavorite(feed: Feed, hapticFeedbackEnabled: Boolean = true, position: Int) {
        val observable = if (feed.isFavorite) repository.unFavorite(feed.id) else repository.favorite(feed.id)

        observable
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                if (hapticFeedbackEnabled) {
                    val isFeedFavorite = !feed.isFavorite
                    val favoriteCount = if (isFeedFavorite) {
                        feed.favoritesCount + 1
                    } else {
                        if (feed.favoritesCount < 0) {
                            feed.favoritesCount
                        } else {
                            feed.favoritesCount - 1
                        }
                    }
                    val updateFeed = feed.copy(isFavorite = isFeedFavorite, favoritesCount = favoriteCount)
                    updateSingleFeed(updateFeed, position)
                }
            }
            .subscribeBy(
                onSuccess = {
                    Timber.d("Error $it")
                },
                onError = {
                    Timber.e("Error $it")
                    _state.onNext(FeedsState.Error(it))
                }
            )
            .addTo(disposables)
    }

    fun deleteFeed(feed: Feed, position: Int) {
        repository.deleteFeed(feed.id)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { success ->
                    _state.onNext(FeedsState.RemoveFeed(feed, position))
                    Timber.d("Success $success")
                },
                onError = { error ->
                    _state.onNext(FeedsState.RefreshFeeds)
                    Timber.e("Error $error")
                    _state.onNext(FeedsState.Error(error))
                }
            )
            .addTo(disposables)
    }

    private fun updateSingleFeed(feed: Feed, position: Int) {
        _state.onNext(FeedsState.UpdateFeed(feed))
    }
}
