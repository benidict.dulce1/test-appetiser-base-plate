package com.appetiser.bentest.features.track.tracklist


import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.features.track.TrackState
import com.appetiser.module.data.features.track.TrackRepository
import com.appetiser.module.domain.models.track.Track
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class TrackListViewModel @Inject constructor(
    private val repository: TrackRepository
): BaseViewModel(){

    private val _state by lazy {
        PublishSubject.create<TrackState>()
    }
    val state: Observable<TrackState> = _state

    private val _loadingVisibility by lazy {
        MutableLiveData<Boolean>()
    }

    val loadingVisibility: LiveData<Boolean> = _loadingVisibility

    override fun isFirstTimeUiCreate(bundle: Bundle?){
        loadLastVisitDate()
        loadTrackList()
    }

    fun loadTrackList(){
        repository.loadTrackList()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .doOnSubscribe {
                _state.onNext(TrackState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(TrackState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(TrackState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    _state.onNext(TrackState.TrackListSuccess(it))

                },
                onError = { err ->
                    _state.onNext(TrackState.Error(err))
                }
            )
            .addTo(disposables)
    }

    fun saveLastVisitDate(date: String){
        repository.saveLastVisitDate(date)
    }

    fun loadLastVisitDate(){
        repository.loadLastVisitDate()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribeBy(
                onSuccess = {
                    _state.onNext(TrackState.LastVisitDateSuccess(it))
                }
            ).addTo(disposables)
    }

    fun saveTrackDetails(track: Track){
        repository.saveTrackDetails(track)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribeBy(
                onSuccess = {
                    _state.onNext(TrackState.TrackDetailsSuccess(it))
                }
            ).addTo(disposables)
    }
}