package com.appetiser.bentest.features.auth.landing

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.method.LinkMovementMethod
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityLandingBinding
import com.appetiser.bentest.features.auth.FacebookLoginManager
import com.appetiser.bentest.features.auth.GoogleSignInManager
import com.appetiser.bentest.features.auth.emailcheck.EmailCheckActivity
import com.appetiser.bentest.features.auth.phonenumbercheck.PhoneNumberCheckActivity
import com.appetiser.bentest.features.auth.register.details.InputNameActivity
import com.appetiser.bentest.features.main.MainActivity
import com.appetiser.module.common.*
import com.google.gson.Gson
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class LandingActivity : BaseViewModelActivity<ActivityLandingBinding, LandingViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    LandingActivity::class.java
                )
            )
        }
    }

    private lateinit var facebookLoginManager: FacebookLoginManager
    private lateinit var googleSigninManager: GoogleSignInManager

    override fun getLayoutId(): Int = R.layout.activity_landing

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        facebookLoginManager = FacebookLoginManager()
        googleSigninManager = GoogleSignInManager(this)

        setupButtonListeners()
        setupViewModel()
    }

    override fun onDestroy() {
        facebookLoginManager.clearListeners()
        googleSigninManager.clearListeners()
        super.onDestroy()
    }

    private fun setupViewModel() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    handleState(state)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: LandingState) {
        when (state) {
            is LandingState.SocialLoginSuccess -> {
                openMainActivity()
            }
            is LandingState.SocialLoginSuccessButNotOnboarded -> {
                openUserDetailsActivity()
            }
            is LandingState.ShowLoading -> {
                showLoadingState(state.loginType)
            }
            is LandingState.HideLoading -> {
                hideLoadingState()
            }
            is LandingState.Error -> {
                toast(getString(R.string.generic_error))
            }
        }
    }

    private fun setupButtonListeners() {
        setupEmailButton()
        setupPhoneButton()
        setupFacebookButton()
        setupGoogleButton()
        setupPrivacy()
    }

    private fun setupPrivacy() {
        binding.privacy.movementMethod = LinkMovementMethod.getInstance()
        binding.privacy.text = getString(R.string.auth_privacy_policy)
            .spannableString(
                this,
                null,
                R.font.inter_regular,
                Color.BLUE,
                "Terms of Service",
                "Privacy Policy",
                clickable = {
                    toast("Clicked $it")
                }
            )
    }

    private fun setupEmailButton() {
        binding
            .btnEmail
            .ninjaTap {
                EmailCheckActivity.openActivity(this)
            }
            .apply { disposables.add(this) }
    }

    private fun setupPhoneButton() {
        binding
            .btnPhone
            .ninjaTap {
                PhoneNumberCheckActivity
                    .openActivity(this)
            }
            .addTo(disposables)
    }

    private fun setupFacebookButton() {
        facebookLoginManager
            .setLoginSuccessListener { result ->
                Timber.d(Gson().toJson(result))
                viewModel.onFacebookLogin(result.accessToken.token)
            }

        facebookLoginManager
            .setLoginErrorListener {
                toast(getString(R.string.generic_error))
            }

        binding
            .btnFacebook
            .ninjaTap {
                Timber.d("clicked!")
                facebookLoginManager.login(this)
            }
            .apply { disposables.add(this) }
    }

    private fun setupGoogleButton() {
        googleSigninManager
            .setSignInSuccessListener { account ->
                Timber.d(Gson().toJson(account))
                viewModel.onGoogleSignIn(account.idToken!!)
            }

        googleSigninManager
            .setSignInErrorListener {
                toast(getString(R.string.generic_error))
            }

        binding
            .btnGoogle
            .ninjaTap {
                googleSigninManager.signIn()
            }
            .apply { disposables.add(this) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == GoogleSignInManager.REQUEST_CODE_GOOGLE_SIGN_IN) {
            googleSigninManager.sendSignInResult(data)
        } else {
            facebookLoginManager.sendLoginResult(requestCode, resultCode, data)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun showLoadingState(loginType: String) {
        when (loginType) {
            LandingViewModel.FACEBOOK -> {
                binding
                    .progressFacebook
                    .setVisible(true)

                binding
                    .imgFacebook
                    .setHidden(true)
            }
            LandingViewModel.GOOGLE -> {
                binding
                    .progressGoggle
                    .setVisible(true)

                binding
                    .imgGoogle
                    .setHidden(true)
            }
        }

        binding
            .btnEmail
            .apply {
                alpha = 0.5f
                isEnabled = false
            }

        binding
            .btnPhone
            .apply {
                alpha = 0.5f
                isEnabled = false
            }

        binding
            .btnFacebook
            .apply {
                alpha = 0.5f
                isEnabled = false
            }

        binding
            .btnGoogle
            .apply {
                alpha = 0.5f
                isEnabled = false
            }
    }

    private fun hideLoadingState() {
        binding.progressFacebook.setVisible(false)
        binding.progressGoggle.setVisible(false)
        binding.imgFacebook.setHidden(false)
        binding.imgGoogle.setHidden(false)

        binding
            .btnEmail
            .apply {
                alpha = 1f
                isEnabled = true
            }

        binding
            .btnPhone
            .apply {
                alpha = 1f
                isEnabled = true
            }

        binding
            .btnFacebook
            .apply {
                alpha = 1f
                isEnabled = true
            }

        binding
            .btnGoogle
            .apply {
                alpha = 1f
                isEnabled = true
            }
    }

    private fun openMainActivity() {
        MainActivity.openActivity(this)
        finishAffinity()
    }

    private fun openUserDetailsActivity() {
        InputNameActivity.openActivity(this)
        finishAffinity()
    }
}
