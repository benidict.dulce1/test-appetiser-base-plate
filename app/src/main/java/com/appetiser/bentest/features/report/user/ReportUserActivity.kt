package com.appetiser.bentest.features.report.user

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseCameraActivity
import com.appetiser.bentest.databinding.ActivityReportUserBinding
import com.appetiser.bentest.features.report.adapter.*
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.domain.models.miscellaneous.ReportCategory
import com.appetiser.module.domain.models.miscellaneous.ReportedUser
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class ReportUserActivity : BaseCameraActivity<ActivityReportUserBinding, ReportUserViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, ReportUserActivity::class.java)
            context.startActivity(intent)
        }
    }

    private lateinit var reportCategoryAdapter: ReportCategoryAdapter

    private val adapter: ReportAttachmentsAdapter by lazy {
        ReportAttachmentsAdapter(this, onItemClickListener)
    }

    private val onItemClickListener = object : OnReportListener {
        override fun addPhoto() {
            launchPhotoChooser()
        }

        override fun deletePhoto(item: ReportAttachmentsViewTypeObject) {
            deleteUploadedPhoto(item)
        }
    }

    override fun getLayoutId() = R.layout.activity_report_user

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViewModel()
        setupRecyclerView()

        binding.btnReport.ninjaTap {
            reportUser(
                this@ReportUserActivity,
                "47",
                reportCategoryAdapter.getItem(binding.spinnerReason.selectedItemPosition)?.id.toString(),
                binding.etDescription.text.toString()
            )
        }.addTo(disposables)

        binding.btnClose.ninjaTap {
            finish()
        }.addTo(disposables)
    }

    private fun setupViewModel() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }

    private fun handleState(state: ReportUserState) {
        when (state) {
            is ReportUserState.DisplayAddButton -> handleDisplayAddButton()
            is ReportUserState.AddPhoto -> handleAddPhoto(state)
            is ReportUserState.RemovePhoto -> handleRemovePhoto(state)
            is ReportUserState.ReportCategoriesFetched -> handleReportCategoriesSuccess(state.reportCategories)
            is ReportUserState.ReportSuccess -> handleReportSuccess(state.reportedUser)
            is ReportUserState.ShowProgressLoading -> handleShowProgress()
            is ReportUserState.HideProgressLoading -> handleHideProgress()
            is ReportUserState.Error -> handleError(state.throwable.message)
        }
    }

    private fun handleReportSuccess(reportedUser: ReportedUser) {
        toast("Report submitted!")
        finish()
    }

    private fun handleHideProgress() {
        // TODO: Handle hide progress
    }

    private fun handleShowProgress() {
        toast("Sending request")
    }

    private fun handleRemovePhoto(state: ReportUserState.RemovePhoto) {
        adapter.removeSingleItem(state.item)
    }

    private fun handleDisplayAddButton() {
        adapter.addSingleItem(ReportAttachmentsViewTypeObject(ReportAttachmentsViewType.DISPLAY_ADD_BTN, ""))
    }

    private fun handleAddPhoto(state: ReportUserState.AddPhoto) {
        adapter.addSingleItem(ReportAttachmentsViewTypeObject(ReportAttachmentsViewType.DISPLAY_PHOTO, state.imagePath))
    }

    private fun handleError(message: String?) {
        Toast.makeText(this@ReportUserActivity, message, Toast.LENGTH_SHORT).show()
    }

    private fun handleReportCategoriesSuccess(reportCategories: List<ReportCategory>) {
        binding
            .spinnerReason
            .apply {
                reportCategoryAdapter = ReportCategoryAdapter(this@ReportUserActivity, reportCategories)
                adapter = reportCategoryAdapter
            }
    }

    private fun setupRecyclerView() {
        with(binding.photoList) {
            layoutManager = GridLayoutManager(this@ReportUserActivity, 3)
            adapter = this@ReportUserActivity.adapter
        }
        viewModel.displayAddPhotoButton(ReportAttachmentsViewType.DISPLAY_ADD_BTN)
    }

    private fun deleteUploadedPhoto(item: ReportAttachmentsViewTypeObject) {
        viewModel.removePhoto(item)
    }

    private fun launchPhotoChooser() {
        rxPermissions.requestEachCombined(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
            .doOnNext {
                when {
                    it.granted -> {
                        showPickerDialog()
                    }
                    else -> {
                        hidePickerDialog()
                    }
                }
            }
            .subscribe()
    }

    override fun setImagePathUrl(captureCameraPath: String) {
        viewModel.addPhoto(captureCameraPath)
    }

    private fun reportUser(
        context: Context,
        reportedUserId: String,
        reasonId: String,
        description: String
    ) {
        viewModel.reportUser(
            context, reportedUserId,
            reasonId, description
        )
    }
}
