package com.appetiser.bentest.features.auth.login

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.notification.NotificationRepository
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.notification.fcm.FirebaseRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val repository: AuthRepository,
    private val notificationRepository: NotificationRepository,
    private val firebaseRepository: FirebaseRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private var email: String = ""
    private var phone: String = ""

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        email = bundle?.getString(LoginActivity.KEY_EMAIL, "").orEmpty()
        phone = bundle?.getString(LoginActivity.KEY_PHONE, "").orEmpty()
    }

    private val _state by lazy {
        PublishSubject.create<LoginState>()
    }

    val state: Observable<LoginState> = _state

    fun requestArguments() {
        val username = when {
            email.isNotEmpty() -> email
            phone.isNotEmpty() -> phone
            else -> ""
        }

        if (username.isEmpty()) return

        _state
            .onNext(
                LoginState.GetUsername(username)
            )
    }

    fun login(email: String, password: String) {
        repository
            .login(email, password)
            .flatMap { session ->
                firebaseRepository
                    .getDeviceToken()
                    .observeOn(schedulers.io())
                    .flatMap { deviceToken ->
                        notificationRepository
                            .registerDeviceToken(
                                deviceToken,
                                resourceManager.getDeviceId()
                            )
                    }
                    .map {
                        session
                    }
            }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(LoginState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(LoginState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(LoginState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = { session ->
                    handleLoginResult(session.user)
                },
                onError = {
                    _state.onNext(LoginState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleLoginResult(user: User) {
        if (user.id.isEmpty()) return

        if (!user.verified) {
            _state.onNext(
                LoginState
                    .UserNotVerified(
                        user,
                        user.email,
                        user.phoneNumber
                    )
            )
            return
        }

        when {
            !user.hasFullName() -> {
                // User has no first and last name.
                _state
                    .onNext(
                        LoginState.NoUserFirstAndLastName
                    )
            }
            !user.hasProfilePhoto() -> {
                // User has no profile photo.
                _state
                    .onNext(
                        LoginState.NoProfilePhoto
                    )
            }
            else -> {
                // User has all required details.
                _state
                    .onNext(
                        LoginState.LoginSuccess(user)
                    )
            }
        }
    }
}
