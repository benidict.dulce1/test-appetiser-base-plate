package com.appetiser.bentest.features.profile

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelFragment
import com.appetiser.bentest.databinding.FragmentProfileBinding
import com.appetiser.bentest.ext.loadAvatarUrl
import com.appetiser.bentest.features.account.AccountActivity
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class ProfileFragment : BaseViewModelFragment<FragmentProfileBinding, ProfileViewModel>() {

    companion object {
        fun newInstance(): ProfileFragment {
            return ProfileFragment()
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_profile

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbarAndStatusBar()
        setupViewModel()
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadAccount()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.profile_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_settings -> {
            AccountActivity.openActivity(requireActivity())
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun setupViewModel() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }

    private fun handleState(state: ProfileState) {
        when (state) {
            is ProfileState.UserProfileSession -> {
                binding.avatar.loadAvatarUrl(state.user.avatar.thumbUrl)
                binding.description.text = state.user.description
            }
        }
    }

    private fun setupToolbarAndStatusBar() {
        setHasOptionsMenu(true)
        val toolbar = (requireActivity() as AppCompatActivity)
        with(toolbar) {
            setSupportActionBar(binding.toolbarView)
            supportActionBar?.title = null
            supportActionBar?.setHomeButtonEnabled(false)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)

            binding.toolbarTitle.text = getString(R.string.my_profile)
        }
    }
}
