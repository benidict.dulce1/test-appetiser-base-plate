package com.appetiser.bentest.features.auth.forgotpassword.verification

sealed class ForgotPasswordVerificationState {

    data class GetUsername(val username: String) : ForgotPasswordVerificationState()

    object ResendTokenSuccess : ForgotPasswordVerificationState()

    data class ForgotPasswordSuccess(val username: String, val token: String) : ForgotPasswordVerificationState()

    data class Error(val throwable: Throwable) : ForgotPasswordVerificationState()

    object ShowProgressLoading : ForgotPasswordVerificationState()

    object HideProgressLoading : ForgotPasswordVerificationState()
}
