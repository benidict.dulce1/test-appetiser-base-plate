package com.appetiser.bentest.features.auth.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityLoginBinding
import com.appetiser.bentest.ext.disabledWithAlpha
import com.appetiser.bentest.ext.enabledWithAlpha
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.features.auth.emailcheck.EmailCheckActivity
import com.appetiser.bentest.features.auth.forgotpassword.ForgotPasswordActivity
import com.appetiser.bentest.features.auth.phonenumbercheck.PhoneNumberCheckActivity
import com.appetiser.bentest.features.auth.register.details.InputNameActivity
import com.appetiser.bentest.features.auth.register.profile.UploadProfilePhotoActivity
import com.appetiser.bentest.features.auth.register.verification.RegisterVerificationCodeActivity
import com.appetiser.bentest.features.main.MainActivity
import com.appetiser.module.common.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.common.widget.CustomPasswordTransformation
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class LoginActivity : BaseViewModelActivity<ActivityLoginBinding, LoginViewModel>() {

    companion object {
        /**
         * @param email if coming from [EmailCheckActivity]
         * @param phone if coming from [PhoneNumberCheckActivity]
         */
        fun openActivity(context: Context, email: String, phone: String = "") {
            val intent = Intent(context, LoginActivity::class.java)
            intent.putExtra(KEY_EMAIL, email)
            intent.putExtra(KEY_PHONE, phone)
            context.startActivity(intent)
        }

        const val KEY_EMAIL = "email"
        const val KEY_PHONE = "phone"
        private const val KEY_PASSWORD = "password"
    }

    override fun getLayoutId(): Int = R.layout.activity_login

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViews(savedInstanceState)
        observeInputViews()
        setUpViewModels()
        setupToolbar()

        viewModel.requestArguments()
    }

    private fun setUpViews(savedInstanceState: Bundle?) {
        binding.etPassword.apply {
            transformationMethod = CustomPasswordTransformation()
        }

        binding.etPassword.apply {
            setText(savedInstanceState?.getString(KEY_PASSWORD))
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    if (binding.btnContinue.isEnabled) {
                        viewModel.login(
                            binding.txtUsername.text.toString(),
                            binding.etPassword.text.toString()
                        )
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding
            .btnContinue
            .ninjaTap {
                viewModel
                    .login(
                        binding.txtUsername.text.toString(),
                        binding.etPassword.text.toString()
                    )
            }
            .addTo(disposables)

        binding
            .forgotPassword
            .ninjaTap {
                ForgotPasswordActivity
                    .openActivity(
                        this,
                        binding.txtUsername.text.toString()
                    )
            }
            .addTo(disposables)
    }

    private fun observeInputViews() {
        val passwordObservable = binding.etPassword.textChangeEvents()
            .skipInitialValue()
            .map { it.text }
            .map { it.isNotEmpty() && it.length >= 8 }

        passwordObservable
            .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    if (it) {
                        binding.btnContinue.enabledWithAlpha()
                    } else {
                        binding.btnContinue.disabledWithAlpha()
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setUpViewModels() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: LoginState) {
        when (state) {
            is LoginState.GetUsername -> {
                binding.txtUsername.text = state.username
            }
            is LoginState.NoUserFirstAndLastName -> {
                startUserDetailsScreen()
            }
            is LoginState.NoProfilePhoto -> {
                startUploadProfilePhotoScreen()
            }
            is LoginState.LoginSuccess -> {
                startMainScreen()
            }
            is LoginState.UserNotVerified -> {
                startVerificationCodeScreen(state)
            }
            is LoginState.Error -> {
                toast(state.throwable.getThrowableError())
            }
            is LoginState.ShowProgressLoading -> {
                toast("Sending request")
            }
        }
    }

    private fun startMainScreen() {
        MainActivity.openActivity(this)
        finishAffinity()
    }

    private fun startVerificationCodeScreen(state: LoginState.UserNotVerified) {
        RegisterVerificationCodeActivity
            .openActivity(
                this,
                email = state.email,
                phone = state.phone
            )
    }

    private fun startUploadProfilePhotoScreen() {
        UploadProfilePhotoActivity.openActivity(this)
    }

    private fun startUserDetailsScreen() {
        InputNameActivity.openActivity(this)
    }
}
