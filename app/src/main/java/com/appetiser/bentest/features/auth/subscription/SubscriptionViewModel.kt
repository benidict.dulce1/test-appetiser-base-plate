package com.appetiser.bentest.features.auth.subscription

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import javax.inject.Inject

class SubscriptionViewModel @Inject constructor() : BaseViewModel() {
    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit
}
