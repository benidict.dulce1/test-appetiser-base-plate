package com.appetiser.bentest.features.main

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class MainViewModel @Inject constructor() : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<MainState>()
    }

    val state: Observable<MainState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit
}
