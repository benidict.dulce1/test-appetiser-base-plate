package com.appetiser.bentest.features.auth

import android.app.Activity
import android.content.Intent
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import timber.log.Timber

class FacebookLoginManager {
    private val callbackManager = CallbackManager.Factory.create()
    private var onSuccessListener: ((LoginResult) -> Unit)? = null
    private var onCancelListener: (() -> Unit)? = null
    private var onErrorListener: ((FacebookException) -> Unit)? = null

    companion object {
        fun isLoggedIn(): Boolean {
            val accessToken = AccessToken.getCurrentAccessToken()
            return accessToken != null && !accessToken.isExpired
        }

        fun logout() {
            LoginManager
                .getInstance()
                .logOut()
        }
    }

    init {
        LoginManager
            .getInstance()
            .registerCallback(
                callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(result: LoginResult) {
                        Timber.d("onSuccess")
                        onSuccessListener?.invoke(result)
                    }

                    override fun onCancel() {
                        Timber.d("onCancel")
                        onCancelListener?.invoke()
                    }

                    override fun onError(error: FacebookException) {
                        Timber.e(error)
                        onErrorListener?.invoke(error)
                    }
                }
            )
    }

    /**
     * Sets facebook login success listener.
     */
    fun setLoginSuccessListener(listener: (LoginResult) -> Unit) {
        onSuccessListener = listener
    }

    /**
     * Sets facebook login cancel listener.
     */
    fun setLoginCancelListener(listener: () -> Unit) {
        onCancelListener = listener
    }

    /**
     * Sets facebook login error listener.
     */
    fun setLoginErrorListener(listener: (FacebookException) -> Unit) {
        onErrorListener = listener
    }

    /**
     * Calls facebook's login manager to log in.
     *
     * @param activity activity to receive `onActivityResult`
     */
    fun login(activity: Activity) {
        LoginManager
            .getInstance()
            .logInWithReadPermissions(activity, listOf("public_profile", "email"))
    }

    fun logout() = Companion.logout()

    fun sendLoginResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    fun isLoggedIn() = Companion.isLoggedIn()

    fun clearListeners() {
        onSuccessListener = null
        onCancelListener = null
        onErrorListener = null
    }
}
