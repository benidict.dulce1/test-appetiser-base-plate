package com.appetiser.bentest.features.main

import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseFragment
import com.appetiser.bentest.databinding.FragmentDummyBinding

class DummyFragment : BaseFragment<FragmentDummyBinding>() {
    companion object {
        fun newInstance(): DummyFragment {
            return DummyFragment()
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_dummy
}
