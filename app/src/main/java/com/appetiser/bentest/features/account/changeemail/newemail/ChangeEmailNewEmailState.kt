package com.appetiser.bentest.features.account.changeemail.newemail

sealed class ChangeEmailNewEmailState {
    object EnableButton : ChangeEmailNewEmailState()

    object DisableButton : ChangeEmailNewEmailState()

    object ShowLoading : ChangeEmailNewEmailState()

    object HideLoading : ChangeEmailNewEmailState()

    object InvalidEmail : ChangeEmailNewEmailState()

    data class ChangeEmailSuccess(
        val email: String,
        val verificationToken: String
    ) : ChangeEmailNewEmailState()

    data class EmailAlreadyTaken(val message: String) : ChangeEmailNewEmailState()

    data class Error(val message: String) : ChangeEmailNewEmailState()
}
