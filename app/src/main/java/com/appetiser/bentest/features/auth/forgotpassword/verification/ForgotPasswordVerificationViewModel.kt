package com.appetiser.bentest.features.auth.forgotpassword.verification

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.module.common.toObservable
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class ForgotPasswordVerificationViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    private lateinit var username: String

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        username = bundle?.getString(ForgotPasswordVerificationActivity.KEY_USERNAME, "").orEmpty()

        username.toObservable {
            _state.onNext(ForgotPasswordVerificationState.GetUsername(it))
        }
    }

    private val _state by lazy {
        PublishSubject.create<ForgotPasswordVerificationState>()
    }

    val state: Observable<ForgotPasswordVerificationState> = _state

    fun resendCode() {
        disposables.add(
            repository.forgotPassword(username)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doOnSubscribe {
                    _state.onNext(ForgotPasswordVerificationState.ShowProgressLoading)
                }
                .doOnSuccess {
                    _state.onNext(ForgotPasswordVerificationState.HideProgressLoading)
                }
                .doOnError {
                    _state.onNext(ForgotPasswordVerificationState.HideProgressLoading)
                }
                .subscribeBy(
                    onSuccess = {
                        _state.onNext(ForgotPasswordVerificationState.ResendTokenSuccess)
                    },
                    onError = {
                        _state.onNext(ForgotPasswordVerificationState.Error(it))
                    }
                )
        )
    }

    fun sendToken(token: String) {
        disposables.add(
            repository.forgotPasswordCheckCode(username, token)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui())
                .doOnSubscribe {
                    _state.onNext(ForgotPasswordVerificationState.ShowProgressLoading)
                }
                .doOnSuccess {
                    _state.onNext(ForgotPasswordVerificationState.HideProgressLoading)
                }
                .doOnError {
                    _state.onNext(ForgotPasswordVerificationState.HideProgressLoading)
                }
                .subscribeBy(
                    onSuccess = {
                        _state.onNext(ForgotPasswordVerificationState.ForgotPasswordSuccess(username, token))
                    },
                    onError = {
                        _state.onNext(ForgotPasswordVerificationState.Error(it))
                    }
                )
        )
    }
}
