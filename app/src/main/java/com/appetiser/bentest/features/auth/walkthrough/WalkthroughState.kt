package com.appetiser.bentest.features.auth.walkthrough

sealed class WalkthroughState {
    data class UpdatePageIndicator(
        val previousPage: Int,
        val currentPage: Int
    ) : WalkthroughState()

    data class ShowStep4Buttons(
        val previousPage: Int,
        val currentPage: Int
    ) : WalkthroughState()

    data class HideStep4Buttons(
        val previousPage: Int,
        val currentPage: Int
    ) : WalkthroughState()

    /**
     * User has logged in and has filled in onboarding details.
     */
    object UserIsLoggedIn : WalkthroughState()

    /**
     * User has logged in but not filled in onboarding details.
     */
    object UserIsLoggedInButNotOnboarded : WalkthroughState()

    /**
     * User is not logged in, will show white
     */
    object UserIsNotLoggedIn : WalkthroughState()
}
