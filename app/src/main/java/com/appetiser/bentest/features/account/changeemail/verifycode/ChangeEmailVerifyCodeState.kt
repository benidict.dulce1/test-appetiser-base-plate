package com.appetiser.bentest.features.account.changeemail.verifycode

sealed class ChangeEmailVerifyCodeState {
    object HideKeyboard : ChangeEmailVerifyCodeState()

    object ShowLoading : ChangeEmailVerifyCodeState()

    object HideLoading : ChangeEmailVerifyCodeState()

    object ResendCodeSuccess : ChangeEmailVerifyCodeState()

    object VerificationSuccess : ChangeEmailVerifyCodeState()

    data class DisplayEmail(val email: String) : ChangeEmailVerifyCodeState()

    data class InvalidVerificationCode(val message: String) : ChangeEmailVerifyCodeState()

    data class Error(val message: String) : ChangeEmailVerifyCodeState()
}
