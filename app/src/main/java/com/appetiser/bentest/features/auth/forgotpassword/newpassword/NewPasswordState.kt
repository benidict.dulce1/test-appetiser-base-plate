package com.appetiser.bentest.features.auth.forgotpassword.newpassword

sealed class NewPasswordState {

    data class GetEmail(val email: String) : NewPasswordState()

    data class Success(val email: String) : NewPasswordState()

    data class Error(val throwable: Throwable) : NewPasswordState()

    object PasswordBelowMinLength : NewPasswordState()

    object PasswordExceedsMaxLength : NewPasswordState()

    object ShowProgressLoading : NewPasswordState()

    object HideProgressLoading : NewPasswordState()
}
