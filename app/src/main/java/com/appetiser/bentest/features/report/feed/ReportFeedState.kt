package com.appetiser.bentest.features.report.feed

import com.appetiser.bentest.features.report.adapter.ReportAttachmentsViewType
import com.appetiser.bentest.features.report.adapter.ReportAttachmentsViewTypeObject
import com.appetiser.module.domain.models.miscellaneous.ReportCategory

sealed class ReportFeedState {

    data class AddPhoto(val imagePath: String) : ReportFeedState()

    data class RemovePhoto(val item: ReportAttachmentsViewTypeObject) : ReportFeedState()

    data class DisplayAddButton(val item: ReportAttachmentsViewType) : ReportFeedState()

    data class ReportCategoriesFetched(val reportCategories: List<ReportCategory>) : ReportFeedState()

    object ReportSuccess : ReportFeedState()

    data class Error(val throwable: Throwable) : ReportFeedState()

    object ShowProgressLoading : ReportFeedState()

    object HideProgressLoading : ReportFeedState()
}
