package com.appetiser.bentest.features.account.settings

sealed class AccountSettingsState {

    object NavigateToEmailCheck : AccountSettingsState()

    object ShowLoading : AccountSettingsState()

    object HideLoading : AccountSettingsState()

    object ShowEmailChangeSuccess : AccountSettingsState()

    object ShowPhoneChangeSuccess : AccountSettingsState()

    object ShowChangePasswordSuccess : AccountSettingsState()

    data class DisplayDetails(
        val email: String,
        val phoneNumber: String
    ) : AccountSettingsState()

    data class Error(val message: String) : AccountSettingsState()
}
