package com.appetiser.bentest.features.account.changephone.verifypassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityChangePhoneVerifyPasswordBinding
import com.appetiser.bentest.features.account.changephone.newphone.ChangePhoneNewPhoneActivity
import com.appetiser.module.common.*
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangePhoneVerifyPasswordActivity : BaseViewModelActivity<ActivityChangePhoneVerifyPasswordBinding, ChangePhoneVerifyPasswordViewModel>() {
    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    ChangePhoneVerifyPasswordActivity::class.java
                )
            )
        }
    }

    override fun canBack(): Boolean = true

    override fun getLayoutId(): Int = R.layout.activity_change_phone_verify_password

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.passwordMaxLength = PASSWORD_MAX_LENGTH

        setupToolbar()
        setupViews()
        setupVmObservers()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: ChangePhoneVerifyPasswordState) {
        when (state) {
            ChangePhoneVerifyPasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            ChangePhoneVerifyPasswordState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            ChangePhoneVerifyPasswordState.ShowLoading -> {
                binding.loading setVisible true
            }
            ChangePhoneVerifyPasswordState.HideLoading -> {
                binding.loading setVisible false
            }
            ChangePhoneVerifyPasswordState.InvalidPassword -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.invalid_password)
            }
            is ChangePhoneVerifyPasswordState.VerificationSuccess -> {
                ChangePhoneNewPhoneActivity
                    .openActivity(
                        this,
                        state.verificationToken
                    )
            }
            is ChangePhoneVerifyPasswordState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun setupViews() {
        binding
            .inputPassword
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel.onPasswordTextChanged(text.toString())
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnContinue
            .ninjaTap {
                binding.inputLayoutPassword.error = ""
                binding.root.hideKeyboard()
                viewModel
                    .verifyPassword(
                        binding.inputPassword.text.toString().trim()
                    )
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }
}
