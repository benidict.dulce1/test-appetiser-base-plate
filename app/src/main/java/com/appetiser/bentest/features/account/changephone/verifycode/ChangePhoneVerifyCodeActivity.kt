package com.appetiser.bentest.features.account.changephone.verifycode

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityChangePhoneVerifyCodeBinding
import com.appetiser.bentest.features.account.settings.AccountSettingsActivity
import com.appetiser.module.common.*
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangePhoneVerifyCodeActivity : BaseViewModelActivity<ActivityChangePhoneVerifyCodeBinding, ChangePhoneVerifyCodeViewModel>() {

    companion object {
        private const val EXTRA_PHONE = "EXTRA_PHONE"
        private const val EXTRA_VERIFICATION_TOKEN = "EXTRA_VERIFICATION_TOKEN"

        fun openActivity(
            context: Context,
            phoneNumber: String,
            verificationToken: String
        ) {
            context.startActivity(
                Intent(
                    context,
                    ChangePhoneVerifyCodeActivity::class.java
                ).apply {
                    putExtra(EXTRA_PHONE, phoneNumber)
                    putExtra(EXTRA_VERIFICATION_TOKEN, verificationToken)
                }
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_change_phone_verify_code

    override fun canBack(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        require(
            intent.extras != null &&
                intent.extras!!.getString(EXTRA_PHONE, "").isNotEmpty() &&
                intent.extras!!.getString(EXTRA_VERIFICATION_TOKEN, "").isNotEmpty()
        ) {
            "Required intent extra `phoneNumber` or `verificationToken` is missing!"
        }

        val phoneNumber = intent.extras!!.getString(EXTRA_PHONE, "")
        val verificationToken = intent.extras!!.getString(EXTRA_VERIFICATION_TOKEN, "")

        setupToolbar()
        setupViews()
        setupVmObservers()

        viewModel
            .setPhoneAndVerificationToken(
                phoneNumber,
                verificationToken
            )
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: ChangePhoneVerifyCodeState) {
        when (state) {
            is ChangePhoneVerifyCodeState.DisplayPhone -> {
                binding
                    .txtPhone
                    .text = state.phoneNumber
            }
            ChangePhoneVerifyCodeState.ShowLoading -> {
                binding.root.hideKeyboard()
            }
            ChangePhoneVerifyCodeState.ShowLoading -> {
                binding.loading setVisible true
            }
            ChangePhoneVerifyCodeState.HideLoading -> {
                binding.loading setVisible false
            }
            is ChangePhoneVerifyCodeState.ResendCodeSuccess -> {
                showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.resent_code_to_mobile_number)
                )
            }
            is ChangePhoneVerifyCodeState.VerificationSuccess -> {
                AccountSettingsActivity
                    .openActivity(
                        this,
                        Bundle().apply {
                            putBoolean(
                                AccountSettingsActivity.EXTRA_FROM_CHANGE_PHONE,
                                true
                            )
                        },
                        Intent.FLAG_ACTIVITY_CLEAR_TOP
                    )
            }
            is ChangePhoneVerifyCodeState.InvalidVerificationCode -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
            is ChangePhoneVerifyCodeState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun setupViews() {
        binding
            .inputCode
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel
                        .onCodeTextChanged(
                            text.toString()
                        )
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnResend
            .ninjaTap {
                viewModel
                    .resendCode()
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }
}
