package com.appetiser.bentest.features.profile.editprofile

sealed class EditProfileState {
    object EnableSaveButton : EditProfileState()

    object DisableSaveButton : EditProfileState()

    object ShowLoading : EditProfileState()

    object HideLoading : EditProfileState()

    object UpdateProfileSuccess : EditProfileState()

    object NavigateUp : EditProfileState()

    object ShowUnsavedChangesDialog : EditProfileState()

    data class ShowDatePicker(
        val year: Int,
        val month: Int,
        val day: Int
    ) : EditProfileState()

    data class DescriptionExceedsMaxLength(
        val maxLength: Int
    ) : EditProfileState()

    data class DisplayDob(
        val year: Int,
        val month: Int,
        val day: Int
    ) : EditProfileState()

    data class PreFillFields(
        val imageUrl: String,
        val fullName: String,
        val description: String
    ) : EditProfileState()

    data class DisplayNewImage(val imageLocalPath: String) : EditProfileState()

    data class Error(val message: String) : EditProfileState()
}
