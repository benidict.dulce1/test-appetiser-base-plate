package com.appetiser.bentest.features.profile

import com.appetiser.module.domain.models.user.User

sealed class ProfileState {

    data class UserProfileSession(val user: User) : ProfileState()

    data class Error(val throwable: Throwable) : ProfileState()

    object ShowProgressLoading : ProfileState()

    object HideProgressLoading : ProfileState()
}
