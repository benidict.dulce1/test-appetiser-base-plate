package com.appetiser.bentest.features.report.feed

import android.content.Context
import android.net.Uri
import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.features.report.adapter.ReportAttachmentsViewType
import com.appetiser.bentest.features.report.adapter.ReportAttachmentsViewTypeObject
import com.appetiser.bentest.features.report.feed.ReportFeedActivity.Companion.KEY_FEED_ID
import com.appetiser.bentest.utils.FileUtils
import com.appetiser.module.data.features.feeds.FeedRepository
import com.appetiser.module.data.features.miscellaneous.MiscellaneousRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class ReportFeedViewModel @Inject constructor(
    private val miscellaneousRepository: MiscellaneousRepository,
    private val repository: FeedRepository
) : BaseViewModel() {

    private var feedId: Long = -1

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        feedId = bundle?.getLong(KEY_FEED_ID, -1) ?: -1

        getReportCategories()
    }

    private val _state by lazy {
        PublishSubject.create<ReportFeedState>()
    }

    val state: Observable<ReportFeedState> = _state

    private var pathList: ArrayList<String> = arrayListOf()

    fun addPhoto(imagePath: String) {
        this.pathList.add(imagePath)
        _state.onNext(ReportFeedState.AddPhoto(imagePath))
    }

    fun removePhoto(item: ReportAttachmentsViewTypeObject) {
        this.pathList.remove(item.imagePath)
        _state.onNext(ReportFeedState.RemovePhoto(item))
    }

    fun displayAddPhotoButton(item: ReportAttachmentsViewType) {
        _state.onNext(ReportFeedState.DisplayAddButton(item))
    }

    private fun getReportCategories() {
        miscellaneousRepository
            .getReportCategories()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { categories ->
                    _state.onNext(ReportFeedState.ReportCategoriesFetched(categories))
                },
                onError = { error ->
                    _state.onNext(ReportFeedState.Error(error))
                }
            )
            .addTo(disposables)
    }

    fun reportFeed(
        context: Context,
        reasonId: Long,
        description: String
    ) {
        var list = listOf<Uri>()
        FileUtils
            .resizePhotos(context, this.pathList)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSuccess { uris ->
                list = uris
            }
            .flatMap { uris ->
                val imagePathList = uris.map { it.path.orEmpty() }

                repository
                    .reportFeed(
                        feedId,
                        reasonId,
                        description,
                        imagePathList
                    )
                    .subscribeOn(schedulers.io())
            }
            .doOnSubscribe {
                _state.onNext(ReportFeedState.ShowProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    deleteResizedPhotos(list)
                    _state.onNext(ReportFeedState.HideProgressLoading)
                    _state.onNext(ReportFeedState.ReportSuccess)
                },
                onError = { error ->
                    _state.onNext(ReportFeedState.HideProgressLoading)
                    _state.onNext(ReportFeedState.Error(error))
                }
            )
            .apply { disposables.add(this) }
    }

    private fun deleteResizedPhotos(uris: List<Uri>) {
        uris.forEach { uri ->
            FileUtils.deleteFile(uri)
        }
    }
}
