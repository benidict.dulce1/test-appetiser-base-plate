package com.appetiser.bentest.features.auth.forgotpassword

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class ForgotPasswordViewModel @Inject constructor(
    private val repository: AuthRepository
) : BaseViewModel() {

    private var username: String = ""

    private val _state by lazy {
        PublishSubject.create<ForgotPasswordState>()
    }

    val state: Observable<ForgotPasswordState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun setUsername(username: String) {
        this.username = username

        if (username.isNotEmpty()) {
            _state
                .onNext(
                    ForgotPasswordState
                        .GetUsername(username)
                )
        }
    }

    fun forgotPassword(username: String) {
        repository
            .forgotPassword(username)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(ForgotPasswordState.ShowProgressLoading)
            }
            .doOnError {
                _state.onNext(ForgotPasswordState.HideProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(ForgotPasswordState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = {
                    if (it) {
                        _state
                            .onNext(
                                ForgotPasswordState.Success
                            )
                    }
                },
                onError = {
                    _state.onNext(ForgotPasswordState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }
}
