package com.appetiser.bentest.features.main

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityMainBinding
import com.appetiser.bentest.features.feed.create.CreateFeedActivity
import com.appetiser.bentest.features.feed.list.FeedsFragment
import com.appetiser.bentest.features.marketplace.MarketplaceFeedFragment
import com.appetiser.bentest.features.notification.NotificationFragment
import com.appetiser.bentest.features.profile.ProfileFragment
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.common.widget.ScrollableViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class MainActivity : BaseViewModelActivity<ActivityMainBinding, MainViewModel>() {
    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    MainActivity::class.java
                )
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupViewPager()
        setUpViewModelsObserver()
        setupNavigationView()

        binding.fab.ninjaTap {
            startForResult.launch(Intent(this, CreateFeedActivity::class.java))
        }.addTo(disposables)
    }

    private val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val fragment = supportFragmentManager.fragments[0] as FeedsFragment
            fragment.scrollToTop()
        }
    }

    override fun onBackPressed() {
        Single.just(binding.viewpager.currentItem)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onSuccess = {
                    handleBackPressed(it)
                },
                onError = {
                    Timber.e("Error $it")
                }
            )
            .addTo(disposables)
    }

    private fun setupNavigationView() {
        binding.bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.page_1 -> {
                setCurrentPosition(0)
            }
            R.id.page_2 -> {
                setCurrentPosition(1)
            }
            R.id.page_4 -> {
                setCurrentPosition(3)
            }
            R.id.page_5 -> {
                setCurrentPosition(4)
            }
            else -> {
            }
        }
        true
    }

    private fun setCurrentPosition(position: Int) {
        binding.viewpager.setCurrentItem(position, false)
    }

    private fun setupViewPager() {
        val listOfFragments: MutableList<Fragment> = mutableListOf()
        listOfFragments.add(FeedsFragment.newInstance())
        listOfFragments.add(MarketplaceFeedFragment())
        // listOfFragments.add(DummyFragment.newInstance())
        listOfFragments.add(NotificationFragment.newInstance())
        listOfFragments.add(ProfileFragment.newInstance())

        val pagerAdapter = ScrollableViewPager.PageAdapter(supportFragmentManager, listOfFragments)
        binding.viewpager.adapter = pagerAdapter
        binding.viewpager.offscreenPageLimit = 5
    }

    private fun setUpViewModelsObserver() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    handleState(state)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: MainState?) {
        when (state) {
            MainState.ShowProgressLoading -> {
                toast(getString(R.string.sending_request))
            }
        }
    }

    private fun handleBackPressed(position: Int) {
        when (position) {
            0 -> {
                super.onBackPressed()
            }
            else -> {
                binding.bottomNavigation.selectedItemId = R.id.page_1
            }
        }
    }
}
