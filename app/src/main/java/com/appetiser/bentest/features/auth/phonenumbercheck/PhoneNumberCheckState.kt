package com.appetiser.bentest.features.auth.phonenumbercheck

sealed class PhoneNumberCheckState {
    data class PhoneNumberDoesNotExist(val phoneNumber: String) : PhoneNumberCheckState()

    data class PhoneNumberExists(val phoneNumber: String) : PhoneNumberCheckState()

    data class Error(val throwable: Throwable) : PhoneNumberCheckState()

    object EnableButton : PhoneNumberCheckState()

    object DisableButton : PhoneNumberCheckState()

    object ShowProgressLoading : PhoneNumberCheckState()

    object HideProgressLoading : PhoneNumberCheckState()
}
