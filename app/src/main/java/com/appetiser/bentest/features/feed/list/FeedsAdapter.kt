package com.appetiser.bentest.features.feed.list

import android.view.LayoutInflater
import android.view.MenuInflater
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BasePagingAdapter
import com.appetiser.bentest.databinding.ItemFeedBinding
import com.appetiser.module.common.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.domain.models.feed.Feed
import com.google.gson.Gson
import com.jakewharton.rxbinding3.appcompat.itemClicks
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit

class FeedsAdapter(
    private val disposable: CompositeDisposable,
    gson: Gson
) : BasePagingAdapter<Feed, BasePagingAdapter.BaseViewHolder<Feed>>(FeedsDiffCallback(gson)) {

    val itemClickListener = PublishSubject.create<FeedItemState>()

    init {
        setHasStableIds(true)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Feed>, position: Int) {
        getItem(position)?.let {
            holder.itemPosition = position
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Feed> {
        return createFeedViewHolder(parent)
    }

    private fun createFeedViewHolder(parent: ViewGroup): FeedViewHolder {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_feed, parent, false)

        val binding = ItemFeedBinding.bind(view)

        return FeedViewHolder(binding)
    }

    fun updateFeed(item: Feed) {
        val position =
            snapshot()
                .indexOfFirst {
                    it?.id == item.id
                }

        if (position != -1) {
            snapshot()[position]?.apply {
                isFavorite = item.isFavorite
                favoritesCount = item.favoritesCount
                commentsCount = item.commentsCount
            }

            notifyItemChanged(position)
        }
    }

    fun getFeed(position: Int): Feed {
        return getItem(position) ?: throw KotlinNullPointerException("No feed found!")
    }

    inner class FeedViewHolder(override val binding: ItemFeedBinding) : BaseViewHolder<Feed>(binding) {
        override fun bind(item: Feed) {
            super.bind(item)

            Observable.merge(binding.imgFeed.clicks(), binding.txtMessage.clicks())
                .throttleFirst(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onNext = {
                        itemClickListener.onNext(FeedItemState.ShowFeedDetails(item))
                    },
                    onError = {
                        Timber.e("Error $it")
                    }
                )
                .addTo(disposable)

            binding.imgHeart
                .ninjaTap {
                    itemClickListener.onNext(FeedItemState.FavoriteClicked(item))
                }
                .addTo(disposable)

            binding.txtCommentCount
                .ninjaTap {
                    itemClickListener.onNext(FeedItemState.CommentClicked(item))
                }
                .addTo(disposable)

            binding.imgOptions
                .ninjaTap {
                    val popup = PopupMenu(it.context, it)
                    val inflater: MenuInflater = popup.menuInflater
                    inflater.inflate(if (item.isUserFeedOwner) R.menu.feed_owner_menu else R.menu.feed_user_menu, popup.menu)

                    popup.itemClicks()
                        .observeOn(AndroidSchedulers.mainThread())
                        .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                        .subscribeBy(
                            onNext = {
                                if (it.itemId == R.id.menu_delete) {
                                    itemClickListener.onNext(FeedItemState.DeleteFeedClicked(item, itemPosition))
                                } else if (it.itemId == R.id.menu_report) {
                                    itemClickListener.onNext(FeedItemState.ReportFeedClicked(item, itemPosition))
                                }
                            }
                        )
                        .addTo(disposable)

                    popup.show()
                }
                .addTo(disposable)

            binding.txtMessage.text = item.body
            binding.txtMessage.triggerTruncateText()

            binding.imgHeart.setImageResource(if (item.isFavorite) R.drawable.ic_heart_selected else R.drawable.ic_heart_unselected)
        }
    }
}

sealed class FeedItemState {
    data class FavoriteClicked(val item: Feed) : FeedItemState()
    data class CommentClicked(val item: Feed) : FeedItemState()
    data class ShowFeedDetails(val item: Feed) : FeedItemState()
    data class DeleteFeedClicked(val item: Feed, val position: Int) : FeedItemState()
    data class ReportFeedClicked(val item: Feed, val position: Int) : FeedItemState()
}
