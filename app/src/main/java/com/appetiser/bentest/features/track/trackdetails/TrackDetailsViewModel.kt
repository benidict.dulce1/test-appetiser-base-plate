package com.appetiser.bentest.features.track.trackdetails

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.features.track.TrackDetailsState
import com.appetiser.bentest.features.track.TrackState
import com.appetiser.module.data.features.track.TrackRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class TrackDetailsViewModel @Inject constructor(
    private val repository: TrackRepository
) : BaseViewModel(){

    private val _state by lazy {
        PublishSubject.create<TrackDetailsState>()
    }
    val state: Observable<TrackDetailsState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?){
        loadTrackDetails()
    }

    fun loadTrackDetails(){
        repository.loadTrackDetails()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribeBy (
                onSuccess = {
                    _state.onNext(TrackDetailsState.TrackDetails(it))
                }
            ).addTo(disposables)
    }

    fun deleteTrackDetails(){
        repository.deleteTrackDetails()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.io())
            .subscribeBy(
                onComplete = {
                    _state.onNext(TrackDetailsState.TrackDetailsDeleted)
                }
            ).addTo(disposables)
    }
}