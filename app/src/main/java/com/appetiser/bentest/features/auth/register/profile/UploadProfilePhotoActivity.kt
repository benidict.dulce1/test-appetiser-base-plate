package com.appetiser.bentest.features.auth.register.profile

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseCameraActivity
import com.appetiser.bentest.databinding.ActivityUploadProfileBinding
import com.appetiser.bentest.ext.disabledWithAlpha
import com.appetiser.bentest.ext.enabledWithAlpha
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.ext.loadAvatarUrl
import com.appetiser.bentest.features.auth.subscription.SubscriptionActivity
import com.appetiser.module.common.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit

class UploadProfilePhotoActivity : BaseCameraActivity<ActivityUploadProfileBinding, UploadPhotoViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            val intent = Intent(context, UploadProfilePhotoActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun setImagePathUrl(captureCameraPath: String) {
        if (captureCameraPath.isNotEmpty()) {
            binding.ivProfilePhoto.triggerChanges()
            binding.ivProfilePhoto.loadAvatarUrl(captureCameraPath)
        } else {
            toast(getString(R.string.invalid_file))
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_upload_profile
    }

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setupViews()
        observeViews()
        setupViewModel()
    }

    private fun setupViewModel() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    when (it) {
                        is UploadPhotoState.ErrorUploadPhoto -> {
                            toast(it.throwable.getThrowableError())
                        }
                        is UploadPhotoState.SuccessUploadPhoto -> {
                            gotoNextScreen()
                        }
                    }
                }
            ).addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun observeViews() {
        val imageObserver = binding.ivProfilePhoto.imageChanges()
            .map { it }

        disposables.add(
            imageObserver
                .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribeBy(
                    onError = {
                        toast(it.message.orEmpty())
                    },
                    onNext = {
                        if (it) {
                            binding.btnContinue.visibility = View.VISIBLE
                            binding.btnContinue.enabledWithAlpha()
                        } else {
                            binding.btnContinue.visibility = View.INVISIBLE
                            binding.btnContinue.disabledWithAlpha()
                        }
                    }
                )
        )
    }

    private fun setupViews() {
        binding.btnCamera.ninjaTap {
            rxPermissions
                .requestEachCombined(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .doOnNext {
                    when {
                        it.granted -> {
                            showPickerDialog()
                        }
                        else -> {
                            hidePickerDialog()
                        }
                    }
                }
                .subscribe()
        }

        binding.btnContinue.ninjaTap {
            viewModel.uploadPhoto(cameraUrl)
        }

        binding.btnSkip.ninjaTap {
            gotoNextScreen()
        }
    }

    private fun gotoNextScreen() {
        SubscriptionActivity.openActivity(this)
        finishAffinity()
    }
}
