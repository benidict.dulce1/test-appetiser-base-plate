package com.appetiser.bentest.features.account.changephone.newphone

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityChangePhoneNewPhoneBinding
import com.appetiser.bentest.features.account.changephone.verifycode.ChangePhoneVerifyCodeActivity
import com.appetiser.module.common.*
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangePhoneNewPhoneActivity : BaseViewModelActivity<ActivityChangePhoneNewPhoneBinding, ChangePhoneNewPhoneViewModel>() {

    companion object {
        private const val EXTRA_VERIFICATION_TOKEN = "EXTRA_VERIFICATION_TOKEN"

        fun openActivity(context: Context, verificationToken: String) {
            context.startActivity(
                Intent(
                    context,
                    ChangePhoneNewPhoneActivity::class.java
                ).apply {
                    putExtra(EXTRA_VERIFICATION_TOKEN, verificationToken)
                }
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_change_phone_new_phone

    override fun canBack(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        require(
            intent.extras != null &&
                intent.extras!!.getString(EXTRA_VERIFICATION_TOKEN, "").isNotEmpty()
        ) {
            "Required intent extra `verificationToken` is missing!"
        }

        viewModel
            .setVerificationToken(
                intent.extras!!.getString(EXTRA_VERIFICATION_TOKEN, "")
            )

        setupToolbar()
        setupViews()
        setupVmObservers()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: ChangePhoneNewPhoneState) {
        when (state) {
            ChangePhoneNewPhoneState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            ChangePhoneNewPhoneState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            ChangePhoneNewPhoneState.ShowLoading -> {
                binding.loading setVisible true
            }
            ChangePhoneNewPhoneState.HideLoading -> {
                binding.loading setVisible false
            }
            is ChangePhoneNewPhoneState.PhoneNumberError -> {
                binding
                    .inputPhone
                    .error = state.message
            }
            is ChangePhoneNewPhoneState.ChangePhoneSuccess -> {
                ChangePhoneVerifyCodeActivity
                    .openActivity(
                        this,
                        state.phoneNumber,
                        state.verificationToken
                    )
            }
            is ChangePhoneNewPhoneState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun setupViews() {
        binding
            .inputPhone
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribe {
                viewModel
                    .onPhoneTextChanged(it.toString())
            }
            .addTo(disposables)

        binding
            .btnContinue
            .ninjaTap {
                binding.inputLayoutPhone.error = ""
                binding.root.hideKeyboard()

                val countryCode = binding.countryCodePicker.selectedCountryCode
                val countryNameCode = binding.countryCodePicker.selectedCountryNameCode
                val phoneNumber = binding.inputPhone.text.toString()

                viewModel
                    .changePhone(
                        countryCode,
                        countryNameCode,
                        phoneNumber
                    )
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }
}
