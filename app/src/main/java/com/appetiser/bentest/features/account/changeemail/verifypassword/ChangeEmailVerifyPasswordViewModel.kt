package com.appetiser.bentest.features.account.changeemail.verifypassword

import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class ChangeEmailVerifyPasswordViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<ChangeEmailVerifyPasswordState>()
    }

    val state: Observable<ChangeEmailVerifyPasswordState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun onPasswordTextChanged(password: String) {
        if (validatePasswordNotEmpty(password)) {
            _state
                .onNext(
                    ChangeEmailVerifyPasswordState.EnableButton
                )
        } else {
            _state
                .onNext(
                    ChangeEmailVerifyPasswordState.DisableButton
                )
        }
    }

    fun verifyPassword(password: String) {
        if (!validatePasswordNotEmpty(password)) return

        authRepository
            .verifyPassword(password)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state
                    .onNext(
                        ChangeEmailVerifyPasswordState.ShowLoading
                    )
            }
            .doOnSuccess {
                _state
                    .onNext(
                        ChangeEmailVerifyPasswordState.HideLoading
                    )
            }
            .doOnError {
                _state
                    .onNext(
                        ChangeEmailVerifyPasswordState.HideLoading
                    )
            }
            .subscribeBy(
                onSuccess = { verificationToken ->
                    _state
                        .onNext(
                            ChangeEmailVerifyPasswordState.VerificationSuccess(
                                verificationToken
                            )
                        )
                },
                onError = { error ->
                    Timber.e(error)
                    if (error is HttpException && error.code() == 400) {
                        _state
                            .onNext(
                                ChangeEmailVerifyPasswordState.InvalidPassword
                            )
                    } else {
                        showGenericError()
                    }
                }
            )
            .addTo(disposables)
    }

    private fun showGenericError() {
        _state
            .onNext(
                ChangeEmailVerifyPasswordState.Error(
                    resourceManager
                        .getString(
                            R.string.generic_error_short
                        )
                )
            )
    }

    private fun validatePasswordNotEmpty(password: String): Boolean {
        return password.isNotEmpty()
    }
}
