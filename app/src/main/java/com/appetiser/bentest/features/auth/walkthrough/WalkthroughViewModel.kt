package com.appetiser.bentest.features.auth.walkthrough

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.utils.PAGE_INITIAL
import com.appetiser.bentest.utils.PAGE_STEP_4_POSITION
import com.appetiser.module.data.features.session.SessionRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import javax.inject.Inject

class WalkthroughViewModel @Inject constructor(
    private val sessionRepository: SessionRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<WalkthroughState>()
    }

    val state: Observable<WalkthroughState> = _state

    private var currentPage = PAGE_INITIAL

    override fun isFirstTimeUiCreate(bundle: Bundle?) {
        sessionRepository
            .getSession()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    val user = session.user
                    if (!user.verified) {
                        _state
                            .onNext(
                                WalkthroughState.UserIsNotLoggedIn
                            )
                        return@subscribeBy
                    }

                    if (user.hasFullName()) {
                        _state
                            .onNext(
                                WalkthroughState.UserIsLoggedIn
                            )
                    } else {
                        _state
                            .onNext(
                                WalkthroughState.UserIsLoggedInButNotOnboarded
                            )
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    fun onPageSelected(pagePosition: Int) {
        if (pagePosition == currentPage) return

        when {
            pagePosition == PAGE_STEP_4_POSITION -> {
                // User reaches step 4
                _state
                    .onNext(
                        WalkthroughState.ShowStep4Buttons(
                            currentPage,
                            pagePosition
                        )
                    )
            }
            pagePosition != PAGE_STEP_4_POSITION && currentPage == PAGE_STEP_4_POSITION -> {
                // User went back to step 3
                _state
                    .onNext(
                        WalkthroughState.HideStep4Buttons(
                            currentPage,
                            pagePosition
                        )
                    )
            }
            else -> {
                _state
                    .onNext(
                        WalkthroughState.UpdatePageIndicator(
                            currentPage,
                            pagePosition
                        )
                    )
            }
        }

        currentPage = pagePosition
    }
}
