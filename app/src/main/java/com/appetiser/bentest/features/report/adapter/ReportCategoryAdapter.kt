package com.appetiser.bentest.features.report.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.appetiser.bentest.R
import com.appetiser.module.domain.models.miscellaneous.ReportCategory

class ReportCategoryAdapter(
    context: Context,
    private val reportCategoryList: List<ReportCategory>
) : ArrayAdapter<ReportCategory>(context, R.layout.item_report_category_label) {

    override fun getItem(position: Int): ReportCategory? {
        return reportCategoryList[position]
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return convertView ?: LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_report_category_label, parent, false)
            .apply {
                findViewById<TextView>(R.id.selectedCategory)
                    .text = reportCategoryList[position].label
            }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return convertView ?: LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_report_category_label, parent, false)
            .apply {
                findViewById<TextView>(R.id.selectedCategory)
                    .text = reportCategoryList[position].label
            }
    }

    override fun getCount(): Int = reportCategoryList.size
}
