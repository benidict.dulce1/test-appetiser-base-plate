package com.appetiser.bentest.features.account.deleteaccount

import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class DeleteAccountPasswordViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<DeleteAccountPasswordState>()
    }

    val state: Observable<DeleteAccountPasswordState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun onPasswordTextChanged(password: String) {
        if (validatePasswordNotEmpty(password)) {
            _state
                .onNext(
                    DeleteAccountPasswordState.EnableButton
                )
        } else {
            _state
                .onNext(
                    DeleteAccountPasswordState.DisableButton
                )
        }
    }

    fun verifyPassword(password: String) {
        if (!validatePasswordNotEmpty(password)) return

        authRepository
            .verifyPassword(password)
            .subscribeOn(schedulers.io())
            .flatMapCompletable { verificationToken ->
                authRepository
                    .deleteAccount(
                        verificationToken
                    )
            }
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(DeleteAccountPasswordState.ShowLoading)
            }
            .doOnComplete {
                _state
                    .onNext(
                        DeleteAccountPasswordState.LogoutSocialLogins
                    )
                _state.onNext(DeleteAccountPasswordState.HideLoading)
            }
            .doOnError {
                _state.onNext(DeleteAccountPasswordState.HideLoading)
            }
            .subscribeBy(
                onComplete = {
                    _state
                        .onNext(
                            DeleteAccountPasswordState.NavigateToWalkthroughScreen
                        )
                },
                onError = { error ->
                    Timber.e(error)
                    if (error is HttpException && error.code() == 400) {
                        _state
                            .onNext(
                                DeleteAccountPasswordState.InvalidPassword
                            )
                    } else {
                        showGenericError()
                    }
                }
            )
            .addTo(disposables)
    }

    private fun showGenericError() {
        _state
            .onNext(
                DeleteAccountPasswordState.Error(
                    resourceManager
                        .getString(
                            R.string.generic_error_short
                        )
                )
            )
    }

    private fun validatePasswordNotEmpty(password: String): Boolean {
        return password.isNotEmpty()
    }
}
