package com.appetiser.bentest.features.account.changeemail.verifypassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityChangeEmailVerifyPasswordBinding
import com.appetiser.bentest.features.account.changeemail.newemail.ChangeEmailNewEmailActivity
import com.appetiser.module.common.*
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangeEmailVerifyPasswordActivity : BaseViewModelActivity<ActivityChangeEmailVerifyPasswordBinding, ChangeEmailVerifyPasswordViewModel>() {
    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    ChangeEmailVerifyPasswordActivity::class.java
                )
            )
        }
    }

    override fun canBack(): Boolean = true

    override fun getLayoutId(): Int = R.layout.activity_change_email_verify_password

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.passwordMaxLength = PASSWORD_MAX_LENGTH

        setupToolbar()
        setupViews()
        setupVmObservers()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: ChangeEmailVerifyPasswordState) {
        when (state) {
            ChangeEmailVerifyPasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            ChangeEmailVerifyPasswordState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            ChangeEmailVerifyPasswordState.ShowLoading -> {
                binding.loading setVisible true
            }
            ChangeEmailVerifyPasswordState.HideLoading -> {
                binding.loading setVisible false
            }
            ChangeEmailVerifyPasswordState.InvalidPassword -> {
                binding
                    .inputLayoutPassword
                    .error = getString(R.string.invalid_password)
            }
            is ChangeEmailVerifyPasswordState.VerificationSuccess -> {
                ChangeEmailNewEmailActivity
                    .openActivity(
                        this,
                        state.verificationToken
                    )
            }
            is ChangeEmailVerifyPasswordState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun setupViews() {
        binding
            .inputPassword
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel.onPasswordTextChanged(text.toString())
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnContinue
            .ninjaTap {
                binding.inputLayoutPassword.error = ""
                binding.root.hideKeyboard()
                viewModel
                    .verifyPassword(
                        binding.inputPassword.text.toString().trim()
                    )
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }
}
