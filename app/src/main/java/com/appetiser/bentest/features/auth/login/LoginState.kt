package com.appetiser.bentest.features.auth.login

import com.appetiser.module.domain.models.user.User

sealed class LoginState {

    /**
     * @param username email or phone number
     */
    data class GetUsername(val username: String) : LoginState()

    data class LoginSuccess(val user: User) : LoginState()

    data class UserNotVerified(
        val user: User,
        val email: String,
        val phone: String
    ) : LoginState()

    data class Error(val throwable: Throwable) : LoginState()

    object NoUserFirstAndLastName : LoginState()

    object NoProfilePhoto : LoginState()

    object ShowProgressLoading : LoginState()

    object HideProgressLoading : LoginState()
}
