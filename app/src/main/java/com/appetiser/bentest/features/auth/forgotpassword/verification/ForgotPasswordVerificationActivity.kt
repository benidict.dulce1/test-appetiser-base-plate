package com.appetiser.bentest.features.auth.forgotpassword.verification

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityForgotPasswordVerificationCodeBinding
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.features.auth.forgotpassword.newpassword.NewPasswordActivity
import com.appetiser.module.common.*
import com.jakewharton.rxbinding3.widget.textChangeEvents
import io.reactivex.BackpressureStrategy
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class ForgotPasswordVerificationActivity : BaseViewModelActivity<ActivityForgotPasswordVerificationCodeBinding, ForgotPasswordVerificationViewModel>() {

    companion object {

        fun openActivity(context: Context, username: String) {
            val intent = Intent(context, ForgotPasswordVerificationActivity::class.java)
            intent.putExtra(KEY_USERNAME, username)
            context.startActivity(intent)
        }

        const val KEY_USERNAME = "username"
    }

    override fun getLayoutId(): Int = R.layout.activity_forgot_password_verification_code

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupViews()
        setupToolbar()
        setupViewModels()
        observeInputViews()
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }

    private fun setupViews() {
        binding.inputCode.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    viewModel.sendToken(binding.inputCode.text.toString())
                    true
                } else {
                    false
                }
            }
        }

        disposables.add(
            binding.noCode.ninjaTap {
                viewModel.resendCode()
            }
        )
    }

    private fun observeInputViews() {
        binding.inputCode.textChangeEvents()
            .skipInitialValue()
            .observeOn(scheduler.ui())
            .map { it.text }
            .map {
                it.isNotEmpty() && it.length >= 5
            }
            .subscribeBy(
                onNext = {
                    if (it) {
                        viewModel.sendToken(binding.inputCode.text.toString())
                    }
                },
                onError = {
                    Timber.e(it)
                }
            ).apply { disposables.add(this) }
    }

    private fun setupViewModels() {
        viewModel.state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    when (state) {
                        is ForgotPasswordVerificationState.GetUsername -> {
                            binding.tvUsername.text = state.username
                        }

                        is ForgotPasswordVerificationState.ResendTokenSuccess -> {
                            toast("New code sent!")
                        }

                        is ForgotPasswordVerificationState.ForgotPasswordSuccess -> {
                            NewPasswordActivity
                                .openActivity(
                                    this@ForgotPasswordVerificationActivity,
                                    state.username,
                                    state.token
                                )
                            finish()
                        }

                        is ForgotPasswordVerificationState.Error -> {
                            toast(state.throwable.getThrowableError())
                        }

                        is ForgotPasswordVerificationState.ShowProgressLoading -> {
                            toast("Sending request")
                        }

                        is ForgotPasswordVerificationState.HideProgressLoading -> {
                        }
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }
}
