package com.appetiser.bentest.features.auth.walkthrough

import android.animation.*
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.viewpager2.widget.ViewPager2
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityWalkthroughBinding
import com.appetiser.bentest.features.auth.landing.LandingActivity
import com.appetiser.bentest.features.auth.register.details.InputNameActivity
import com.appetiser.bentest.features.main.MainActivity
import com.appetiser.bentest.utils.PAGE_STEP_1_POSITION
import com.appetiser.bentest.utils.PAGE_STEP_2_POSITION
import com.appetiser.bentest.utils.PAGE_STEP_3_POSITION
import com.appetiser.bentest.utils.PAGE_STEP_4_POSITION
import com.appetiser.module.common.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.gone
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.setHidden
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class WalkthroughActivity : BaseViewModelActivity<ActivityWalkthroughBinding, WalkthroughViewModel>() {

    companion object {
        fun openActivity(context: Context) {
            context.startActivity(
                Intent(
                    context,
                    WalkthroughActivity::class.java
                )
            )
        }
    }

    private var indicatorDefaultWidth: Int = 0
    private var indicatorSelectedWidth: Int = 0

    override fun getLayoutId(): Int = R.layout.activity_walkthrough

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initializePagerIndicator()

        window.decorView.setOnApplyWindowInsetsListener { _, windowInsets ->
            binding
                .container
                .apply {
                    setPadding(
                        paddingLeft,
                        paddingTop,
                        paddingRight,
                        windowInsets.systemWindowInsetBottom
                    )
                }
            windowInsets
        }

        setupViews()
        setupViewPager()
        setupVmObservers()
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    handleState(state)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: WalkthroughState) {
        when (state) {
            is WalkthroughState.UpdatePageIndicator -> {
                updatePageIndicator(
                    state.previousPage,
                    state.currentPage
                )
            }
            is WalkthroughState.ShowStep4Buttons -> {
                updatePageIndicator(
                    state.previousPage,
                    state.currentPage
                )

                showStep4Buttons()
            }
            is WalkthroughState.HideStep4Buttons -> {
                updatePageIndicator(
                    state.previousPage,
                    state.currentPage
                )

                hideStep4Buttons()
            }
            is WalkthroughState.UserIsNotLoggedIn -> {
                binding.cover.gone()
            }
            is WalkthroughState.UserIsLoggedIn -> {
                openMainActivity()
            }
            is WalkthroughState.UserIsLoggedInButNotOnboarded -> {
                openUserDetailsActivity()
            }
        }
    }

    private fun showStep4Buttons() {
        val indicatorAnimator =
            ObjectAnimator
                .ofFloat(
                    binding.pagerIndicatorContainer,
                    "y",
                    binding.pagerIndicatorContainer.y,
                    binding.pagerIndicatorContainer.y - (binding.btnContinue.height)
                )

        val btnAnimator =
            ObjectAnimator
                .ofFloat(
                    binding.btnContinue,
                    "alpha",
                    0f,
                    1f
                )
                .apply {
                    addListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationStart(animation: Animator?, isReverse: Boolean) {
                            super.onAnimationStart(animation, isReverse)
                            binding
                                .btnContinue
                                .setHidden(false)
                        }
                    })
                }

        AnimatorSet()
            .apply {
                playTogether(
                    indicatorAnimator,
                    btnAnimator
                )
                duration = 300
                start()
            }
    }

    private fun hideStep4Buttons() {
        val indicatorAnimator =
            ObjectAnimator
                .ofFloat(
                    binding.pagerIndicatorContainer,
                    "y",
                    binding.pagerIndicatorContainer.y,
                    binding.pagerIndicatorContainer.y + (binding.btnContinue.height)
                )

        val btnAnimator =
            ObjectAnimator
                .ofFloat(
                    binding.btnContinue,
                    "alpha",
                    1f,
                    0f
                )
                .apply {
                    addListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator?) {
                            super.onAnimationEnd(animation)
                            binding
                                .btnContinue
                                .setHidden(true)
                        }
                    })
                }

        AnimatorSet()
            .apply {
                playTogether(
                    indicatorAnimator,
                    btnAnimator
                )
                duration = 300
                start()
            }
    }

    /**
     * Animates view pager indicator when swiped.
     */
    private fun updatePageIndicator(previousPage: Int, currentPage: Int) {
        val currentIndicator = when (currentPage) {
            PAGE_STEP_1_POSITION -> binding.indicator1
            PAGE_STEP_2_POSITION -> binding.indicator2
            PAGE_STEP_3_POSITION -> binding.indicator3
            PAGE_STEP_4_POSITION -> binding.indicator4
            else -> throw IllegalArgumentException("Invalid currentPage!")
        }

        val previousIndicator = when (previousPage) {
            PAGE_STEP_1_POSITION -> binding.indicator1
            PAGE_STEP_2_POSITION -> binding.indicator2
            PAGE_STEP_3_POSITION -> binding.indicator3
            PAGE_STEP_4_POSITION -> binding.indicator4
            else -> throw IllegalStateException("Invalid previousPage!")
        }

        currentIndicator.isSelected = true
        previousIndicator.isSelected = false

        val current = ValueAnimator.ofInt(indicatorDefaultWidth, indicatorSelectedWidth)
            .apply {
                addUpdateListener { valueAnimator ->
                    val newWidth = valueAnimator.animatedValue as Int
                    currentIndicator.layoutParams.width = newWidth
                    currentIndicator.requestLayout()
                }
            }

        val previous = ValueAnimator.ofInt(indicatorSelectedWidth, indicatorDefaultWidth)
            .apply {
                addUpdateListener { valueAnimator ->
                    val newWidth = valueAnimator.animatedValue as Int
                    previousIndicator.layoutParams.width = newWidth
                    previousIndicator.requestLayout()
                }
            }

        AnimatorSet()
            .apply {
                playTogether(
                    current,
                    previous
                )
                duration = 300
                interpolator = AccelerateDecelerateInterpolator()
                start()
            }
    }

    private fun initializePagerIndicator() {
        indicatorDefaultWidth = resources.getDimensionPixelSize(R.dimen.default_pager_indicator_width)
        indicatorSelectedWidth =
            resources
                .getDimensionPixelSize(
                    R.dimen.selected_pager_indicator_width
                )

        // Select first dot.
        binding.indicator1.isSelected = true
        binding
            .indicator1
            .layoutParams
            .width = resources.getDimensionPixelSize(R.dimen.selected_pager_indicator_width)
    }

    private val pageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            viewModel.onPageSelected(position)
        }
    }

    private fun setupViewPager() {
        binding
            .viewPager
            .apply {
                adapter = WalkthroughAdapter()
                offscreenPageLimit = 3
                registerOnPageChangeCallback(pageChangeCallback)
            }
    }

    private fun setupViews() {
        binding
            .btnNext
            .ninjaTap {
                binding
                    .viewPager
                    .apply {
                        setCurrentItem(
                            currentItem + 1,
                            true
                        )
                    }
            }
            .addTo(disposables)

        Observable
            .merge(
                binding.btnSkip.clicks(),
                binding.btnContinue.clicks()
            )
            .throttleFirst(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = {
                    LandingActivity.openActivity(this)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun openMainActivity() {
        MainActivity.openActivity(this)
        finishAffinity()
    }

    private fun openUserDetailsActivity() {
        InputNameActivity.openActivity(this)
        finishAffinity()
    }

    override fun onDestroy() {
        binding
            .viewPager
            .unregisterOnPageChangeCallback(
                pageChangeCallback
            )
        super.onDestroy()
    }
}
