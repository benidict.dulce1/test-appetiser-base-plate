package com.appetiser.bentest.features.auth.register.phonenumber

import android.os.Bundle
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.utils.PhoneNumberHelper
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.domain.models.user.User
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class PhoneNumberInputViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val phoneNumberHelper: PhoneNumberHelper
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<PhoneNumberInputState>()
    }

    val state: Observable<PhoneNumberInputState> = _state

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun savePhoneNumber(
        countryCode: String,
        countryNameCode: String,
        phoneNumber: String
    ) {
        val formattedPhoneNumber =
            phoneNumberHelper
                .getInternationalFormattedPhoneNumber(
                    "$countryCode$phoneNumber",
                    countryNameCode
                )

        userRepository
            .updateUser(
                User
                    .empty()
                    .apply {
                        this.phoneNumber = formattedPhoneNumber
                    }
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(PhoneNumberInputState.ShowProgressLoading)
            }
            .doOnSuccess {
                _state.onNext(PhoneNumberInputState.HideProgressLoading)
            }
            .doOnError {
                _state.onNext(PhoneNumberInputState.HideProgressLoading)
            }
            .subscribeBy(
                onSuccess = { user ->
                    if (user.phoneNumber.isNotEmpty()) {
                        _state.onNext(PhoneNumberInputState.UserDetailsUpdated(user))
                    } else {
                        _state.onNext(PhoneNumberInputState.Error(Throwable("Something went wrong")))
                    }
                },
                onError = {
                    _state.onNext(PhoneNumberInputState.Error(it))
                }
            )
            .apply { disposables.add(this) }
    }
}
