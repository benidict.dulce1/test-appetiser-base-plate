package com.appetiser.bentest.features.notification

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelFragment
import com.appetiser.bentest.databinding.FragmentNotificationBinding
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class NotificationFragment : BaseViewModelFragment<FragmentNotificationBinding, NotificationViewModel>() {

    companion object {
        fun newInstance(): NotificationFragment {
            return NotificationFragment()
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_notification

    override fun setActivityAsViewModelProvider(): Boolean = true

    private val adapter: NotificationAdapter by lazy {
        NotificationAdapter(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel

        setupRecyclerView()
        setupViewModel()
    }

    private fun setupRecyclerView() {
        with(binding.notificationList) {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = this@NotificationFragment.adapter
        }
    }

    private fun setupViewModel() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }

    private fun handleState(state: NotificationState) {
        when (state) {
            is NotificationState.FetchNotificationItems -> {
                adapter.updateItems(state.items)
            }
        }
    }
}
