package com.appetiser.bentest.features.track.tracklist

import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.navigation.fragment.findNavController
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelFragment
import com.appetiser.bentest.databinding.FragmentTrackListBinding
import com.appetiser.bentest.features.track.TrackState
import com.appetiser.bentest.features.track.tracklist.adapter.TrackListAdapter
import com.appetiser.bentest.utils.currentDateTime
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.include_toolbar.*

class TrackListFragment : BaseViewModelFragment<FragmentTrackListBinding, TrackListViewModel>(), SwipeRefreshLayout.OnRefreshListener{

    override fun getLayoutId(): Int = R.layout.fragment_track_list

    override fun setActivityAsViewModelProvider(): Boolean = true

    private val adapter: TrackListAdapter by lazy {
        TrackListAdapter(requireContext()){
            viewModel.saveTrackDetails(it)
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.loadLastVisitDate()
        viewModel.loadTrackList()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel
        viewModel.saveLastVisitDate(currentDateTime())
        setupSwipeRefresh()
        setupRecyclerView()
        setupViewModel()
        setupToolbar()
    }

    private fun setupToolbar(){
        imgBack.visibility = GONE
        toolbarTitle.text = "Track"
    }

    private fun setupSwipeRefresh(){
        binding.swipeRefresh.setOnRefreshListener(this)
        binding.viewRefresh.setOnClickListener {
            binding.gpEmpty.visibility = GONE
            onRefresh()
        }
    }

    override fun onRefresh() {
        binding.swipeRefresh.isRefreshing = false
        viewModel.loadTrackList()
    }

    private fun setupRecyclerView(){
        with(binding.trackList){
            adapter = this@TrackListFragment.adapter
        }
    }

    private fun setupViewModel(){
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }

    private fun handleState(state: TrackState){
        when(state){
            is TrackState.ShowProgressLoading -> {
                binding.pbUserList.visibility = VISIBLE
            }
            is TrackState.HideProgressLoading -> {
                binding.pbUserList.visibility = GONE
            }
            is TrackState.Error -> {
                binding.gpEmpty.visibility = VISIBLE
            }
            is TrackState.TrackListSuccess -> {
                binding.gpEmpty.visibility = GONE
                adapter.updateItems(state.list)
            }
            is TrackState.TrackDetailsSuccess -> {
                findNavController().navigate(R.id.action_track_list_to_track_details)
            }
            is TrackState.LastVisitDateSuccess -> {
                binding.tvDate.text = state.date
            }
        }
    }
}