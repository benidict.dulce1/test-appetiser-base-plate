package com.appetiser.bentest.features.feed.create

import android.Manifest
import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseCameraActivity
import com.appetiser.bentest.databinding.ActivityCreateFeedBinding
import com.appetiser.bentest.ext.disabledWithAlpha
import com.appetiser.bentest.ext.enabledWithAlpha
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.ext.loadImageUrl
import com.appetiser.module.common.NINJA_TAP_THROTTLE_TIME
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.jakewharton.rxbinding3.view.focusChanges
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit

class CreateFeedActivity : BaseCameraActivity<ActivityCreateFeedBinding, CreateFeedViewModel>() {

    override fun getLayoutId(): Int = R.layout.activity_create_feed

    override fun canBack(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setupViewModel()
        setupViews()
        observeViews()
    }

    private fun setupViewModel() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: CreateFeedState) {
        when (state) {
            is CreateFeedState.CreatedFeedSuccess -> {
                toast("Success!")
                setResult(Activity.RESULT_OK)
                finish()
            }

            is CreateFeedState.Error -> {
                toast(state.throwable.getThrowableError())
            }

            is CreateFeedState.ShowProgressLoading -> {
                toast("Uploading")
            }

            is CreateFeedState.HideProgressLoading -> {
                toast("Done")
            }
        }
    }

    private fun setupViews() {
        binding.ivAttachment.ninjaTap {
            rxPermissions
                .requestEachCombined(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .doOnNext {
                    binding.etDescription.clearFocus()
                    when {
                        it.granted -> {
                            showPickerDialog()
                        }
                        else -> {
                            hidePickerDialog()
                        }
                    }
                }
                .subscribe()
        }

        binding.etDescription.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    viewModel.createFeed(binding.etDescription.text.toString())
                    true
                } else {
                    false
                }
            }
        }
    }

    private fun observeViews() {
        val imageObserver = binding.ivAttachment.imageChanges()
            .map { it }

        val descriptionObserver = binding.etDescription.textChanges()
            .map { it.isNotEmpty() }

        disposables.add(
            Observable.combineLatest<Boolean, Boolean, Boolean>(
                imageObserver, descriptionObserver,
                BiFunction { imageChange, isNotEmpty ->
                    return@BiFunction imageChange && isNotEmpty
                }
            )
                .debounce(NINJA_TAP_THROTTLE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(scheduler.ui())
                .subscribeBy(
                    onError = {
                        toast(it.message.orEmpty())
                    },
                    onNext = {
                        if (it) {
                            binding.btnShare.enabledWithAlpha()
                        } else {
                            binding.btnShare.disabledWithAlpha()
                        }
                    }
                )
        )

        binding.etDescription
            .focusChanges()
            .skipInitialValue()
            .subscribeBy(
                onNext = {
                    if (it) {
                        with(binding.nestedScrollView) {
                            postDelayed(
                                {
                                    fullScroll(View.FOCUS_DOWN)
                                },
                                450
                            )
                        }
                    }
                }
            )
            .addTo(disposables)

        binding.btnShare
            .ninjaTap {
                viewModel.createFeed(binding.etDescription.text.toString())
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarTitle(getString(R.string.create_a_post))
    }

    override fun setImagePathUrl(captureCameraPath: String) {
        if (captureCameraPath.isNotEmpty()) {
            viewModel.imagePath = captureCameraPath
            binding.ivAttachment.triggerChanges()
            binding.ivAttachment.loadImageUrl(captureCameraPath)
        } else {
            toast(getString(R.string.invalid_file))
        }
    }
}
