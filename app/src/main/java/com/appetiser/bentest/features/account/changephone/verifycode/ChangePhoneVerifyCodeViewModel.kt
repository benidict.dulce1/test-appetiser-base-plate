package com.appetiser.bentest.features.account.changephone.verifycode

import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.ext.getThrowableError
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import timber.log.Timber
import javax.inject.Inject

class ChangePhoneVerifyCodeViewModel @Inject constructor(
    private val authRepository: AuthRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<ChangePhoneVerifyCodeState>()
    }

    val state: Observable<ChangePhoneVerifyCodeState> = _state

    private lateinit var phoneNumber: String
    private lateinit var verificationToken: String

    /**
     * Copy of previously failed code.
     *
     * This is so we don't repetitively send wrong code when user inputs the same one.
     */
    private var failedCode: String = ""

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun setPhoneAndVerificationToken(
        phoneNumber: String,
        verificationToken: String
    ) {
        this.phoneNumber = phoneNumber
        this.verificationToken = verificationToken

        _state
            .onNext(
                ChangePhoneVerifyCodeState.DisplayPhone(
                    phoneNumber
                )
            )
    }

    fun onCodeTextChanged(code: String) {
        if (code.length < 5 || code == failedCode) return

        authRepository
            .verifyChangePhone(
                verificationToken,
                code
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state
                    .onNext(
                        ChangePhoneVerifyCodeState.HideKeyboard
                    )
                showLoading()
            }
            .doOnComplete {
                hideLoading()
            }
            .doOnError {
                hideLoading()
            }
            .subscribeBy(
                onComplete = {
                    _state
                        .onNext(
                            ChangePhoneVerifyCodeState.VerificationSuccess
                        )
                },
                onError = { error ->
                    Timber.e(error)
                    handleVerificationError(error, code)
                }
            )
            .addTo(disposables)
    }

    private fun handleVerificationError(error: Throwable, code: String) {
        if (error is HttpException && error.code() == 400) {
            failedCode = code
            _state
                .onNext(
                    ChangePhoneVerifyCodeState.InvalidVerificationCode(
                        error.getThrowableError()
                    )
                )
        } else {
            showGenericError()
        }
    }

    fun resendCode() {
        authRepository
            .requestChangePhone(
                verificationToken,
                phoneNumber
            )
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                showLoading()
            }
            .doOnComplete {
                hideLoading()
            }
            .doOnError {
                hideLoading()
            }
            .subscribeBy(
                onComplete = {
                    _state
                        .onNext(
                            ChangePhoneVerifyCodeState.ResendCodeSuccess
                        )
                },
                onError = {
                    Timber.e(it)
                    showGenericError()
                }
            )
            .addTo(disposables)
    }

    private fun showLoading() {
        _state
            .onNext(
                ChangePhoneVerifyCodeState.ShowLoading
            )
    }

    private fun hideLoading() {
        _state
            .onNext(
                ChangePhoneVerifyCodeState.HideLoading
            )
    }

    private fun showGenericError() {
        _state
            .onNext(
                ChangePhoneVerifyCodeState.Error(
                    resourceManager.getString(
                        R.string.generic_error_short
                    )
                )
            )
    }
}
