package com.appetiser.bentest.features.auth.register.createpassword

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityCreatePasswordBinding
import com.appetiser.bentest.features.auth.emailcheck.EmailCheckActivity
import com.appetiser.bentest.features.auth.phonenumbercheck.PhoneNumberCheckActivity
import com.appetiser.bentest.features.auth.register.verification.RegisterVerificationCodeActivity
import com.appetiser.module.common.TEXT_WATCHER_DEBOUNCE_TIME
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.widget.CustomPasswordTransformation
import com.appetiser.module.domain.utils.PASSWORD_MAX_LENGTH
import com.appetiser.module.domain.utils.PASSWORD_MIN_LENGTH
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class CreatePasswordActivity : BaseViewModelActivity<ActivityCreatePasswordBinding, CreatePasswordViewModel>() {

    companion object {
        /**
         * @param email if coming from [EmailCheckActivity]
         * @param phone if coming from [PhoneNumberCheckActivity]
         */
        fun openActivity(context: Context, email: String = "", phone: String = "") {
            val intent = Intent(context, CreatePasswordActivity::class.java)
            intent.putExtra(KEY_EMAIL, email)
            intent.putExtra(KEY_PHONE, phone)
            context.startActivity(intent)
        }

        const val KEY_EMAIL = "email"
        const val KEY_PHONE = "phone"
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_create_password
    }

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val email = intent.extras?.getString(KEY_EMAIL) ?: ""
        val phone = intent.extras?.getString(KEY_PHONE) ?: ""

        check(email.isNotEmpty() || phone.isNotEmpty()) {
            "Required parameter email or phone not found!"
        }

        setupToolbar()
        observeInputViews()
        setVmObservers()

        viewModel.setEmailOrPhone(email, phone)
    }

    private fun setVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: CreatePasswordState) {
        when (state) {
            is CreatePasswordState.FillUsername -> {
                binding
                    .txtUsername
                    .text = state.username
            }
            CreatePasswordState.EnableButton -> {
                binding.btnContinue.isEnabled = true
            }
            CreatePasswordState.DisableButton -> {
                binding.btnContinue.isEnabled = false
            }
            CreatePasswordState.PasswordBelowMinLength -> {
                binding
                    .etPasswordLayout
                    .error = getString(R.string.password_below_minimum_format, PASSWORD_MIN_LENGTH)
            }
            CreatePasswordState.PasswordExceedsMaxLength -> {
                binding
                    .etPasswordLayout
                    .error = getString(R.string.password_exceeds_maximum_format, PASSWORD_MAX_LENGTH)
            }
            CreatePasswordState.ShowLoading -> {
                disableFields()
            }
            CreatePasswordState.HideLoading -> {
                enableFields()
            }
            is CreatePasswordState.StartVerificationCodeScreen -> {
                startVerificationCodeActivity(
                    state.email,
                    state.phone
                )
            }
        }
    }

    private fun enableFields() {
        binding
            .etPassword
            .isEnabled = true

        binding
            .btnContinue
            .isEnabled = true
    }

    private fun disableFields() {
        binding
            .etPassword
            .isEnabled = false

        binding
            .btnContinue
            .isEnabled = false
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
        setupData()
    }

    private fun observeInputViews() {
        binding
            .etPassword
            .apply {
                transformationMethod = CustomPasswordTransformation()
            }

        binding
            .etPassword
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribe {
                viewModel.onPasswordTextChange(it.toString())
            }
            .addTo(disposables)
    }

    private fun setupData() {
        binding.etPassword.apply {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    if (binding.btnContinue.isEnabled) {
                        savePassword()
                    }
                    true
                } else {
                    false
                }
            }
        }

        binding
            .btnContinue
            .ninjaTap {
                savePassword()
            }
            .addTo(disposables)
    }

    private fun savePassword() {
        viewModel
            .savePassword(
                binding.etPassword.text.toString().trim()
            )
    }

    private fun startVerificationCodeActivity(email: String, phone: String) {
        RegisterVerificationCodeActivity
            .openActivity(
                this,
                email = email,
                phone = phone
            )
    }
}
