package com.appetiser.bentest.features.feed.list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.postDelayed
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelFragment
import com.appetiser.bentest.databinding.FragmentFeedsBinding
import com.appetiser.bentest.features.feed.details.FeedDetailsActivity
import com.appetiser.bentest.features.report.feed.ReportFeedActivity
import com.google.gson.Gson
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

class FeedsFragment : BaseViewModelFragment<FragmentFeedsBinding, FeedsViewModel>() {

    companion object {
        fun newInstance(): FeedsFragment {
            return FeedsFragment()
        }
    }

    @Inject
    lateinit var gson: Gson

    private val linearLayoutManager by lazy {
        LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
    }

    private lateinit var adapter: FeedsAdapter

    override fun getLayoutId(): Int = R.layout.fragment_feeds

    override fun setActivityAsViewModelProvider(): Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.vm = viewModel

        setupRecyclerView()
        setupViewModel()
        viewModel.fetchFeeds()
    }

    private val startFeedDetailsForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val position = linearLayoutManager.findFirstVisibleItemPosition()
            val item = adapter.getFeed(position)
            viewModel.feedDetails(item.id, position)
        }
    }

    private fun setupRecyclerView() {
        adapter = FeedsAdapter(disposables, gson)
        with(binding.feedList) {
            itemAnimator = null
            layoutAnimation = null
            clearAnimation()
            layoutManager = linearLayoutManager
        }
        binding.feedList.adapter = adapter.withLoadStateFooter(
            footer = FeedsLoadStateAdapter(adapter)
        )

        adapter.itemClickListener
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = { state ->
                    when (state) {
                        is FeedItemState.ShowFeedDetails -> {
                            val intent = Intent(requireContext(), FeedDetailsActivity::class.java)
                            intent.putExtra(FeedDetailsActivity.KEY_FEED_ID, state.item.id)
                            startFeedDetailsForResult.launch(intent)
                        }
                        is FeedItemState.FavoriteClicked -> {
                            val position = linearLayoutManager.findFirstVisibleItemPosition()
                            viewModel.toggleFavorite(feed = state.item, position = position)
                        }

                        is FeedItemState.CommentClicked -> {
                            val intent = Intent(requireContext(), FeedDetailsActivity::class.java)
                            intent.putExtra(FeedDetailsActivity.KEY_FEED_ID, state.item.id)
                            intent.putExtra(FeedDetailsActivity.KEY_COMMENT_CLICKED, true)
                            startFeedDetailsForResult.launch(intent)
                        }

                        is FeedItemState.DeleteFeedClicked -> {
                            viewModel.deleteFeed(state.item, state.position)
                        }

                        is FeedItemState.ReportFeedClicked -> {
                            ReportFeedActivity.openActivity(requireActivity(), state.item.id)
                        }
                    }
                }
            )
            .addTo(disposables)

        adapter.addLoadStateListener { loadState ->
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error

            errorState?.let {
                Timber.e("onLoadStateError: ${it.error.message}")
            }
        }
    }

    private fun setupViewModel() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }

    private fun handleState(state: FeedsState) {
        when (state) {
            is FeedsState.UpdateFeed -> {
                adapter.updateFeed(state.item)
            }
            is FeedsState.RemoveFeed -> {
                adapter.refresh()
                binding.feedList.postDelayed({ binding.feedList.scrollToPosition(state.position) }, 500)
            }

            is FeedsState.RefreshFeeds -> {
                adapter.refresh()
            }

            is FeedsState.Error -> {
                adapter.refresh()
            }

            is FeedsState.FeedsFetched -> {
                adapter.updateItems(lifecycle, state.items)
            }
        }
    }

    fun scrollToTop() {
        adapter.refresh()
        binding.feedList.postDelayed({ binding.feedList.smoothScrollToPosition(0) }, 500)
    }
}
