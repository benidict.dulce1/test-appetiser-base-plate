package com.appetiser.bentest.features.feed.list

import androidx.recyclerview.widget.DiffUtil
import com.appetiser.module.domain.models.feed.Feed
import com.google.gson.Gson

class FeedsDiffCallback(private val gson: Gson) : DiffUtil.ItemCallback<Feed>() {

    override fun areItemsTheSame(oldItem: Feed, newItem: Feed): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Feed, newItem: Feed): Boolean {
        return gson.toJson(oldItem) == gson.toJson(newItem)
    }
}
