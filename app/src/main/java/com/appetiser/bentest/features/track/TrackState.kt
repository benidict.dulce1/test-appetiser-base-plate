package com.appetiser.bentest.features.track

import com.appetiser.module.domain.models.track.Track

sealed class TrackState{
    data class TrackListSuccess(val list: List<Track>): TrackState()
    data class Error(val throwable: Throwable) : TrackState()
    object ShowProgressLoading : TrackState()
    object HideProgressLoading : TrackState()

    data class TrackDetailsSuccess(val track: Track): TrackState()

    data class LastVisitDateSuccess(val date: String): TrackState()

}