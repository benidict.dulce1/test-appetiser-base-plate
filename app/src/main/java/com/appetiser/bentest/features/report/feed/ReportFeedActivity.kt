package com.appetiser.bentest.features.report.feed

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseCameraActivity
import com.appetiser.bentest.databinding.ActivityReportFeedBinding
import com.appetiser.bentest.features.report.adapter.*
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.domain.models.miscellaneous.ReportCategory
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class ReportFeedActivity : BaseCameraActivity<ActivityReportFeedBinding, ReportFeedViewModel>() {

    companion object {
        fun openActivity(context: Context, feedId: Long) {
            val intent = Intent(context, ReportFeedActivity::class.java)
            intent.putExtra(KEY_FEED_ID, feedId)
            context.startActivity(intent)
        }

        const val KEY_FEED_ID = "feed_id"
    }

    private lateinit var reportCategoryAdapter: ReportCategoryAdapter

    private val adapter: ReportAttachmentsAdapter by lazy {
        ReportAttachmentsAdapter(this, onItemClickListener)
    }

    private val onItemClickListener = object : OnReportListener {
        override fun addPhoto() {
            launchPhotoChooser()
        }

        override fun deletePhoto(item: ReportAttachmentsViewTypeObject) {
            deleteUploadedPhoto(item)
        }
    }

    override fun getLayoutId() = R.layout.activity_report_feed

    override fun canBack(): Boolean {
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViewModel()
        setupRecyclerView()

        binding.btnReport.ninjaTap {
            reportFeed(
                this@ReportFeedActivity,
                reportCategoryAdapter.getItem(binding.spinnerReason.selectedItemPosition)?.id?.toLong()
                    ?: 0,
                binding.etDescription.text.toString()
            )
        }.addTo(disposables)

        binding.btnClose.ninjaTap {
            finish()
        }.addTo(disposables)
    }

    private fun setupViewModel() {
        viewModel.state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }

    private fun handleState(state: ReportFeedState) {
        when (state) {
            is ReportFeedState.DisplayAddButton -> handleDisplayAddButton()
            is ReportFeedState.AddPhoto -> handleAddPhoto(state)
            is ReportFeedState.RemovePhoto -> handleRemovePhoto(state)
            is ReportFeedState.ReportCategoriesFetched -> handleReportCategoriesSuccess(state.reportCategories)
            is ReportFeedState.ReportSuccess -> handleReportSuccess()
            is ReportFeedState.ShowProgressLoading -> handleShowProgress()
            is ReportFeedState.HideProgressLoading -> handleHideProgress()
            is ReportFeedState.Error -> handleError(state.throwable.message)
        }
    }

    private fun handleReportSuccess() {
        toast("Report submitted!")
        finish()
    }

    private fun handleHideProgress() {
        // TODO: Handle hide progress
    }

    private fun handleShowProgress() {
        toast("Sending request")
    }

    private fun handleRemovePhoto(state: ReportFeedState.RemovePhoto) {
        adapter.removeSingleItem(state.item)
    }

    private fun handleDisplayAddButton() {
        adapter.addSingleItem(ReportAttachmentsViewTypeObject(ReportAttachmentsViewType.DISPLAY_ADD_BTN, ""))
    }

    private fun handleAddPhoto(state: ReportFeedState.AddPhoto) {
        adapter.addSingleItem(ReportAttachmentsViewTypeObject(ReportAttachmentsViewType.DISPLAY_PHOTO, state.imagePath))
    }

    private fun handleError(message: String?) {
        toast(message.orEmpty())
    }

    private fun handleReportCategoriesSuccess(reportCategories: List<ReportCategory>) {
        binding
            .spinnerReason
            .apply {
                reportCategoryAdapter = ReportCategoryAdapter(this@ReportFeedActivity, reportCategories)
                adapter = reportCategoryAdapter
            }
    }

    private fun setupRecyclerView() {
        with(binding.photoList) {
            layoutManager = GridLayoutManager(this@ReportFeedActivity, 3)
            adapter = this@ReportFeedActivity.adapter
        }
        viewModel.displayAddPhotoButton(ReportAttachmentsViewType.DISPLAY_ADD_BTN)
    }

    private fun deleteUploadedPhoto(item: ReportAttachmentsViewTypeObject) {
        viewModel.removePhoto(item)
    }

    private fun launchPhotoChooser() {
        rxPermissions.requestEachCombined(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
            .doOnNext {
                when {
                    it.granted -> {
                        showPickerDialog()
                    }
                    else -> {
                        hidePickerDialog()
                    }
                }
            }
            .subscribe()
    }

    override fun setImagePathUrl(captureCameraPath: String) {
        viewModel.addPhoto(captureCameraPath)
    }

    private fun reportFeed(
        context: Context,
        reasonId: Long,
        description: String
    ) {
        viewModel.reportFeed(
            context, reasonId, description
        )
    }
}
