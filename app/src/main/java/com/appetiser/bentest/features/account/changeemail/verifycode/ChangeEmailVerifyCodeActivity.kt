package com.appetiser.bentest.features.account.changeemail.verifycode

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModelActivity
import com.appetiser.bentest.databinding.ActivityChangeEmailVerifyCodeBinding
import com.appetiser.bentest.features.account.settings.AccountSettingsActivity
import com.appetiser.module.common.*
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.util.concurrent.TimeUnit

class ChangeEmailVerifyCodeActivity : BaseViewModelActivity<ActivityChangeEmailVerifyCodeBinding, ChangeEmailVerifyCodeViewModel>() {

    companion object {
        private const val EXTRA_EMAIL = "EXTRA_EMAIL"
        private const val EXTRA_VERIFICATION_TOKEN = "EXTRA_VERIFICATION_TOKEN"

        fun openActivity(
            context: Context,
            email: String,
            verificationToken: String
        ) {
            context.startActivity(
                Intent(
                    context,
                    ChangeEmailVerifyCodeActivity::class.java
                ).apply {
                    putExtra(EXTRA_EMAIL, email)
                    putExtra(EXTRA_VERIFICATION_TOKEN, verificationToken)
                }
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_change_email_verify_code

    override fun canBack(): Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        require(
            intent.extras != null &&
                intent.extras!!.getString(EXTRA_EMAIL, "").isNotEmpty() &&
                intent.extras!!.getString(EXTRA_VERIFICATION_TOKEN, "").isNotEmpty()
        ) {
            "Required intent extra `email` or `verificationToken` is missing!"
        }

        val email = intent.extras!!.getString(EXTRA_EMAIL, "")
        val verificationToken = intent.extras!!.getString(EXTRA_VERIFICATION_TOKEN, "")

        setupToolbar()
        setupViews()
        setupVmObservers()

        viewModel
            .setEmailAndVerificationToken(
                email,
                verificationToken
            )
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: ChangeEmailVerifyCodeState) {
        when (state) {
            is ChangeEmailVerifyCodeState.DisplayEmail -> {
                binding
                    .txtEmail
                    .text = state.email
            }
            ChangeEmailVerifyCodeState.ShowLoading -> {
                binding.root.hideKeyboard()
            }
            ChangeEmailVerifyCodeState.ShowLoading -> {
                binding.loading setVisible true
            }
            ChangeEmailVerifyCodeState.HideLoading -> {
                binding.loading setVisible false
            }
            is ChangeEmailVerifyCodeState.ResendCodeSuccess -> {
                showGenericSuccessSnackBar(
                    binding.root,
                    getString(R.string.resent_code_to_email)
                )
            }
            is ChangeEmailVerifyCodeState.VerificationSuccess -> {
                AccountSettingsActivity
                    .openActivity(
                        this,
                        Bundle().apply {
                            putBoolean(
                                AccountSettingsActivity.EXTRA_FROM_CHANGE_EMAIL,
                                true
                            )
                        },
                        Intent.FLAG_ACTIVITY_CLEAR_TOP
                    )
            }
            is ChangeEmailVerifyCodeState.InvalidVerificationCode -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
            is ChangeEmailVerifyCodeState.Error -> {
                showGenericErrorSnackBar(
                    binding.root,
                    state.message
                )
            }
        }
    }

    private fun setupViews() {
        binding
            .inputCode
            .textChanges()
            .skipInitialValue()
            .debounce(TEXT_WATCHER_DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
            .subscribeBy(
                onNext = { text ->
                    viewModel.onCodeTextChanged(text.toString())
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)

        binding
            .btnResend
            .ninjaTap {
                viewModel
                    .resendCode()
            }
            .addTo(disposables)
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator()
        setToolbarNoTitle()
    }
}
