package com.appetiser.bentest.features.profile.editprofile

import android.os.Bundle
import com.appetiser.bentest.R
import com.appetiser.bentest.base.BaseViewModel
import com.appetiser.bentest.utils.DEFAULT_DOB_SUBTRACTION
import com.appetiser.bentest.utils.DateUtils
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.domain.models.Session
import com.appetiser.module.domain.models.user.User
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.time.LocalDate
import javax.inject.Inject

class EditProfileViewModel @Inject constructor(
    private val sessionRepository: SessionRepository,
    private val userRepository: UserRepository,
    private val resourceManager: ResourceManager
) : BaseViewModel() {

    companion object {
        private const val DATE_NOT_SET = -1
    }

    private val descriptionMaxLength by lazy {
        resourceManager.getInteger(R.integer.profile_description_max_length)
    }

    private val _state by lazy {
        PublishSubject.create<EditProfileState>()
    }

    val state: Observable<EditProfileState> = _state

    private val defaultDob by lazy {
        LocalDate
            .now()
            .minusYears(DEFAULT_DOB_SUBTRACTION)
    }
    private lateinit var user: User

    private var dobYear: Int = DATE_NOT_SET
    private var dobMonth: Int = DATE_NOT_SET
    private var dobDay: Int = DATE_NOT_SET

    private var prevSavedDobYear: Int = DATE_NOT_SET
    private var prevSavedDobMonth: Int = DATE_NOT_SET
    private var prevSavedDobDay: Int = DATE_NOT_SET

    private var imageLocalPath = ""

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun loadProfile() {
        sessionRepository
            .getSession()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = ::handleSession,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleSession(session: Session) {
        user = session.user
        user.run {
            _state
                .onNext(
                    EditProfileState.PreFillFields(
                        avatar.thumbUrl,
                        fullName,
                        description
                    )
                )

            if (birthDate.isNotEmpty()) {
                val parsedBirthDate = DateUtils.parseBirthDate(birthDate)
                _state
                    .onNext(
                        EditProfileState.DisplayDob(
                            parsedBirthDate.year,
                            parsedBirthDate.monthValue,
                            parsedBirthDate.dayOfMonth
                        )
                    )
            }
        }
    }

    fun saveProfile(
        fullName: String,
        description: String
    ) {
        if (!validateFields(fullName, description)) return

        updateProfile(fullName, description)
            .observeOn(schedulers.ui())
            .doOnSubscribe {
                _state.onNext(EditProfileState.ShowLoading)
            }
            .doOnSuccess {
                _state.onNext(EditProfileState.HideLoading)
            }
            .doOnError {
                _state.onNext(EditProfileState.HideLoading)
            }
            .subscribeBy(
                onSuccess = { user ->
                    setPreviousSavedDob()

                    this.user = user
                    _state
                        .onNext(
                            EditProfileState.UpdateProfileSuccess
                        )
                },
                onError = {
                    Timber.e(it)
                    _state
                        .onNext(
                            EditProfileState.Error(
                                resourceManager.getString(R.string.generic_error_short)
                            )
                        )
                }
            )
            .addTo(disposables)
    }

    private fun setPreviousSavedDob() {
        prevSavedDobYear = dobYear
        prevSavedDobMonth = dobMonth
        prevSavedDobDay = dobDay
    }

    private fun validateFields(fullName: String, description: String): Boolean {
        if (!validateNameNotEmpty(fullName)) {
            _state
                .onNext(
                    EditProfileState.DisableSaveButton
                )
            return false
        }

        if (description.length > descriptionMaxLength) {
            _state
                .onNext(
                    EditProfileState.DescriptionExceedsMaxLength(
                        descriptionMaxLength
                    )
                )
            return false
        }

        return true
    }

    /**
     * Returns Single<User> for updating profile.
     * Checks whether photo should be uploaded or not.
     */
    private fun updateProfile(fullName: String, description: String): Single<User> {
        val updateDetails =
            updateDetails(
                fullName,
                description
            ).subscribeOn(schedulers.io())

        return if (imageLocalPath.isNotEmpty()) {
            val uploadPhoto = uploadPhoto().subscribeOn(schedulers.io())
            uploadPhoto
                .mergeWith(updateDetails)
                .lastOrError()
        } else {
            updateDetails
        }
    }

    private fun uploadPhoto(): Single<User> {
        return userRepository
            .uploadPhoto(imageLocalPath)
    }

    private fun updateDetails(
        fullName: String,
        description: String
    ): Single<User> {
        return userRepository
            .updateUser(
                User
                    .empty()
                    .apply {
                        this.fullName = fullName
                        this.description = description

                        if (isDateSet()) {
                            this.birthDate = "$dobYear-$dobMonth-$dobDay"
                        }
                    }
            )
    }

    fun onProfilePhotoChanged(imageLocalPath: String) {
        this.imageLocalPath = imageLocalPath

        _state
            .onNext(
                EditProfileState.DisplayNewImage(
                    imageLocalPath
                )
            )
    }

    fun onDobSelected(year: Int, monthOfYear: Int, dayOfMonth: Int) {
        dobYear = year
        dobMonth = monthOfYear
        dobDay = dayOfMonth

        _state
            .onNext(
                EditProfileState.DisplayDob(
                    dobYear,
                    dobMonth,
                    dobDay
                )
            )
    }

    fun onNameTextChanged(name: String) {
        if (validateNameNotEmpty(name)) {
            _state
                .onNext(
                    EditProfileState.EnableSaveButton
                )
        } else {
            _state
                .onNext(
                    EditProfileState.DisableSaveButton
                )
        }
    }

    fun onDatePickerClick() {
        val year: Int
        val month: Int
        val day: Int

        when {
            isDateSet() -> {
                year = dobYear
                month = dobMonth
                day = dobDay
            }
            user.birthDate.isNotEmpty() -> {
                val parsedBirthDate = DateUtils.parseBirthDate(user.birthDate)
                year = parsedBirthDate.year
                month = parsedBirthDate.monthValue
                day = parsedBirthDate.dayOfMonth
            }
            else -> {
                year = defaultDob.year
                month = defaultDob.monthValue
                day = defaultDob.dayOfMonth
            }
        }

        _state
            .onNext(
                EditProfileState.ShowDatePicker(
                    year,
                    month,
                    day
                )
            )
    }

    fun onBackPressed(fullName: String, description: String) {
        val hasNameChanges = user.fullName != fullName
        val hasDescriptionChanges = user.description != description
        val hasDobChanges = isDateSet() && hasDobChangesAfterSave()

        if (hasNameChanges || hasDescriptionChanges || hasDobChanges) {
            _state
                .onNext(
                    EditProfileState.ShowUnsavedChangesDialog
                )
        } else {
            _state
                .onNext(
                    EditProfileState.NavigateUp
                )
        }
    }

    private fun validateNameNotEmpty(fullName: String): Boolean {
        return fullName.isNotEmpty()
    }

    private fun isDateSet(): Boolean {
        return dobYear != DATE_NOT_SET &&
            dobMonth != DATE_NOT_SET &&
            dobDay != DATE_NOT_SET
    }

    private fun hasDobChangesAfterSave(): Boolean {
        return dobYear != prevSavedDobYear &&
            dobMonth != prevSavedDobMonth &&
            dobDay != prevSavedDobDay
    }
}
