package com.appetiser.bentest.di

import com.appetiser.bentest.utils.schedulers.BaseSchedulerProvider
import com.appetiser.bentest.utils.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulerModule {

    @Provides
    @Singleton
    fun providesSchedulerSource(): BaseSchedulerProvider =
        SchedulerProvider.getInstance()
}
