package com.appetiser.bentest.di.builders

import com.appetiser.bentest.di.scopes.ServiceScope
import com.appetiser.bentest.services.fcm.AppFirebaseMessagingService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceBuilder {

    @ServiceScope
    @ContributesAndroidInjector
    abstract fun contributeAppFirebaseMessagingService(): AppFirebaseMessagingService
}
