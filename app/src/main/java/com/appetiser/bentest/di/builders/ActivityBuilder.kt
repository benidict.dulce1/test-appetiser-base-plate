package com.appetiser.bentest.di.builders

import com.appetiser.bentest.features.track.BaseMainHostActivity
import com.appetiser.bentest.di.scopes.ActivityScope
import com.appetiser.bentest.features.account.about.AboutActivity
import com.appetiser.bentest.features.account.AccountActivity
import com.appetiser.bentest.features.account.about.WebViewActivity
import com.appetiser.bentest.features.account.changeemail.newemail.ChangeEmailNewEmailActivity
import com.appetiser.bentest.features.account.changeemail.verifycode.ChangeEmailVerifyCodeActivity
import com.appetiser.bentest.features.account.changeemail.verifypassword.ChangeEmailVerifyPasswordActivity
import com.appetiser.bentest.features.account.changepassword.currentpassword.ChangeCurrentPasswordActivity
import com.appetiser.bentest.features.account.changepassword.newpassword.ChangeNewPasswordActivity
import com.appetiser.bentest.features.account.changephone.newphone.ChangePhoneNewPhoneActivity
import com.appetiser.bentest.features.account.changephone.verifycode.ChangePhoneVerifyCodeActivity
import com.appetiser.bentest.features.account.changephone.verifypassword.ChangePhoneVerifyPasswordActivity
import com.appetiser.bentest.features.account.deleteaccount.DeleteAccountPasswordActivity
import com.appetiser.bentest.features.account.settings.AccountSettingsActivity
import com.appetiser.bentest.features.auth.emailcheck.EmailCheckActivity
import com.appetiser.bentest.features.auth.forgotpassword.ForgotPasswordActivity
import com.appetiser.bentest.features.auth.forgotpassword.newpassword.NewPasswordActivity
import com.appetiser.bentest.features.auth.forgotpassword.verification.ForgotPasswordVerificationActivity
import com.appetiser.bentest.features.auth.landing.LandingActivity
import com.appetiser.bentest.features.auth.login.LoginActivity
import com.appetiser.bentest.features.auth.phonenumbercheck.PhoneNumberCheckActivity
import com.appetiser.bentest.features.auth.register.phonenumber.PhoneNumberInputActivity
import com.appetiser.bentest.features.auth.register.createpassword.CreatePasswordActivity
import com.appetiser.bentest.features.auth.register.details.InputNameActivity
import com.appetiser.bentest.features.auth.register.profile.UploadProfilePhotoActivity
import com.appetiser.bentest.features.auth.register.verification.RegisterVerificationCodeActivity
import com.appetiser.bentest.features.auth.subscription.SubscriptionActivity
import com.appetiser.bentest.features.auth.walkthrough.WalkthroughActivity
import com.appetiser.bentest.features.feed.create.CreateFeedActivity
import com.appetiser.bentest.features.feed.details.FeedDetailsActivity
import com.appetiser.bentest.features.main.MainActivity
import com.appetiser.bentest.features.profile.editprofile.EditProfileActivity
import com.appetiser.bentest.features.report.feed.ReportFeedActivity
import com.appetiser.bentest.features.report.user.ReportUserActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeEmailCheckActivity(): EmailCheckActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeLoginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeForgotPasswordActivity(): ForgotPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeRegisterVerificationActivity(): RegisterVerificationCodeActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeRegisterActivity(): PhoneNumberInputActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeForgotPasswordVerificationActivity(): ForgotPasswordVerificationActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeNewPasswordActivity(): NewPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeUserDetailsActivity(): InputNameActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeLandingActivity(): LandingActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeWalkthroughActivity(): WalkthroughActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeCreatePasswordActivity(): CreatePasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeUploadProfilePhotoActivity(): UploadProfilePhotoActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeSubscriptionActivity(): SubscriptionActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributePhoneNumberCheckActivity(): PhoneNumberCheckActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeReportUserActivity(): ReportUserActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeReportFeedActivity(): ReportFeedActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeFeedDetailsActivity(): FeedDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeCreateFeedActivity(): CreateFeedActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeAccountActivity(): AccountActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeEditProfileActivity(): EditProfileActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeAccountSettingsActivity(): AccountSettingsActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeAboutActivity(): AboutActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeAboutContainerActivity(): WebViewActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangeCurrentPasswordActivity(): ChangeCurrentPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangeNewPasswordActivity(): ChangeNewPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangeEmailVerifyPasswordActivity(): ChangeEmailVerifyPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangeEmailNewEmailActivity(): ChangeEmailNewEmailActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangeEmailVerifyCodeActivity(): ChangeEmailVerifyCodeActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangePhoneVerifyPasswordActivity(): ChangePhoneVerifyPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangePhoneNewPhoneActivity(): ChangePhoneNewPhoneActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeChangePhoneVerifyCodeActivity(): ChangePhoneVerifyCodeActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeDeleteAccountPasswordActivity(): DeleteAccountPasswordActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeBaseMainHostActivity(): BaseMainHostActivity
}
