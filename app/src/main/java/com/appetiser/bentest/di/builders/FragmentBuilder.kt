package com.appetiser.bentest.di.builders

import com.appetiser.bentest.di.scopes.FragmentScope
import com.appetiser.bentest.features.profile.ProfileFragment
import com.appetiser.bentest.features.feed.list.FeedsFragment
import com.appetiser.bentest.features.main.DummyFragment
import com.appetiser.bentest.features.notification.NotificationFragment
import com.appetiser.bentest.features.splash.SplashFragment
import com.appetiser.bentest.features.track.trackdetails.TrackDetailsFragment
import com.appetiser.bentest.features.track.tracklist.TrackListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeNotificationFragment(): NotificationFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeFeedsFragment(): FeedsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeAccountFragment(): ProfileFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeDummyFragment(): DummyFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeSplashFragment(): SplashFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeTrackListFragment(): TrackListFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeTrackDetailsFragment(): TrackDetailsFragment
}
