package com.appetiser.bentest.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
