package com.appetiser.bentest.di

import android.app.Application
import com.appetiser.bentest.BentestApplication
import com.appetiser.bentest.di.builders.ActivityBuilder
import com.appetiser.bentest.di.builders.FragmentBuilder
import com.appetiser.bentest.di.builders.ServiceBuilder
import com.appetiser.module.data.features.RepositoryModule
import com.appetiser.module.local.StorageModule
import com.appetiser.module.local.features.DatabaseModule
import com.appetiser.module.network.NetworkModule
import com.appetiser.module.network.RemoteSourceModule
import com.appetiser.module.network.features.ApiServiceModule
import com.appetiser.module.notification.fcm.FirebaseModule
import com.appetiser.module.payouts.di.PayoutsActivityBuilder
import com.appetiser.module.payouts.di.PayoutsFragmentBuilder
import com.appetiser.module.payouts.di.PayoutsModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        StorageModule::class,
        FragmentBuilder::class,
        DatabaseModule::class,
        NetworkModule::class,
        ApiServiceModule::class,
        ActivityBuilder::class,
        SchedulerModule::class,
        ServiceBuilder::class,
        RepositoryModule::class,
        RemoteSourceModule::class,
        FirebaseModule::class,
        PayoutsModule::class,
        PayoutsActivityBuilder::class,
        PayoutsFragmentBuilder::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: BentestApplication)
}
