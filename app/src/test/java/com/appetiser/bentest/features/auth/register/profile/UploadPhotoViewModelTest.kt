package com.appetiser.bentest.features.auth.register.profile

import com.appetiser.bentest.Stubs
import com.appetiser.bentest.core.BaseViewModelTest
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.domain.utils.any
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.times
import org.mockito.Mockito.verify

class UploadPhotoViewModelTest : BaseViewModelTest() {

    private lateinit var subject: UploadPhotoViewModel

    private val repository: UserRepository = mock()
    private val observer: TestObserver<UploadPhotoState> = mock()

    @Before
    fun setup() {
        subject = UploadPhotoViewModel(repository)
        subject.schedulers = schedulers
        subject.state.subscribe(observer)
    }

    @Test
    fun uploadPhoto_ShouldEmitSuccessUploadPhoto_WhenResponseIsSuccessful() {
        val filePath = "/mnt/test"
        val user = Stubs.USER_LOGGED_IN

        val expectedState1 = UploadPhotoState.ShowProgressLoading
        val expectedState2 = UploadPhotoState.HideProgressLoading
        val expectedState3 = UploadPhotoState.SuccessUploadPhoto

        whenever(repository.uploadPhoto(any()))
            .thenReturn(Single.just(user))

        subject.uploadPhoto(filePath)
        testScheduler.triggerActions()

        argumentCaptor<UploadPhotoState>()
            .run {
                verify(
                    observer,
                    times(3)
                ).onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun uploadPhoto_ShouldEmitErrorUploadPhoto_WhenResponseThrowsException() {
        val filePath = "/mnt/test"
        val error = Throwable("Something went wrong")
        val expectedState1 = UploadPhotoState.ShowProgressLoading
        val expectedState2 = UploadPhotoState.HideProgressLoading
        val expectedState3 = UploadPhotoState.ErrorUploadPhoto(error)

        whenever(repository.uploadPhoto(any()))
            .thenReturn(Single.error(error))

        subject.uploadPhoto(filePath)
        testScheduler.triggerActions()

        argumentCaptor<UploadPhotoState>()
            .run {
                verify(
                    observer,
                    times(3)
                ).onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }
}
