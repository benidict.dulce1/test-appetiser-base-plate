package com.appetiser.bentest.features.account.deleteaccount

import com.appetiser.bentest.core.BaseViewModelTest
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.*
import io.reactivex.Completable
import io.reactivex.Observer
import io.reactivex.Single
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.HttpException
import retrofit2.Response

class DeleteAccountPasswordViewModelTest : BaseViewModelTest() {

    private val authRepository: AuthRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val stateObserver: Observer<DeleteAccountPasswordState> = mock()

    private lateinit var subject: DeleteAccountPasswordViewModel

    @Before
    fun setUp() {
        subject = DeleteAccountPasswordViewModel(authRepository, resourceManager)
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun onPasswordTextChanged_ShouldDisableButton_WhenPasswordIsEmpty() {
        val password = ""
        val expectedState = DeleteAccountPasswordState.DisableButton

        subject.onPasswordTextChanged(password)

        argumentCaptor<DeleteAccountPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun onPasswordTextChanged_ShouldEnableButton_WhenPasswordIsNotEmpty() {
        val password = "password"
        val expectedState = DeleteAccountPasswordState.EnableButton

        subject.onPasswordTextChanged(password)

        argumentCaptor<DeleteAccountPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun verifyPassword_ShouldNotCallRepository_WhenPasswordIsEmpty() {
        val password = ""

        subject.verifyPassword(password)

        Mockito.verifyZeroInteractions(authRepository)
    }

    @Test
    fun verifyPassword_ShouldEmitInvalidPassword_WhenPasswordIsIncorrect() {
        val password = "incorrect password"
        val expectedState1 = DeleteAccountPasswordState.ShowLoading
        val expectedState2 = DeleteAccountPasswordState.HideLoading
        val expectedState3 = DeleteAccountPasswordState.InvalidPassword
        val throwable =
            HttpException(
                Response.error<String>(
                    400,
                    "Test exception".toResponseBody()
                )
            )

        whenever(authRepository.verifyPassword(password))
            .thenReturn(Single.error(throwable))
        whenever(authRepository.deleteAccount(any()))
            .thenReturn(Completable.error(throwable))

        subject.verifyPassword(password)
        testScheduler.triggerActions()

        argumentCaptor<DeleteAccountPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun verifyPassword_ShouldEmitError_WhenResponseThrowsException() {
        val password = "password"
        val errorMessage = "Test error"
        val expectedState1 = DeleteAccountPasswordState.ShowLoading
        val expectedState2 = DeleteAccountPasswordState.HideLoading
        val expectedState3 = DeleteAccountPasswordState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        whenever(authRepository.verifyPassword(password))
            .thenReturn(Single.error(throwable))
        whenever(authRepository.deleteAccount(any()))
            .thenReturn(Completable.error(throwable))
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)

        subject.verifyPassword(password)
        testScheduler.triggerActions()

        argumentCaptor<DeleteAccountPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun verifyPassword_ShouldEmitVerificationSuccess_WhenResponseIsSuccessful() {
        val password = "password"
        val verificationToken = "test verification token"
        val expectedState1 = DeleteAccountPasswordState.ShowLoading
        val expectedState2 = DeleteAccountPasswordState.LogoutSocialLogins
        val expectedState3 = DeleteAccountPasswordState.HideLoading
        val expectedState4 = DeleteAccountPasswordState.NavigateToWalkthroughScreen

        whenever(authRepository.verifyPassword(password))
            .thenReturn(Single.just(verificationToken))
        whenever(authRepository.deleteAccount(any()))
            .thenReturn(Completable.complete())

        subject.verifyPassword(password)
        testScheduler.triggerActions()

        argumentCaptor<DeleteAccountPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(4)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
                Assert.assertEquals(expectedState4, allValues[3])
            }
    }
}
