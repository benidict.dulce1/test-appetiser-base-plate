package com.appetiser.bentest.features.account.settings

import com.appetiser.bentest.core.BaseViewModelTest
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.data.features.notification.NotificationRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.utils.*
import com.appetiser.module.notification.fcm.FirebaseRepository
import io.reactivex.Completable
import io.reactivex.Observer
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.times

class AccountSettingsViewModelTest : BaseViewModelTest() {

    private val authRepository: AuthRepository = mock()
    private val sessionRepository: SessionRepository = mock()
    private val notificationRepository: NotificationRepository = mock()
    private val firebaseRepository: FirebaseRepository = mock()
    private val resourceManager: ResourceManager = mock()

    private val stateObserver: Observer<AccountSettingsState> = mock()

    private lateinit var subject: AccountSettingsViewModel

    @Before
    fun setUp() {
        subject = AccountSettingsViewModel(
            authRepository,
            sessionRepository,
            notificationRepository,
            firebaseRepository,
            resourceManager
        )
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun logout_ShouldEmitError_WhenResponseThrowsException() {
        val errorMessage = "Test error."
        val mockToken = "mock token"

        val expectedState1 = AccountSettingsState.ShowLoading
        val expectedState2 = AccountSettingsState.HideLoading
        val expectedState3 = AccountSettingsState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        whenever(firebaseRepository.getDeviceToken())
            .thenReturn(Single.just(mockToken))
        whenever(notificationRepository.unRegisterDeviceToken(any(), any()))
            .thenReturn(Single.just(true))
        whenever(authRepository.logout())
            .thenReturn(Completable.error(throwable))
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)

        subject
            .logout {
                // do nothing
            }
        testScheduler.triggerActions()

        argumentCaptor<AccountSettingsState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun logout_ShouldEmitNavigateToWalkthroughScreen_WhenResponseIsSuccessful() {
        val mockToken = "mock token"

        val expectedState1 = AccountSettingsState.ShowLoading
        val expectedState2 = AccountSettingsState.HideLoading
        val expectedState3 = AccountSettingsState.NavigateToEmailCheck

        whenever(firebaseRepository.getDeviceToken())
            .thenReturn(Single.just(mockToken))
        whenever(notificationRepository.unRegisterDeviceToken(any(), any()))
            .thenReturn(Single.just(true))
        whenever(authRepository.logout())
            .thenReturn(Completable.complete())

        subject
            .logout {
                // do nothing
            }
        testScheduler.triggerActions()

        argumentCaptor<AccountSettingsState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }
}
