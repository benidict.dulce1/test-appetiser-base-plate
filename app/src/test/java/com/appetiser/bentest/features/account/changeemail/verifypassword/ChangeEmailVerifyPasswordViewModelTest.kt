package com.appetiser.bentest.features.account.changeemail.verifypassword

import com.appetiser.bentest.core.BaseViewModelTest
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.anyInt
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import io.reactivex.Observer
import io.reactivex.Single
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.times
import retrofit2.HttpException
import retrofit2.Response

class ChangeEmailVerifyPasswordViewModelTest : BaseViewModelTest() {

    private val authRepository: AuthRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val stateObserver: Observer<ChangeEmailVerifyPasswordState> = mock()

    private lateinit var subject: ChangeEmailVerifyPasswordViewModel

    @Before
    fun setUp() {
        subject = ChangeEmailVerifyPasswordViewModel(authRepository, resourceManager)
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun onPasswordTextChanged_ShouldDisableButton_WhenPasswordIsEmpty() {
        val password = ""
        val expectedState = ChangeEmailVerifyPasswordState.DisableButton

        subject.onPasswordTextChanged(password)

        argumentCaptor<ChangeEmailVerifyPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun onPasswordTextChanged_ShouldEnableButton_WhenPasswordIsNotEmpty() {
        val password = "password"
        val expectedState = ChangeEmailVerifyPasswordState.EnableButton

        subject.onPasswordTextChanged(password)

        argumentCaptor<ChangeEmailVerifyPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun verifyPassword_ShouldNotCallRepository_WhenPasswordIsEmpty() {
        val password = ""

        subject.verifyPassword(password)

        Mockito.verifyZeroInteractions(authRepository)
    }

    @Test
    fun verifyPassword_ShouldEmitInvalidPassword_WhenPasswordIsIncorrect() {
        val password = "incorrect password"
        val expectedState1 = ChangeEmailVerifyPasswordState.ShowLoading
        val expectedState2 = ChangeEmailVerifyPasswordState.HideLoading
        val expectedState3 = ChangeEmailVerifyPasswordState.InvalidPassword
        val throwable =
            HttpException(
                Response.error<String>(
                    400,
                    "Test exception".toResponseBody()
                )
            )

        whenever(authRepository.verifyPassword(password))
            .thenReturn(Single.error(throwable))

        subject.verifyPassword(password)
        testScheduler.triggerActions()

        argumentCaptor<ChangeEmailVerifyPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun verifyPassword_ShouldEmitError_WhenResponseThrowsException() {
        val password = "password"
        val errorMessage = "Test error"
        val expectedState1 = ChangeEmailVerifyPasswordState.ShowLoading
        val expectedState2 = ChangeEmailVerifyPasswordState.HideLoading
        val expectedState3 = ChangeEmailVerifyPasswordState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        whenever(authRepository.verifyPassword(password))
            .thenReturn(Single.error(throwable))
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)

        subject.verifyPassword(password)
        testScheduler.triggerActions()

        argumentCaptor<ChangeEmailVerifyPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun verifyPassword_ShouldEmitVerificationSuccess_WhenResponseIsSuccessful() {
        val password = "password"
        val verificationToken = "test verification token"
        val expectedState1 = ChangeEmailVerifyPasswordState.ShowLoading
        val expectedState2 = ChangeEmailVerifyPasswordState.HideLoading
        val expectedState3 =
            ChangeEmailVerifyPasswordState
                .VerificationSuccess(
                    verificationToken
                )

        whenever(authRepository.verifyPassword(password))
            .thenReturn(Single.just(verificationToken))

        subject.verifyPassword(password)
        testScheduler.triggerActions()

        argumentCaptor<ChangeEmailVerifyPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }
}
