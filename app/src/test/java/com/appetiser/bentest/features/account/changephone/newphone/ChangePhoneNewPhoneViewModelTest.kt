package com.appetiser.bentest.features.account.changephone.newphone

import com.appetiser.bentest.core.BaseViewModelTest
import com.appetiser.bentest.utils.PhoneNumberHelper
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.*
import io.reactivex.Completable
import io.reactivex.Observer
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert

import org.junit.Before

import org.junit.Test
import org.mockito.Mockito
import retrofit2.HttpException
import retrofit2.Response

class ChangePhoneNewPhoneViewModelTest : BaseViewModelTest() {

    private val authRepository: AuthRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val phoneNumberHelper: PhoneNumberHelper = mock()
    private val stateObserver: Observer<ChangePhoneNewPhoneState> = mock()

    private lateinit var subject: ChangePhoneNewPhoneViewModel

    @Before
    fun setUp() {
        subject = ChangePhoneNewPhoneViewModel(authRepository, resourceManager, phoneNumberHelper)
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun onPhoneTextChanged_ShouldDisableButton_WhenPhoneNumberIsEmpty() {
        val phoneNumber = ""
        val expectedState = ChangePhoneNewPhoneState.DisableButton

        subject.onPhoneTextChanged(phoneNumber)

        argumentCaptor<ChangePhoneNewPhoneState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun onPhoneTextChanged_ShouldEnableButton_WhenPhoneNumberIsNotEmpty() {
        val phoneNumber = "+639435542134"
        val expectedState = ChangePhoneNewPhoneState.EnableButton

        subject.onPhoneTextChanged(phoneNumber)

        argumentCaptor<ChangePhoneNewPhoneState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun changePhone_ShouldNotCallRepository_WhenPhoneNumberIsEmpty() {
        val countryCode = ""
        val countryNameCode = ""
        val phoneNumber = ""

        subject
            .changePhone(
                countryCode,
                countryNameCode,
                phoneNumber
            )

        Mockito.verifyZeroInteractions(authRepository)
    }

    @Test
    fun changePhone_ShouldEmitPhoneNumberExists_WhenPhoneNumberIsAlreadyTaken() {
        val countryCode = "63"
        val countryNameCode = "PH"
        val phoneNumber = "9435542134"
        val errorMessage = "Phone number already taken."
        val verificationToken = "test verification token"

        val expectedState1 = ChangePhoneNewPhoneState.ShowLoading
        val expectedState2 = ChangePhoneNewPhoneState.HideLoading
        val throwable =
            HttpException(
                Response
                    .error<Unit>(
                        422,
                        errorMessage.toResponseBody()
                    )
            )

        whenever(authRepository.requestChangePhone(any(), any()))
            .thenReturn(Completable.error(throwable))
        whenever(phoneNumberHelper.getInternationalFormattedPhoneNumber(any(), any()))
            .thenReturn("+$countryCode$phoneNumber")

        subject.setVerificationToken(verificationToken)
        subject
            .changePhone(
                countryCode,
                countryNameCode,
                phoneNumber
            )
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneNewPhoneState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert
                    .assertTrue(
                        allValues[2] is ChangePhoneNewPhoneState.PhoneNumberError
                    )
            }
    }

    @Test
    fun changePhone_ShouldEmitError_WhenResponseThrowsException() {
        val countryCode = "63"
        val countryNameCode = "PH"
        val phoneNumber = "9435542134"
        val errorMessage = "Test error."
        val verificationToken = "test verification token"

        val expectedState1 = ChangePhoneNewPhoneState.ShowLoading
        val expectedState2 = ChangePhoneNewPhoneState.HideLoading
        val expectedState3 = ChangePhoneNewPhoneState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)
        whenever(authRepository.requestChangePhone(any(), any()))
            .thenReturn(Completable.error(throwable))
        whenever(phoneNumberHelper.getInternationalFormattedPhoneNumber(any(), any()))
            .thenReturn("+$countryCode$phoneNumber")

        subject.setVerificationToken(verificationToken)
        subject
            .changePhone(
                countryCode,
                countryNameCode,
                phoneNumber
            )
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneNewPhoneState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun changePhone_ShouldEmitChangePhoneSuccess_WhenResponseIsSuccessful() {
        val countryCode = "63"
        val countryNameCode = "PH"
        val phoneNumber = "9435542134"
        val formattedPhoneNumbeer = "+$countryCode$phoneNumber"

        val verificationToken = "test verification token"

        val expectedState1 = ChangePhoneNewPhoneState.ShowLoading
        val expectedState2 = ChangePhoneNewPhoneState.HideLoading
        val expectedState3 =
            ChangePhoneNewPhoneState
                .ChangePhoneSuccess(
                    formattedPhoneNumbeer,
                    verificationToken
                )

        whenever(authRepository.requestChangePhone(any(), any()))
            .thenReturn(Completable.complete())
        whenever(phoneNumberHelper.getInternationalFormattedPhoneNumber(any(), any()))
            .thenReturn(formattedPhoneNumbeer)

        subject.setVerificationToken(verificationToken)
        subject
            .changePhone(
                countryCode,
                countryNameCode,
                phoneNumber
            )
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneNewPhoneState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }
}
