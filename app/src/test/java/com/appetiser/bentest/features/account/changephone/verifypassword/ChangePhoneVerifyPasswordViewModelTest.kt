package com.appetiser.bentest.features.account.changephone.verifypassword

import com.appetiser.bentest.core.BaseViewModelTest
import com.appetiser.bentest.utils.ResourceManager
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.anyInt
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import io.reactivex.Observer
import io.reactivex.Single
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.HttpException
import retrofit2.Response

class ChangePhoneVerifyPasswordViewModelTest : BaseViewModelTest() {

    private val authRepository: AuthRepository = mock()
    private val resourceManager: ResourceManager = mock()
    private val stateObserver: Observer<ChangePhoneVerifyPasswordState> = mock()

    private lateinit var subject: ChangePhoneVerifyPasswordViewModel

    @Before
    fun setUp() {
        subject = ChangePhoneVerifyPasswordViewModel(authRepository, resourceManager)
        subject.schedulers = schedulers
        subject.state.subscribe(stateObserver)
    }

    @Test
    fun onPasswordTextChanged_ShouldDisableButton_WhenPasswordIsEmpty() {
        val password = ""
        val expectedState = ChangePhoneVerifyPasswordState.DisableButton

        subject.onPasswordTextChanged(password)

        argumentCaptor<ChangePhoneVerifyPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun onPasswordTextChanged_ShouldEnableButton_WhenPasswordIsNotEmpty() {
        val password = "password"
        val expectedState = ChangePhoneVerifyPasswordState.EnableButton

        subject.onPasswordTextChanged(password)

        argumentCaptor<ChangePhoneVerifyPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState, value)
            }
    }

    @Test
    fun verifyPassword_ShouldNotCallRepository_WhenPasswordIsEmpty() {
        val password = ""

        subject.verifyPassword(password)

        Mockito.verifyZeroInteractions(authRepository)
    }

    @Test
    fun verifyPassword_ShouldEmitInvalidPassword_WhenPasswordIsIncorrect() {
        val password = "incorrect password"
        val expectedState1 = ChangePhoneVerifyPasswordState.ShowLoading
        val expectedState2 = ChangePhoneVerifyPasswordState.HideLoading
        val expectedState3 = ChangePhoneVerifyPasswordState.InvalidPassword
        val throwable =
            HttpException(
                Response.error<String>(
                    400,
                    "Test exception".toResponseBody()
                )
            )

        whenever(authRepository.verifyPassword(password))
            .thenReturn(Single.error(throwable))

        subject.verifyPassword(password)
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneVerifyPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun verifyPassword_ShouldEmitError_WhenResponseThrowsException() {
        val password = "password"
        val errorMessage = "Test error"
        val expectedState1 = ChangePhoneVerifyPasswordState.ShowLoading
        val expectedState2 = ChangePhoneVerifyPasswordState.HideLoading
        val expectedState3 = ChangePhoneVerifyPasswordState.Error(errorMessage)
        val throwable = Throwable(errorMessage)

        whenever(authRepository.verifyPassword(password))
            .thenReturn(Single.error(throwable))
        whenever(resourceManager.getString(anyInt()))
            .thenReturn(errorMessage)

        subject.verifyPassword(password)
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneVerifyPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun verifyPassword_ShouldEmitVerificationSuccess_WhenResponseIsSuccessful() {
        val password = "password"
        val verificationToken = "test verification token"
        val expectedState1 = ChangePhoneVerifyPasswordState.ShowLoading
        val expectedState2 = ChangePhoneVerifyPasswordState.HideLoading
        val expectedState3 =
            ChangePhoneVerifyPasswordState
                .VerificationSuccess(
                    verificationToken
                )

        whenever(authRepository.verifyPassword(password))
            .thenReturn(Single.just(verificationToken))

        subject.verifyPassword(password)
        testScheduler.triggerActions()

        argumentCaptor<ChangePhoneVerifyPasswordState>()
            .run {
                Mockito
                    .verify(
                        stateObserver,
                        Mockito.times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }
}
