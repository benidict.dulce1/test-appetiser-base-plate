package com.appetiser.bentest.features.auth.walkthrough

import android.os.Bundle
import com.appetiser.bentest.Stubs
import com.appetiser.bentest.core.BaseViewModelTest
import com.appetiser.bentest.utils.PAGE_STEP_1_POSITION
import com.appetiser.bentest.utils.PAGE_STEP_2_POSITION
import com.appetiser.bentest.utils.PAGE_STEP_3_POSITION
import com.appetiser.bentest.utils.PAGE_STEP_4_POSITION
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.times

class WalkthroughViewModelTest : BaseViewModelTest() {

    private lateinit var subject: WalkthroughViewModel

    private val sessionRepository: SessionRepository = mock()
    private val mockBundle: Bundle = mock()
    private val observer: TestObserver<WalkthroughState> = mock()

    @Before
    fun setUp() {
        subject = WalkthroughViewModel(sessionRepository)
        subject.schedulers = schedulers
        subject.state.subscribe(observer)
    }

    @Test
    fun isFirstTimeUiCreate_ShouldEmitLoggedInState_WhenUserSessionExistsAndOnboarded() {
        val expected = WalkthroughState.UserIsLoggedIn

        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(Stubs.SESSION_LOGGED_IN))

        subject.isFirstTimeUiCreate(mockBundle)

        testScheduler.triggerActions()

        Mockito
            .verify(
                sessionRepository,
                times(1)
            )
            .getSession()

        argumentCaptor<WalkthroughState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun isFirstTimeUiCreate_ShouldEmitLoggedInNotOnboardedState_WhenUserSessionExistsButNotOnboarded() {
        val expected = WalkthroughState.UserIsLoggedInButNotOnboarded

        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(Stubs.SESSION_USER_LOGGED_IN_NO_FULLNAME))

        subject.isFirstTimeUiCreate(mockBundle)

        testScheduler.triggerActions()

        Mockito
            .verify(
                sessionRepository,
                times(1)
            )
            .getSession()

        argumentCaptor<WalkthroughState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun isFirstTimeUiCreate_ShouldNotEmitLoggedInState_WhenUserSessionDoesNotExist() {
        whenever(sessionRepository.getSession())
            .thenReturn(Single.just(Stubs.SESSION_NOT_VERIFIED))
        val expected =
            WalkthroughState
                .UserIsNotLoggedIn

        subject.isFirstTimeUiCreate(mockBundle)

        testScheduler.triggerActions()

        Mockito
            .verify(
                sessionRepository,
                times(1)
            )
            .getSession()

        argumentCaptor<WalkthroughState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun onPageSelected_ShouldEmitCorrectPage_WhenSelectedPageIs2() {
        val previousPage = PAGE_STEP_1_POSITION
        val currentPage = PAGE_STEP_2_POSITION
        val expected =
            WalkthroughState
                .UpdatePageIndicator(
                    previousPage,
                    currentPage
                )

        subject.onPageSelected(currentPage)

        argumentCaptor<WalkthroughState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(1)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun onPageSelected_ShouldEmitCorrectPage_WhenSelectedPageIs3() {
        val previousPage = PAGE_STEP_2_POSITION
        val currentPage = PAGE_STEP_3_POSITION
        val expected =
            WalkthroughState
                .UpdatePageIndicator(
                    previousPage,
                    currentPage
                )

        subject.onPageSelected(PAGE_STEP_2_POSITION)
        subject.onPageSelected(currentPage)

        argumentCaptor<WalkthroughState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(2)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun onPageSelected_ShouldEmitShowStep4Buttons_WhenSelectedPageIs4() {
        val previousPage = PAGE_STEP_3_POSITION
        val currentPage = PAGE_STEP_4_POSITION
        val expected =
            WalkthroughState
                .ShowStep4Buttons(
                    previousPage,
                    currentPage
                )

        subject.onPageSelected(PAGE_STEP_3_POSITION)
        subject.onPageSelected(currentPage)

        argumentCaptor<WalkthroughState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(2)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun onPageSelected_ShouldEmitHideStep4Buttons_WhenSelectedPageIs4AndPreviousPageIs3() {
        val previousPage = PAGE_STEP_4_POSITION
        val currentPage = PAGE_STEP_3_POSITION
        val expected =
            WalkthroughState
                .HideStep4Buttons(
                    previousPage,
                    currentPage
                )

        subject.onPageSelected(PAGE_STEP_4_POSITION)
        subject.onPageSelected(currentPage)

        argumentCaptor<WalkthroughState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(2)
                    )
                    .onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }
}
