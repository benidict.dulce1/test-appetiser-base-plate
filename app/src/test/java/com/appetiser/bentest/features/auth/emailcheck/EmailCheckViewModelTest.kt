package com.appetiser.bentest.features.auth.emailcheck

import com.appetiser.bentest.core.TestSchedulerProvider
import com.appetiser.bentest.core.TestUtils.Companion.buildErrorResponseCheckEmail
import com.appetiser.module.data.features.auth.AuthRepository
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import retrofit2.HttpException

class EmailCheckViewModelTest {

    private lateinit var subject: EmailCheckViewModel

    private val testScheduler = TestScheduler()
    private val schedulers = TestSchedulerProvider(testScheduler)

    private val authRepository: AuthRepository = mock()
    private val observer: TestObserver<EmailCheckState> = mock()

    @Before
    fun setup() {
        subject = EmailCheckViewModel(authRepository)
        subject.schedulers = schedulers
        subject.state.subscribe(observer)
    }

    @Test
    fun checkEmail_ShouldEmitEmailExists_WhenResponseReturnsTrue() {
        val email = "foo@bar.baz"
        val exist = true
        val expected = EmailCheckState.EmailExists(email)

        whenever(authRepository.checkUsername(email))
            .thenReturn(Single.just(exist))

        subject.checkEmail(email)
        testScheduler.triggerActions()

        argumentCaptor<EmailCheckState>()
            .run {
                verify(
                    observer,
                    times(3)
                ).onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun checkEmail_ShouldEmitEmailDoesNotExist_WhenResponseReturns404() {
        val email = "foo1233123@bar.baz"
        val error = HttpException(buildErrorResponseCheckEmail())
        val expected = EmailCheckState.EmailDoesNotExist(email)

        whenever(authRepository.checkUsername(email))
            .thenReturn(Single.error(error))

        subject.checkEmail(email)
        testScheduler.triggerActions()

        argumentCaptor<EmailCheckState>()
            .run {
                verify(
                    observer,
                    times(3)
                ).onNext(capture())

                Assert.assertEquals(expected, value)
            }
    }

    @Test
    fun checkEmail_ShouldEmitError_WhenResponseThrowsException() {
        val email = "foo@bar.baz"
        val error = Throwable("Something went wrong")
        val expected = EmailCheckState.Error(error)

        whenever(authRepository.checkUsername(email))
            .thenReturn(Single.error(error))

        subject.checkEmail(email)
        testScheduler.triggerActions()

        argumentCaptor<EmailCheckState>().run {
            verify(
                observer,
                times(3)
            ).onNext(capture())

            Assert.assertEquals(expected, value)
        }
    }
}
