package com.appetiser.bentest.features.auth.register.phonenumber

import com.appetiser.bentest.Stubs
import com.appetiser.bentest.core.BaseViewModelTest
import com.appetiser.bentest.utils.PhoneNumberHelper
import com.appetiser.module.data.features.user.UserRepository
import com.appetiser.module.domain.utils.any
import com.appetiser.module.domain.utils.argumentCaptor
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.domain.utils.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.times

class PhoneNumberInputViewModelTest : BaseViewModelTest() {

    private val userRepository: UserRepository = mock()
    private val phoneNumberHelper: PhoneNumberHelper = mock()

    private val observer: TestObserver<PhoneNumberInputState> = mock()

    private lateinit var subject: PhoneNumberInputViewModel

    @Before
    fun setUp() {
        subject = PhoneNumberInputViewModel(userRepository, phoneNumberHelper)
        subject.schedulers = schedulers
        subject.state.subscribe(observer)
    }

    @Test
    fun savePhoneNumber_ShouldEmitUserDetailsUpdated_WhenResponseIsSuccessful() {
        val countryCode = "63"
        val countryNameCode = "PH"
        val phoneNumber = "9435542134"

        val user = Stubs.USER_LOGGED_IN
        val expectedState1 = PhoneNumberInputState.ShowProgressLoading
        val expectedState2 = PhoneNumberInputState.HideProgressLoading
        val expectedState3 = PhoneNumberInputState.UserDetailsUpdated(user)

        whenever(userRepository.updateUser(any()))
            .thenReturn(Single.just(user))
        whenever(phoneNumberHelper.getInternationalFormattedPhoneNumber(any(), any()))
            .thenReturn("+$countryCode$phoneNumber")

        subject
            .savePhoneNumber(
                countryCode,
                countryNameCode,
                phoneNumber
            )
        testScheduler.triggerActions()

        argumentCaptor<PhoneNumberInputState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }

    @Test
    fun savePhoneNumber_ShouldEmitError_WhenResponseThrowsException() {
        val countryCode = "63"
        val countryNameCode = "PH"
        val phoneNumber = "9435542134"

        val errorMessage = "Test error."
        val throwable = Throwable(errorMessage)
        val expectedState1 = PhoneNumberInputState.ShowProgressLoading
        val expectedState2 = PhoneNumberInputState.HideProgressLoading
        val expectedState3 = PhoneNumberInputState.Error(throwable)

        whenever(userRepository.updateUser(any()))
            .thenReturn(Single.error(throwable))
        whenever(phoneNumberHelper.getInternationalFormattedPhoneNumber(any(), any()))
            .thenReturn("+$countryCode$phoneNumber")

        subject
            .savePhoneNumber(
                countryCode,
                countryNameCode,
                phoneNumber
            )
        testScheduler.triggerActions()

        argumentCaptor<PhoneNumberInputState>()
            .run {
                Mockito
                    .verify(
                        observer,
                        times(3)
                    )
                    .onNext(capture())

                Assert.assertEquals(expectedState1, allValues[0])
                Assert.assertEquals(expectedState2, allValues[1])
                Assert.assertEquals(expectedState3, allValues[2])
            }
    }
}
