package com.appetiser.module.payments.features.payments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.preference.PreferenceManager
import com.appetiser.module.payments.R
import com.appetiser.module.payments.databinding.ActivityPaymentsBinding
import com.appetiser.module.payments.utils.StripeEphemeralKeyProvider
import com.appetiser.module.payments.utils.extensions.setVisible
import com.appetiser.module.payments.utils.extensions.showAlertDialog
import com.appetiser.module.payments.utils.extensions.toast
import com.stripe.android.*
import com.stripe.android.model.Customer
import com.stripe.android.model.PaymentMethod
import com.stripe.android.view.PaymentMethodsActivityStarter
import timber.log.Timber

class PaymentsActivity : AppCompatActivity() {

    companion object {
        private const val PREF_PAYMENT_METHOD_ID = "PREF_PAYMENT_METHOD_ID"
        private const val EXTRA_EPHEMERAL_KEY_JSON = "EXTRA_EPHEMERAL_KEY_JSON"

        /**
         * @param context
         * @param ephemeralKeyJson ephemeral key json. you should get this from your backend API.
         */
        fun openActivity(context: Context, ephemeralKeyJson: String) {
            context.startActivity(
                Intent(
                    context,
                    PaymentsActivity::class.java
                ).apply {
                    putExtra(EXTRA_EPHEMERAL_KEY_JSON, ephemeralKeyJson)
                }
            )
        }
    }

    private lateinit var binding: ActivityPaymentsBinding
    private lateinit var paymentSession: PaymentSession
    private var fromActivityResult: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_payments
        )
        binding.lifecycleOwner = this

        val ephemeralKeyJson = intent?.getStringExtra(EXTRA_EPHEMERAL_KEY_JSON)

        require(!ephemeralKeyJson.isNullOrEmpty()) {
            "Missing required intent extra 'ephemeralKeyJson'!"
        }

        CustomerSession
            .initCustomerSession(
                this,
                StripeEphemeralKeyProvider(ephemeralKeyJson) {
                    onCustomerSessionReady()
                }
            )
    }

    private fun onCustomerSessionReady() {
        CustomerSession
            .getInstance()
            .retrieveCurrentCustomer(object : CustomerSession.CustomerRetrievalListener {
                override fun onCustomerRetrieved(customer: Customer) {
                    setupPaymentSession()
                }

                override fun onError(errorCode: Int, errorMessage: String, stripeError: StripeError?) {
                    Timber.tag("onError").d(errorMessage)
                    toast(errorMessage)
                }
            })
    }

    private fun setupPaymentSession() {
        paymentSession = PaymentSession(
            this,
            createPaymentSessionConfig()
        )
        paymentSession.init(createPaymentSessionListener())
        paymentSession.presentPaymentMethodSelection()
    }

    private fun createPaymentSessionListener(): PaymentSession.PaymentSessionListener {
        return object : PaymentSession.PaymentSessionListener {
            override fun onCommunicatingStateChanged(isCommunicating: Boolean) {
                Timber.tag("onCommunicatingStateChanged").d("$isCommunicating")
                binding.progressBar setVisible isCommunicating
            }

            override fun onError(errorCode: Int, errorMessage: String) {
                Timber.tag("onError").d(errorMessage)
                showAlertDialog(
                    title = null,
                    body = errorMessage
                ) { _, _ ->
                    // do nothing
                }
            }

            override fun onPaymentSessionDataChanged(data: PaymentSessionData) {
                // NOTE: This will only be notified if user selected
                // different payment source aside from the default one.
                if (fromActivityResult) {
                    check(data.paymentMethod != null) {
                        toast(
                            getString(R.string.generic_error)
                        )
                        "Payment method is unexpectedly null after selecting one."
                    }

                    with(data.paymentMethod!!) {
                        id?.let {
                            saveLastSelectedPaymentMethodId(it)
                        }
                        handlePaymentMethodSelection(this)
                    }
                }
            }
        }
    }

    private fun createPaymentSessionConfig(): PaymentSessionConfig {
        return PaymentSessionConfig.Builder()
            .setPaymentMethodTypes(
                listOf(PaymentMethod.Type.Card)
            )
            .setShippingMethodsRequired(false)
            .setShippingInfoRequired(false)
            .setCanDeletePaymentMethods(true)
            .build()
    }

    private fun handlePaymentMethodSelection(paymentMethod: PaymentMethod) {
        // TODO 03/08/2020 Add required implementation. Maybe send result to calling activity?
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data == null) {
            finish()
            return
        }

        val result = PaymentMethodsActivityStarter.Result.fromIntent(data)

        if (result?.paymentMethod == null) {
            finish()
            return
        }

        val lastSelectedPaymentId = getLastSelectedPaymentMethodId()
        val paymentMethod = result.paymentMethod!!

        if (paymentMethod.id == lastSelectedPaymentId) {
            // User selected the same payment source.
            // We manually call this here since paymentSession.handlePaymentData()
            // only notifies callback when user selected different
            // payment source aside from the default one.
            handlePaymentMethodSelection(paymentMethod)
        } else {
            fromActivityResult = true
            paymentSession.handlePaymentData(requestCode, resultCode, data)
        }
    }

    private fun saveLastSelectedPaymentMethodId(paymentMethodId: String) {
        PreferenceManager
            .getDefaultSharedPreferences(this.applicationContext)
            .edit()
            .putString(PREF_PAYMENT_METHOD_ID, paymentMethodId)
            .apply()
    }

    private fun getLastSelectedPaymentMethodId(): String? {
        return PreferenceManager
            .getDefaultSharedPreferences(this.applicationContext)
            .getString(PREF_PAYMENT_METHOD_ID, null)
    }
}
