package com.appetiser.module.payments.utils

import android.os.Handler
import androidx.annotation.Size
import com.stripe.android.EphemeralKeyProvider
import com.stripe.android.EphemeralKeyUpdateListener

class StripeEphemeralKeyProvider constructor(
    private val ephemeralKeyJson: String,
    private val onCreateKeySuccessListener: () -> Unit
) : EphemeralKeyProvider {

    override fun createEphemeralKey(
        @Size(min = 4) apiVersion: String,
        keyUpdateListener: EphemeralKeyUpdateListener
    ) {
        keyUpdateListener.onKeyUpdate(ephemeralKeyJson)
        // Add delay to give time for Stripe to initialize Customer session.
        Handler()
            .postDelayed(
                {
                    onCreateKeySuccessListener()
                },
                500
            )
    }
}
