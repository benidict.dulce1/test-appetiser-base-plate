package com.appetiser.module.payments.utils

import android.content.Context
import com.stripe.android.CustomerSession
import com.stripe.android.PaymentConfiguration as StripePaymentConfiguration

object PaymentsHelper {
    fun initPayments(applicationContext: Context, stripeApiKey: String) {
        StripePaymentConfiguration
            .init(
                applicationContext,
                stripeApiKey
            )
    }

    fun logout() {
        CustomerSession.endCustomerSession()
    }
}
