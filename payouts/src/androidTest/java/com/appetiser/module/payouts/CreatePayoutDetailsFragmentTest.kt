package com.appetiser.module.payouts

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.PositionAssertions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class CreatePayoutDetailsFragmentTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(PayoutsActivity::class.java)

    @Before
    fun setUp() {
        onView(withId(R.id.editPayoutDetailsButton)).perform(click())
    }

    @Test
    fun toolbarTitle() {
        onView(withId(R.id.toolbarTitle)).check(matches(withText("Edit Payout Details")))
        onView(withId(R.id.toolbarButton)).run {
            check(matches(isDisplayed()))
            check(matches(withText("Save")))
        }
    }

    @Test
    fun bankDetails() {
        onView(withId(R.id.bankDetails)).run {
            check(matches(withText("Bank Details")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
        }
    }

    @Test
    fun accountHolderName() {
        onView(withId(R.id.accountHolderName)).run {
            check(matches(withHint("Account Holder Name")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.bankDetails)))
        }
    }

    @Test
    fun bsb() {
        onView(withId(R.id.bsb)).run {
            check(matches(withHint("BSB")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.accountHolderName)))
        }
    }

    @Test
    fun accountNumber() {
        onView(withId(R.id.accountNumber)).run {
            check(matches(withHint("Account Number")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.bsb)))
        }
    }

    @Test
    fun ownerDetails() {
        onView(withId(R.id.ownerDetails)).run {
            check(matches(withText("Owner Details")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.accountNumber)))
        }
    }

    @Test
    fun firstName() {
        onView(withId(R.id.firstName)).run {
            check(matches(withHint("First Name")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isCompletelyBelow(withId(R.id.ownerDetails)))
            check(isCompletelyLeftOf(withId(R.id.lastName)))
        }
    }

    @Test
    fun lastName() {
        onView(withId(R.id.lastName)).run {
            check(matches(withHint("Last Name")))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.ownerDetails)))
            check(isCompletelyRightOf(withId(R.id.firstName)))
        }
    }

    @Test
    fun dateOfBirth() {
        onView(withId(R.id.dateOfBirth)).run {
            check(matches(withHint("Date of Birth")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isCompletelyBelow(withId(R.id.firstName)))
        }
    }

    @Test
    fun ownerAddress() {
        onView(withId(R.id.ownerAddress)).run {
            check(matches(withText("Owner Address")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isCompletelyBelow(withId(R.id.dateOfBirth)))
        }
    }

    @Test
    fun streetAddress() {
        onView(withId(R.id.streetAddress)).run {
            check(matches(withHint("Street Address")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isCompletelyBelow(withId(R.id.ownerAddress)))
        }
    }

    @Test
    fun citySuburb() {
        onView(withId(R.id.citySuburb)).run {
            check(matches(withHint("City/Suburb")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isCompletelyBelow(withId(R.id.streetAddress)))
        }
    }

    @Test
    fun state() {
        onView(withId(R.id.state)).run {
            check(matches(withHint("State")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isCompletelyBelow(withId(R.id.citySuburb)))
            check(isCompletelyLeftOf(withId(R.id.postCode)))
        }
    }

    @Test
    fun postCode() {
        onView(withId(R.id.postCode)).run {
            check(matches(withHint("Post Code")))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.citySuburb)))
            check(isCompletelyRightOf(withId(R.id.state)))
        }
    }

    @Test
    fun idVerification() {
        onView(withId(R.id.idVerification)).run {
            check(matches(withText("ID Verification")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isCompletelyBelow(withId(R.id.state)))
        }
    }

    @Test
    fun frontLabel() {
        onView(withId(R.id.frontLabel)).run {
            check(matches(withText("Front")))
            check(isCompletelyRightOf(withId(R.id.startGuideline)))
            check(isCompletelyBelow(withId(R.id.idVerification)))
        }
    }

    @Test
    fun backLabel() {
        onView(withId(R.id.backLabel)).run {
            check(matches(withText("Back")))
            check(isCompletelyLeftOf(withId(R.id.endGuideline)))
            check(isCompletelyRightOf(withId(R.id.frontLabel)))
            check(isTopAlignedWith(withId(R.id.frontLabel)))
        }
    }

    @Test
    fun frontPhoto() {
        onView(withId(R.id.frontPhoto)).run {
            check(isCompletelyBelow(withId(R.id.frontLabel)))
            perform(click())
            check(withId(R.id.choosePhotoSourceParent).matches(isDisplayed()))
        }
    }

    @Test
    fun backPhoto() {
        onView(withId(R.id.backPhoto)).run {
            check(isCompletelyRightOf(withId(R.id.frontPhoto)))
            check(isTopAlignedWith(withId(R.id.frontPhoto)))
            check(isCompletelyBelow(withId(R.id.backLabel)))
        }
    }
}