package com.appetiser.module.payouts

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.PositionAssertions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class BankDetailsFragmentTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(PayoutsActivity::class.java)

    @Test
    fun toolbar() {
        onView(withId(R.id.toolbarTitle)).check(matches(withText("Payouts")))
        onView(withId(R.id.toolbarButton)).check(matches(not(isDisplayed())))
    }

    @Test
    fun bankDetails() {
        onView(withId(R.id.bankDetails)).run {
            check(matches(withText("Bank Details")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
        }
    }

    @Test
    fun accountHolderNameLabel() {
        onView(withId(R.id.accountHolderNameLabel)).run {
            check(matches(withText("Account Holder Name")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.bankDetails)))
        }
    }

    @Test
    fun accountHolderNameValue() {
        onView(withId(R.id.accountHolderNameValue)).run {
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.accountHolderNameLabel)))
        }
    }

    @Test
    fun separator1() {
        onView(withId(R.id.separator1)).check(isCompletelyBelow(withId(R.id.accountHolderNameValue)))
    }

    @Test
    fun bsbLabel() {
        onView(withId(R.id.bsbLabel)).run {
            check(matches(withText("BSB")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.separator1)))
        }
    }

    @Test
    fun bsbValue() {
        onView(withId(R.id.bsbValue)).run {
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.bsbLabel)))
        }
    }

    @Test
    fun separator2() {
        onView(withId(R.id.separator2)).check(isCompletelyBelow(withId(R.id.bsbValue)))
    }

    @Test
    fun accountNumberLabel() {
        onView(withId(R.id.accountNumberLabel)).run {
            check(matches(withText("Account Number")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.separator2)))
        }
    }

    @Test
    fun accountNumberValue() {
        onView(withId(R.id.accountNumberValue)).run {
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.accountNumberLabel)))
        }
    }

    @Test
    fun editPayoutDetailsButton() {
        onView(withId(R.id.editPayoutDetailsButton)).run {
            check(matches(withText("Edit Payout Details")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.accountNumberValue)))

            perform(click())
            onView(withId(R.id.editPayoutDetailsParent)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun idVerificationMessage() {
        onView(withId(R.id.idVerificationMessage)).run {
            check(matches(withText("Your ID needs to be verified to receive payouts in your bank account")))
            check(isLeftAlignedWith(withId(R.id.startGuideline)))
            check(isRightAlignedWith(withId(R.id.endGuideline)))
            check(isCompletelyBelow(withId(R.id.editPayoutDetailsButton)))
        }

    }
}