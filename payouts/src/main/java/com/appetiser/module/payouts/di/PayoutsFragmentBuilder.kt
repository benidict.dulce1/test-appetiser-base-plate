package com.appetiser.module.payouts.di

import com.appetiser.module.payouts.createpayoutdetails.CreatePayoutDetailsFragment
import com.appetiser.module.payouts.displaypayoutdetails.BankDetailsFragment
import com.appetiser.module.payouts.editpayoutdetails.EditPayoutDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PayoutsFragmentBuilder {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesBankDetailsFragment(): BankDetailsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesCreatePayoutDetailsFragment(): CreatePayoutDetailsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesEditPayoutDetailsFragment(): EditPayoutDetailsFragment
}