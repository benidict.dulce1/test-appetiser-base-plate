package com.appetiser.module.payouts

import com.appetiser.module.payouts.data.PayoutDetails

sealed class PayoutsState {
    object GoBackToSettingsScreen : PayoutsState()
    object GoToCreatePaymentDetailsScreen : PayoutsState()

    data class ShowErrorMessage(val errorMessage: String?) : PayoutsState()
    data class ShowPayoutDetails(val details: PayoutDetails) : PayoutsState()
}
