package com.appetiser.module.payouts.createpayoutdetails

import android.content.Intent
import android.hardware.camera2.CameraCharacteristics
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.appetiser.module.common.createImageFile
import com.appetiser.module.payouts.ActivityRequestCodes.REQUEST_CAMERA
import java.io.File

class Camera(val fragment: Fragment) {

    var photoFilePath = ""

    fun takePhoto(): String {
        val activity = fragment.requireActivity()

        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(activity.packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = activity.createImageFile().apply {
                    photoFilePath = this.absolutePath
                }

                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        activity,
                        "${activity.packageName}.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    takePictureIntent.putExtra(
                        "android.intent.extras.CAMERA_FACING",
                        CameraCharacteristics.LENS_FACING_FRONT
                    )

                    when {
                        Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
                            takePictureIntent.putExtra(
                                "android.intent.extras.CAMERA_FACING",
                                CameraCharacteristics.LENS_FACING_FRONT
                            )
                            takePictureIntent.putExtra(
                                "android.intent.extra.USE_FRONT_CAMERA",
                                true
                            )
                        }
                        else -> takePictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1)
                    }

                    fragment.startActivityForResult(takePictureIntent, REQUEST_CAMERA)
                }
            }
        }

        return photoFilePath
    }
}