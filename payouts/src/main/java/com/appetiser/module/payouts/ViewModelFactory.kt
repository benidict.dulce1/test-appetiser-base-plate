package com.appetiser.module.payouts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.appetiser.module.payouts.data.PayoutsRepository
import javax.inject.Inject

class ViewModelFactory @Inject constructor(val repository: PayoutsRepository) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(PayoutsViewModel::class.java) -> {
                PayoutsViewModel(repository) as T
            }
            else -> {
                throw IllegalArgumentException("Unknown ViewModel class")
            }
        }
    }
}