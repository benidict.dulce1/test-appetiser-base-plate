package com.appetiser.module.payouts.data

data class PayoutDetails(
    val bankDetails: BankDetails? = null,
    val ownerDetails: OwnerDetails,
    val ownerAddress: OwnerAddress,
    val idVerification: IdVerification? = null
) {
    data class BankDetails(
        val id: String? = null,
        val accountHolderName: String,
        val bsb: String,
        val accountNumber: String
    )

    data class OwnerDetails(
        val firstName: String,
        val lastName: String,
        val birthDay: Int,
        val birthMonth: Int,
        val birthYear: Int
    )

    data class OwnerAddress(
        val street: String,
        val city: String,
        val state: String,
        val country: String,
        val postCode: String
    )

    data class IdVerification(
        val frontPhoto: String,
        val backPhoto: String
    )
}