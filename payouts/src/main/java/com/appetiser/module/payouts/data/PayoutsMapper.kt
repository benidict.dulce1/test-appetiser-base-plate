package com.appetiser.module.payouts.data

import com.appetiser.module.network.features.payouts.ExternalAccountRequest
import com.appetiser.module.network.features.payouts.GetPayoutDetailsResponse
import com.appetiser.module.network.features.payouts.PayoutDetailsRequest
import com.appetiser.module.network.features.payouts.PayoutDetailsRequest.*
import com.appetiser.module.network.features.payouts.PayoutDetailsRequest.Individual.DateOfBirth
import com.appetiser.module.network.features.payouts.PayoutDetailsRequest.Verification.Document
import com.appetiser.module.payouts.data.PayoutDetails.OwnerAddress
import com.appetiser.module.payouts.data.PayoutDetails.OwnerDetails

class PayoutsMapper {
    fun domainToRemotePostPayoutDetails(from: PayoutDetails): PayoutDetailsRequest {
        return PayoutDetailsRequest(
            externalAccount = ExternalAccount(
                country = from.ownerAddress.country,
                accountHolderName = from.bankDetails!!.accountHolderName,
                routingNumber = from.bankDetails.bsb,
                accountNumber = from.bankDetails.accountNumber
            ),
            individual = Individual(
                firstName = from.ownerDetails.firstName,
                lastName = from.ownerDetails.lastName,
                dob = DateOfBirth(
                    day = from.ownerDetails.birthDay,
                    month = from.ownerDetails.birthMonth,
                    year = from.ownerDetails.birthYear
                ),
                address = Address(
                    city = from.ownerAddress.city,
                    line1 = from.ownerAddress.street,
                    postCode = from.ownerAddress.postCode,
                    state = from.ownerAddress.state,
                    country = from.ownerAddress.country
                ),
                verification = Verification(
                    Document(
                        back = from.idVerification!!.backPhoto,
                        front = from.idVerification.frontPhoto
                    )
                )
            )
        )
    }

    fun domainToRemoteUpdatePayoutDetails(from: PayoutDetails): PayoutDetailsRequest {
        return PayoutDetailsRequest(
            individual = Individual(
                firstName = from.ownerDetails.firstName,
                lastName = from.ownerDetails.lastName,
                dob = DateOfBirth(
                    day = from.ownerDetails.birthDay,
                    month = from.ownerDetails.birthMonth,
                    year = from.ownerDetails.birthYear
                ),
                address = Address(
                    city = from.ownerAddress.city,
                    line1 = from.ownerAddress.street,
                    postCode = from.ownerAddress.postCode,
                    state = from.ownerAddress.state,
                    country = from.ownerAddress.country
                )
            )
        )
    }

    fun remoteToDomain(from: GetPayoutDetailsResponse): PayoutDetails {
        return PayoutDetails(
            bankDetails = PayoutDetails.BankDetails(
                id = from.data.externalAccounts.data[0].id,
                accountHolderName = from.data.externalAccounts.data[0].accountHolderName,
                bsb = from.data.externalAccounts.data[0].routingNumber,
                accountNumber = "*${from.data.externalAccounts.data[0].last4}"
            ),
            ownerDetails = OwnerDetails(
                firstName = from.data.individual.firstName,
                lastName = from.data.individual.lastName,
                birthDay = from.data.individual.dob.day,
                birthMonth = from.data.individual.dob.month,
                birthYear = from.data.individual.dob.year
            ),
            ownerAddress = OwnerAddress(
                street = from.data.individual.address.line1,
                city = from.data.individual.address.city,
                state = from.data.individual.address.state,
                country = from.data.individual.address.country,
                postCode = from.data.individual.address.postalCode
            )
        )
    }

    fun domainToRemoteUpdateExternalAccount(from: PayoutDetails): ExternalAccountRequest {
        return ExternalAccountRequest(
            id = from.bankDetails!!.id!!,
            accountHolderName = from.bankDetails.accountHolderName
        )
    }
}
