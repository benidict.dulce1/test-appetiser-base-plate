package com.appetiser.module.payouts

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProviders
import com.appetiser.module.payouts.PayoutsState.GoToCreatePaymentDetailsScreen
import com.appetiser.module.payouts.PayoutsState.ShowPayoutDetails
import com.appetiser.module.payouts.createpayoutdetails.CreatePayoutDetailsFragment
import com.appetiser.module.payouts.displaypayoutdetails.BankDetailsFragment
import dagger.android.AndroidInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

class PayoutsActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val disposables: CompositeDisposable = CompositeDisposable()

    val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PayoutsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payouts)
        setUpToolbar()
        setUpViewModel()
    }

    private fun setUpViewModel() {
        viewModel.getPayoutDetails()

        viewModel
            .state
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleState(state: PayoutsState) {
        findViewById<ProgressBar>(R.id.loading).visibility = View.GONE

        when (state) {
            GoToCreatePaymentDetailsScreen -> {
                supportFragmentManager.commit {
                    replace(R.id.fragmentContainer, CreatePayoutDetailsFragment())
                }
            }

            is ShowPayoutDetails -> {
                supportFragmentManager.commit {
                    replace(R.id.fragmentContainer, BankDetailsFragment())
                }
            }
        }
    }

    private fun setUpToolbar() {
        findViewById<TextView>(R.id.toolbarTitle).text = getString(R.string.payouts)
        setSupportActionBar(findViewById(R.id.toolbarView))
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            title = null
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}