package com.appetiser.module.payouts

object ActivityRequestCodes {
    const val REQUEST_GALLERY = 0
    const val REQUEST_CAMERA = 1
}