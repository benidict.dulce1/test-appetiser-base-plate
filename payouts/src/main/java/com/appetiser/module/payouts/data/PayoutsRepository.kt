package com.appetiser.module.payouts.data

import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.payouts.IdVerificationResponse
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class PayoutsRepository @Inject constructor(
    private val sessionLocalSource: SessionLocalSource,
    private val payoutsRemoteSource: PayoutsRemoteSource,
    private val payoutsMapper: PayoutsMapper
) {

    fun savePayoutDetails(payoutDetails: PayoutDetails): Completable {
        return payoutsRemoteSource.savePayoutDetails(
            getToken(),
            payoutsMapper.domainToRemotePostPayoutDetails(payoutDetails)
        )
    }

    fun uploadVerificationDocument(filePath: String): Single<IdVerificationResponse> {
        return payoutsRemoteSource.uploadVerificationDocument(
            getToken(),
            filePath
        )
    }

    fun updatePayoutDetails(payoutDetails: PayoutDetails): Completable {
        return payoutsRemoteSource.updatePayoutDetails(
            getToken(),
            payoutsMapper.domainToRemoteUpdatePayoutDetails(payoutDetails),
            payoutsMapper.domainToRemoteUpdateExternalAccount(payoutDetails)
        )
    }

    fun getPayoutDetails(): Single<PayoutDetails> {
        return payoutsRemoteSource
            .getPayoutDetails(getToken())
            .map {
                payoutsMapper.remoteToDomain(it)
            }
    }

    fun deleteAccount(): Completable {
        return payoutsRemoteSource.deleteAccount(getToken())
    }

    private fun getToken() = "Bearer ${sessionLocalSource.getUserToken()}"
}
