package com.appetiser.module.payouts.createpayoutdetails

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.appetiser.module.common.ninjaTap
import com.appetiser.module.common.toast
import com.appetiser.module.domain.utils.DEFAULT_PHOTO_MAX_DIMENSION_PX
import com.appetiser.module.payouts.ActivityRequestCodes.REQUEST_CAMERA
import com.appetiser.module.payouts.ActivityRequestCodes.REQUEST_GALLERY
import com.appetiser.module.payouts.PayoutsActivity
import com.appetiser.module.payouts.PayoutsState
import com.appetiser.module.payouts.PayoutsState.*
import com.appetiser.module.payouts.R
import com.appetiser.module.payouts.data.PayoutDetails
import com.appetiser.module.payouts.data.PayoutDetails.*
import com.bumptech.glide.Glide
import com.mukesh.countrypicker.CountryPicker
import com.tbruyelle.rxpermissions2.RxPermissions
import com.yalantis.ucrop.UCrop
import dagger.android.support.AndroidSupportInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import java.io.File
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class CreatePayoutDetailsFragment : Fragment() {

    private val countryPicker by lazy {
        CountryPicker.Builder()
            .with(requireContext())
            .listener {
                requireView().findViewById<TextView>(R.id.country).text = it.name
            }
            .build()
    }

    private val viewModel by lazy {
        (requireActivity() as PayoutsActivity).viewModel
    }

    private val rxPermissions: RxPermissions by lazy {
        RxPermissions(this)
    }

    @IdRes
    private var tappedPhotoId = 0

    private val defaultDob by lazy {
        LocalDate
            .now()
            .minusYears(DEFAULT_DOB_SUBTRACTION)
    }

    private var frontPhotoFile: String = ""
    private var backPhotoFile: String = ""

    private var birthDay = 0
    private var birthMonth = 0
    private var birthYear = 0

    private val shortDateFormatter by lazy { DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT) }

    private val photoSourceDialog by lazy { PhotoSourceDialog() }

    private val disposables: CompositeDisposable = CompositeDisposable()

    private lateinit var datePickerDialog: DatePickerDialog

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create_payout_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
    }

    override fun onStart() {
        super.onStart()
        startObservers()
    }

    private fun startObservers() {
        viewModel
            .state
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun setUpViews() {
        requireActivity().run {
            findViewById<TextView>(R.id.toolbarTitle).text = getString(R.string.edit_payout_details)

            findViewById<TextView>(R.id.toolbarButton).apply {
                text = getString(R.string.save)
                visibility = View.VISIBLE
            }

            findViewById<Button>(R.id.frontPhoto).ninjaTap {
                startPhotoSourceDialog(R.id.frontPhoto)
            }

            findViewById<Button>(R.id.backPhoto).ninjaTap {
                startPhotoSourceDialog(R.id.backPhoto)
            }

            findViewById<EditText>(R.id.dateOfBirth).let {
                datePickerDialog = DatePickerDialog(
                    requireContext(),
                    { _, year, month, day ->
                        (it as EditText).setText(
                            shortDateFormatter.format(
                                LocalDate.of(
                                    year,
                                    month,
                                    day
                                )
                            )
                        )

                        birthDay = day
                        birthMonth = month
                        birthYear = year
                    },
                    defaultDob.year,
                    defaultDob.monthValue - 1,
                    defaultDob.dayOfMonth
                )

                it.ninjaTap {
                    datePickerDialog.show()
                }
            }


            findViewById<TextView>(R.id.toolbarButton).ninjaTap {
                viewModel.savePayoutDetails(getPayoutDetailsFromUi())
            }

            findViewById<TextView>(R.id.country).ninjaTap {
                countryPicker.showBottomSheet(requireActivity() as AppCompatActivity)
            }
        }
    }

    private fun startPhotoSourceDialog(@IdRes photoId: Int) {
        rxPermissions
            .requestEachCombined(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            .doOnNext {
                when {
                    it.granted -> {
                        photoSourceDialog.show(
                            childFragmentManager,
                            null
                        )

                        tappedPhotoId = photoId
                    }
                    else -> {
                        Toast.makeText(
                            requireContext(),
                            getString(R.string.no_camera_photos_permission),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
            .subscribe()
            .addTo(disposables)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK) return

        when {
            requestCode == REQUEST_CAMERA -> {
                handleCameraResults(resultCode)
            }

            requestCode == REQUEST_GALLERY && resultCode == Activity.RESULT_OK -> {
                handleGalleryResults(data)
            }

            requestCode == UCrop.REQUEST_CROP -> {
                handleCropResults(resultCode, data!!)
            }

            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun getPayoutDetailsFromUi(): PayoutDetails {
        requireActivity().run {
            val country = countryPicker.getCountryByName(findViewById<TextView>(R.id.country).text.toString())

            return PayoutDetails(
                BankDetails(
                    accountHolderName = findViewById<EditText>(R.id.accountHolderName).text.toString(),
                    bsb = findViewById<EditText>(R.id.bsb).text.toString(),
                    accountNumber = findViewById<EditText>(R.id.accountNumber).text.toString()
                ),
                OwnerDetails(
                    firstName = findViewById<EditText>(R.id.firstName).text.toString(),
                    lastName = findViewById<EditText>(R.id.lastName).text.toString(),
                    birthDay = birthDay,
                    birthMonth = birthMonth,
                    birthYear = birthYear
                ),
                OwnerAddress(
                    street = findViewById<EditText>(R.id.streetAddress).text.toString(),
                    city = findViewById<EditText>(R.id.citySuburb).text.toString(),
                    state = findViewById<EditText>(R.id.state).text.toString(),
                    postCode = findViewById<EditText>(R.id.postCode).text.toString(),
                    country = country.code
                ),
                IdVerification(
                    frontPhoto = frontPhotoFile,
                    backPhoto = backPhotoFile
                )
            )
        }
    }

    private fun handleGalleryResults(data: Intent?) {
        cropImage(data?.data!!)
    }

    private fun handleCropResults(resultCode: Int, data: Intent) {
        when (resultCode) {
            Activity.RESULT_OK -> {
                val path = UCrop.getOutput(data)?.path!!

                when (tappedPhotoId) {
                    R.id.frontPhoto -> {
                        frontPhotoFile = path
                    }
                    R.id.backPhoto -> {
                        backPhotoFile = path
                    }
                }

                Glide.with(this)
                    .load(UCrop.getOutput(data)?.path!!)
                    .placeholder(R.drawable.placeholder)
                    .into(view?.findViewById(tappedPhotoId)!!)
            }

            UCrop.RESULT_ERROR -> {
                requireActivity().toast(
                    UCrop.getError(data)?.message ?: getString(R.string.generic_error)
                )
            }
        }
    }

    private fun handleCameraResults(resultCode: Int) {
        if (resultCode == Activity.RESULT_CANCELED) {
            deleteImage()
        } else {
            cropImage(Uri.fromFile(File(photoSourceDialog.camera.photoFilePath)))
        }
    }

    private fun handleState(state: PayoutsState) {
        when (state) {
            GoBackToSettingsScreen -> {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.payout_details_created),
                    Toast.LENGTH_LONG
                ).show()
                requireActivity().finish()
            }

            is ShowPayoutDetails -> {
                showPayoutDetails(state.details)
            }

            is ShowErrorMessage -> {
                Toast.makeText(
                    requireContext(),
                    state.errorMessage ?: getString(R.string.generic_error_short),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun showPayoutDetails(details: PayoutDetails) {
        requireActivity().run {
            findViewById<EditText>(R.id.firstName).setText(details.ownerDetails.firstName)
            findViewById<EditText>(R.id.lastName).setText(details.ownerDetails.lastName)

            findViewById<EditText>(R.id.dateOfBirth).setText(
                shortDateFormatter.format(
                    LocalDate.of(
                        details.ownerDetails.birthYear,
                        details.ownerDetails.birthMonth,
                        details.ownerDetails.birthDay
                    )
                )
            )

            datePickerDialog.updateDate(
                details.ownerDetails.birthYear,
                details.ownerDetails.birthMonth - 1,
                details.ownerDetails.birthDay
            )
        }
    }

    private fun cropImage(photoUri: Uri) {
        UCrop
            .of(
                photoUri,
                Uri.fromFile(File.createTempFile("temp", null))
            )
            .withAspectRatio(1.5f, 1f)
            .withMaxResultSize(DEFAULT_PHOTO_MAX_DIMENSION_PX, DEFAULT_PHOTO_MAX_DIMENSION_PX)
            .start(requireContext(), this)
    }

    private fun deleteImage() {
        File(photoSourceDialog.camera.photoFilePath).let {
            if (it.exists()) {
                it.delete()
            }
        }
    }

    companion object {
        private const val DEFAULT_DOB_SUBTRACTION = 18L
    }
}
