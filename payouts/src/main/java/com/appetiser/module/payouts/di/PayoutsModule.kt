package com.appetiser.module.payouts.di

import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.payouts.ViewModelFactory
import com.appetiser.module.payouts.data.PayoutsMapper
import com.appetiser.module.payouts.data.PayoutsRemoteSource
import com.appetiser.module.payouts.data.PayoutsRepository
import dagger.Module
import dagger.Provides

@Module
class PayoutsModule {

    @Provides
    fun providesViewModelFactory(repository: PayoutsRepository) = ViewModelFactory(repository)

    @Provides
    fun providesPayoutsRepository(
        sessionLocalSource: SessionLocalSource,
        payoutsRemoteSource: PayoutsRemoteSource,
        payoutsMapper: PayoutsMapper
    ) = PayoutsRepository(sessionLocalSource, payoutsRemoteSource, payoutsMapper)

    @Provides
    fun providesPayoutsRemoteSource(apiServices: BaseplateApiServices) =
        PayoutsRemoteSource(apiServices)

    @Provides
    fun providesPayoutsMapper() = PayoutsMapper()
}