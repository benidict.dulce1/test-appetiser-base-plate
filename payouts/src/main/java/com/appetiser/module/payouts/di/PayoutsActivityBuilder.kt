package com.appetiser.module.payouts.di

import com.appetiser.module.payouts.PayoutsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PayoutsActivityBuilder {
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributesPayoutsActivity(): PayoutsActivity
}