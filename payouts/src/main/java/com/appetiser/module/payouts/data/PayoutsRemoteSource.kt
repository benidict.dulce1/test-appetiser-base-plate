package com.appetiser.module.payouts.data

import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.payouts.ExternalAccountRequest
import com.appetiser.module.network.features.payouts.GetPayoutDetailsResponse
import com.appetiser.module.network.features.payouts.IdVerificationResponse
import com.appetiser.module.network.features.payouts.PayoutDetailsRequest
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

class PayoutsRemoteSource @Inject constructor(
    private val apiServices: BaseplateApiServices
) {
    fun getPayoutDetails(token: String): Single<GetPayoutDetailsResponse> {
        return apiServices.getPayoutDetails(token)
    }

    fun savePayoutDetails(token: String, request: PayoutDetailsRequest): Completable {
        return apiServices.postPayoutDetails(token, request)
    }

    fun uploadVerificationDocument(
        token: String,
        filePath: String
    ): Single<IdVerificationResponse> {
        val imageFile = File(filePath)
        val requestImageBody = imageFile.asRequestBody("image/*".toMediaTypeOrNull())
        val requestBody =
            MultipartBody.Part.createFormData(
                "file",
                imageFile.name,
                requestImageBody
            )

        return apiServices.postIdVerification(
            token,
            file = requestBody
        )
    }

    fun deleteAccount(token: String): Completable {
        return apiServices.deletePayoutDetails(token)
    }

    fun updatePayoutDetails(
        token: String,
        payoutDetailsRequest: PayoutDetailsRequest,
        externalAccountRequest: ExternalAccountRequest
    ): Completable {
        return apiServices.updateExternalAccount(
            token,
            externalAccountRequest.id,
            externalAccountRequest
        )
            .andThen(apiServices.postPayoutDetails(token, payoutDetailsRequest))
    }
}
