package com.appetiser.module.common.widget

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.graphics.drawable.Icon
import android.net.Uri
import android.util.AttributeSet
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import io.reactivex.subjects.PublishSubject

class RxImageView : AppCompatImageView {

    private val publishSubject = PublishSubject.create<Boolean>()

    fun imageChanges() = publishSubject

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    override fun setImageBitmap(bm: Bitmap) {
        super.setImageBitmap(bm)
        publishSubject.onNext(true)
    }

    override fun setImageResource(@DrawableRes resId: Int) {
        super.setImageResource(resId)
        publishSubject.onNext(resId != 0)
    }

    override fun setImageURI(uri: Uri?) {
        super.setImageURI(uri)
        publishSubject.onNext(true)
    }

    override fun setImageIcon(icon: Icon?) {
        super.setImageIcon(icon)
        publishSubject.onNext(true)
    }

    override fun setBackground(background: Drawable?) {
        super.setBackground(background)
        publishSubject.onNext(true)
    }

    override fun setBackgroundResource(resId: Int) {
        super.setBackgroundResource(resId)
        publishSubject.onNext(resId != 0)
    }

    // special case just to get the publish subject changes
    fun triggerChanges() {
        publishSubject.onNext(true)
    }

    fun refreshTriggerChanges() {
        publishSubject.onNext(false)
    }
}
