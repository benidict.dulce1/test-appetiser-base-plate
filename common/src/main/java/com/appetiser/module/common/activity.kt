package com.appetiser.module.common

import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.res.Resources
import android.os.Environment
import android.util.DisplayMetrics
import android.view.View
import android.view.Window.ID_ANDROID_CONTENT
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.text.HtmlCompat
import com.appetiser.baseplate.common.R
import com.google.android.material.snackbar.Snackbar
import java.io.File
import java.io.IOException

fun getStatusBarHeight(): Int {
    val res = Resources.getSystem()
    val resourceId = res.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        return res.getDimensionPixelSize(resourceId)
    }

    return 0
}

fun Activity.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Activity.getDeviceHeight(): Int {
    return DisplayMetrics().apply {
        this@getDeviceHeight.windowManager.defaultDisplay.getMetrics(this)
    }.heightPixels
}

fun Activity.getDeviceWidth(): Int {
    return DisplayMetrics().apply {
        this@getDeviceWidth.windowManager.defaultDisplay.getMetrics(this)
    }.heightPixels
}

fun Activity.convertPxToDp(px: Float): Float {
    return px / this.resources.displayMetrics.density
}

fun Activity.convertDpToPx(dp: Float): Float {
    return dp * this.resources.displayMetrics.density
}

@Throws(IOException::class)
fun Activity.createImageFile(): File {
    val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!

    return File.createTempFile(
        "avatar", /* prefix */
        ".jpg", /* suffix */
        storageDir /* directory */
    )
}

fun Activity.getRootView(): View {
    return findViewById<View>(android.R.id.content)
}

fun Activity.isKeyboardOpen(): Boolean {
    val rootView = this.getRootView()
    val heightDiff = rootView.height - rootView.height
    val contentViewTop = window.findViewById<View>(ID_ANDROID_CONTENT).top

    return heightDiff > contentViewTop
}

fun Activity.isKeyboardClosed(): Boolean {
    return !this.isKeyboardOpen()
}

fun Activity.showAlertDialog(
    title: String?,
    body: String,
    dialogInterface: (DialogInterface, Int) -> Unit
) {
    if (body.isEmpty()) {
        return
    }

    val alertBuilder = AlertDialog.Builder(this)
    alertBuilder.setTitle(title)
    alertBuilder.setMessage(HtmlCompat.fromHtml(body, HtmlCompat.FROM_HTML_MODE_LEGACY))
    alertBuilder.setCancelable(false)
    alertBuilder.setPositiveButton(this.getString(android.R.string.ok), dialogInterface)
    alertBuilder.create().show()
}

fun Activity.showConfirmDialog(
    title: String = "",
    body: String,
    positiveButtonText: String,
    negativeButtonText: String,
    positiveClickListener: () -> Unit = {},
    negativeClickListener: () -> Unit = {}
) {
    val alertBuilder = AlertDialog.Builder(this)
    title.isNotEmpty().let {
        alertBuilder.setTitle(title)
    }
    alertBuilder.setMessage(HtmlCompat.fromHtml(body, HtmlCompat.FROM_HTML_MODE_LEGACY))
    alertBuilder
        .setPositiveButton(
            positiveButtonText
        ) { _, _ ->
            positiveClickListener()
        }
    alertBuilder
        .setNegativeButton(
            negativeButtonText
        ) { _, _ ->
            negativeClickListener()
        }
    alertBuilder.setCancelable(false)
    alertBuilder.create().show()
}

fun Activity.showGenericSuccessSnackBar(
    parentView: View,
    text: String
) {
    showSuccessSnackBar(
        parentView,
        text,
        parentView
            .context
            .getString(
                R.string.hide
            )
    ) {
        it.dismiss()
    }
}

fun Activity.showGenericErrorSnackBar(
    parentView: View,
    text: String
) {
    showErrorSnackBar(
        parentView,
        text,
        parentView
            .context
            .getString(
                R.string.hide
            )
    ) {
        it.dismiss()
    }
}

@SuppressLint("WrongConstant")
fun Activity.showSuccessSnackBar(
    parentView: View,
    text: String,
    actionText: String,
    actionClickListener: (Snackbar) -> Unit
): Snackbar {
    return Snackbar.make(parentView, text, SNACKBAR_DURATION)
        .apply {
            setAction(actionText) {
                actionClickListener(this)
            }
            setActionTextColor(
                parentView.context.getColor(R.color.white)
            )
            setBackgroundTint(
                parentView.context.getColor(R.color.cerulean_blue)
            )
            show()
        }
}

@SuppressLint("WrongConstant")
fun Activity.showErrorSnackBar(
    parentView: View,
    text: String,
    actionText: String,
    actionClickListener: (Snackbar) -> Unit
): Snackbar {
    return Snackbar.make(parentView, text, SNACKBAR_DURATION)
        .apply {
            setAction(actionText) {
                actionClickListener(this)
            }
            setActionTextColor(
                parentView.context.getColor(R.color.white)
            )
            setBackgroundTint(
                parentView.context.getColor(R.color.carnation)
            )
            show()
        }
}
