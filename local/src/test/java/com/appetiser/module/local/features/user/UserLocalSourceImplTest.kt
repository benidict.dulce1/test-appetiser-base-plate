package com.appetiser.module.local.features.user

import androidx.room.EmptyResultSetException
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.domain.utils.any
import com.appetiser.module.local.features.Stubs
import com.appetiser.module.local.features.user.dao.UserDao
import com.appetiser.module.local.features.user.models.UserDB
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class UserLocalSourceImplTest {

    private val userDao: UserDao = Mockito.mock(UserDao::class.java)
    private val stubDbUserSession = Stubs.DB_USER_SESSION
    private val stubUserSession = Stubs.USER_SESSION

    private lateinit var subject: UserLocalSource

    @Before
    fun setUp() {
        subject = UserLocalSourceImpl(userDao)
    }

    @Test
    fun getUser_ShouldReturnEmptyUser_WhenQueryIsEmpty() {
        val expected = Stubs.EMPTY_DB_USER_SESSION.fullName

        `when`(userDao.getUser())
            .thenReturn(
                Single.error(EmptyResultSetException("Empty query."))
            )

        subject
            .getUser()
            .test()
            .assertComplete()
            .assertValue { it.fullName == expected }
    }

    @Test
    fun getUser_ShouldMapDbUserSessionToDomainUserSession() {
        val expected = UserDB.toDomain(stubDbUserSession)

        `when`(userDao.getUser()).thenReturn(Single.just(stubDbUserSession))

        subject
            .getUser()
            .test()
            .assertComplete()
            .assertValue { it == expected }
    }

    @Test
    fun saveUser_ShouldLogoutPreviousUser() {
        testSaveUser()

        Mockito.verify(userDao, Mockito.times(1)).deleteUsers()
    }

    @Test
    fun saveUser_ShouldInsertNewUserSessionToDatabase() {
        testSaveUser()

        Mockito.verify(userDao, Mockito.times(1))
            .saveUser(UserDB.fromDomain(stubUserSession))
    }

    @Test
    fun saveUser_ShouldEmitUserArgument() {
        val expected = stubUserSession

        testSaveUser()
            .assertValue {
                it == expected
            }
    }

    private fun testSaveUser(): TestObserver<User> {
        `when`(userDao.deleteUsers()).thenReturn(Completable.complete())
        `when`(userDao.saveUser(any())).thenReturn(Single.just(1))

        return subject
            .saveUser(stubUserSession)
            .test()
            .assertValue { stubUserSession == it }
    }
}
