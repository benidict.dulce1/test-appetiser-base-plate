package com.appetiser.module.local.features.track

import com.appetiser.module.domain.models.track.Track
import io.reactivex.Completable
import io.reactivex.Single

interface TrackLocalSource {
    fun saveTrack(track: Track): Single<Track>
    fun getTrack(): Single<Track>
    fun deleteTrack(): Completable
    fun saveLastVisitDate(date: String)
    fun getLastVisitDate(): Single<String>
}