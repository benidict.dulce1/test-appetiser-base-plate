package com.appetiser.module.local.features.user.models

import androidx.room.TypeConverter
import com.appetiser.module.domain.models.user.MediaFile
import com.google.gson.Gson

class UserDBConverters {
    @TypeConverter
    fun avatarFromString(value: String?): MediaFile {
        return Gson().fromJson(value ?: "{}", MediaFile::class.java)
    }

    @TypeConverter
    fun avatarToString(avatar: MediaFile?): String {
        return Gson().toJson(avatar ?: MediaFile.empty())
    }
}
