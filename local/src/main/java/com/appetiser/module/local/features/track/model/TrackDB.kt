package com.appetiser.module.local.features.track.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.appetiser.module.domain.models.track.Track
import com.appetiser.module.local.features.user.models.UserDB

@Entity(tableName = TrackDB.TRACK_TABLE_NAME)
data class TrackDB(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "dataId")
    val dataId: Int = 0,
    @ColumnInfo(name = "artistId")
    val artistId: String?= null,
    @ColumnInfo(name = "wrapperType")
    val wrapperType: String?=null,
    @ColumnInfo(name = "kind")
    val kind: String?= null,
    @ColumnInfo(name = "collectionId")
    val collectionId: String?=null,
    @ColumnInfo(name = "trackId")
    val trackId: String?=null,
    @ColumnInfo(name = "artistName")
    val artistName: String?=null,
    @ColumnInfo(name = "collectionName")
    val collectionName: String?=null,
    @ColumnInfo(name = "trackName")
    val trackName: String?=null,
    @ColumnInfo(name = "collectionCensoredName")
    val collectionCensoredName: String?=null,
    @ColumnInfo(name = "trackCensoredName")
    val trackCensoredName :String?=null,
    @ColumnInfo(name = "collectionArtistId")
    val collectionArtistId: String?=null,
    @ColumnInfo(name = "collectionArtistViewUrl")
    val collectionArtistViewUrl: String?=null,
    @ColumnInfo(name = "collectionViewUrl")
    val collectionViewUrl: String?=null,
    @ColumnInfo(name = "trackViewUrl")
    val trackViewUrl: String?=null,
    @ColumnInfo(name = "previewUrl")
    val previewUrl: String?=null,
    @ColumnInfo(name = "artworkUrl30")
    val artworkUrl30: String?=null,
    @ColumnInfo(name = "artworkUrl60")
    val artworkUrl60: String?=null,
    @ColumnInfo(name = "artworkUrl100")
    val artworkUrl100: String?=null,
    @ColumnInfo(name = "collectionPrice")
    val collectionPrice: String?=null,
    @ColumnInfo(name = "trackPrice")
    val trackPrice: String?=null,
    @ColumnInfo(name = "trackRentalPrice")
    val trackRentalPrice: String?=null,
    @ColumnInfo(name = "collectionHdPrice")
    val collectionHdPrice: String?=null,
    @ColumnInfo(name = "trackHdPrice")
    val trackHdPrice: String?=null,
    @ColumnInfo(name = "trackHdRentalPrice")
    val trackHdRentalPrice: String?=null,
    @ColumnInfo(name = "releaseDate")
    val releaseDate: String?=null,
    @ColumnInfo(name = "collectionExplicitness")
    val collectionExplicitness: String?=null,
    @ColumnInfo(name = "trackExplicitness")
    val trackExplicitness: String?=null,
    @ColumnInfo(name = "discCount")
    val discCount: String?=null,
    @ColumnInfo(name = "discNumber")
    val discNumber: String?=null,
    @ColumnInfo(name = "trackCount")
    val trackCount: String?=null,
    @ColumnInfo(name = "trackNumber")
    val trackNumber: String?=null,
    @ColumnInfo(name = "trackTimeMillis")
    val trackTimeMillis: String?=null,
    @ColumnInfo(name = "country")
    val country: String?=null,
    @ColumnInfo(name = "currency")
    val currency: String?=null,
    @ColumnInfo(name = "primaryGenreName")
    val primaryGenreName: String?=null,
    @ColumnInfo(name = "contentAdvisoryRating")
    val contentAdvisoryRating: String?=null,
    @ColumnInfo(name = "shortDescription")
    val shortDescription: String?= null,
    @ColumnInfo(name = "longDescription")
    val longDescription: String?= null,
    @ColumnInfo(name = "hasITunesExtras")
    val hasITunesExtras: String?=null
){
    companion object{
        const val TRACK_TABLE_NAME = "track_table"
        const val EMPTY_TRACK_ID = "empty"

        fun empty(): TrackDB {
            return TrackDB(
                trackId = EMPTY_TRACK_ID
            )
        }

        fun fromDomain(track: Track): TrackDB{
            return with(track){
                TrackDB(
                    dataId = dataId,
                    artistId = artistId,
                    wrapperType = wrapperType,
                    kind = kind,
                    collectionId = collectionId,
                    trackId = trackId,
                    artistName = artistName,
                    collectionName = collectionName,
                    trackName = trackName,
                    collectionCensoredName = collectionCensoredName,
                    trackCensoredName = trackCensoredName,
                    collectionViewUrl = collectionViewUrl,
                    trackViewUrl = trackViewUrl,
                    previewUrl = previewUrl,
                    artworkUrl30 = artworkUrl30,
                    artworkUrl60 = artworkUrl60,
                    artworkUrl100 = artworkUrl100,
                    collectionPrice = collectionPrice,
                    trackPrice = trackPrice,
                    collectionHdPrice = collectionHdPrice,
                    trackHdPrice = trackHdPrice,
                    trackHdRentalPrice = trackHdRentalPrice,
                    releaseDate = releaseDate,
                    collectionExplicitness = collectionExplicitness,
                    trackExplicitness = trackExplicitness,
                    discCount = discCount,
                    discNumber = discNumber,
                    trackCount = trackCount,
                    trackNumber = trackNumber,
                    trackTimeMillis = trackTimeMillis,
                    country = country,
                    currency = currency,
                    primaryGenreName = primaryGenreName,
                    contentAdvisoryRating = contentAdvisoryRating,
                    shortDescription = shortDescription,
                    longDescription = longDescription,
                    hasITunesExtras = hasITunesExtras
                )
            }
        }

        fun toDomain(trackDB: TrackDB): Track{
            return with(trackDB){
                Track(
                    dataId = dataId,
                    artistId = artistId,
                    wrapperType = wrapperType,
                    kind = kind,
                    collectionId = collectionId,
                    trackId = trackId,
                    artistName = artistName,
                    collectionName = collectionName,
                    trackName = trackName,
                    collectionCensoredName = collectionCensoredName,
                    trackCensoredName = trackCensoredName,
                    collectionViewUrl = collectionViewUrl,
                    trackViewUrl = trackViewUrl,
                    previewUrl = previewUrl,
                    artworkUrl30 = artworkUrl30,
                    artworkUrl60 = artworkUrl60,
                    artworkUrl100 = artworkUrl100,
                    collectionPrice = collectionPrice,
                    trackPrice = trackPrice,
                    collectionHdPrice = collectionHdPrice,
                    trackHdPrice = trackHdPrice,
                    trackHdRentalPrice = trackHdRentalPrice,
                    releaseDate = releaseDate,
                    collectionExplicitness = collectionExplicitness,
                    trackExplicitness = trackExplicitness,
                    discCount = discCount,
                    discNumber = discNumber,
                    trackCount = trackCount,
                    trackNumber = trackNumber,
                    trackTimeMillis = trackTimeMillis,
                    country = country,
                    currency = currency,
                    primaryGenreName = primaryGenreName,
                    contentAdvisoryRating = contentAdvisoryRating,
                    shortDescription = shortDescription,
                    longDescription = longDescription,
                    hasITunesExtras = hasITunesExtras
                )
            }
        }

    }
}