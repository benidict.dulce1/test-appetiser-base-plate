package com.appetiser.module.local.features.user

import com.appetiser.module.domain.models.user.User
import io.reactivex.Completable
import io.reactivex.Single

interface UserLocalSource {
    fun saveUser(user: User): Single<User>

    fun getUser(): Single<User>

    fun deleteUser(): Completable
}
