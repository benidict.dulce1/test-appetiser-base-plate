package com.appetiser.module.local.features.track

import android.content.SharedPreferences
import androidx.room.EmptyResultSetException
import androidx.room.Insert
import com.appetiser.module.domain.models.track.Track
import com.appetiser.module.domain.utils.OnErrorResumeNext
import com.appetiser.module.local.features.token.AccessTokenLocalSourceImpl
import com.appetiser.module.local.features.track.dao.TrackDao
import com.appetiser.module.local.features.track.model.TrackDB
import io.reactivex.Completable
import io.reactivex.Single
import timber.log.Timber
import javax.inject.Inject

class TrackLocalSourceImpl @Inject constructor(
    private val sharedPreferences: SharedPreferences,
    private val trackDao: TrackDao
): TrackLocalSource{

    companion object {
        private const val PREF_LAST_VISIT = "PREF_LAST_VISIT"
    }

    override fun saveLastVisitDate(date: String) {
        sharedPreferences.edit()
            .apply {
                putString(PREF_LAST_VISIT, date)
                apply()
            }
    }

    override fun getLastVisitDate(): Single<String> = Single.just(sharedPreferences.getString(PREF_LAST_VISIT, "") ?: "")

    override fun saveTrack(track: Track): Single<Track>
            = trackDao
                .deleteTrack()
                .andThen(trackDao.saveTrack(TrackDB.fromDomain(track)))
                .map {
                    Timber.tag("saveTrack").d("$it")
                    track
                }

    override fun getTrack(): Single<Track>
            = trackDao
                .getTrack()
                .compose (
                    OnErrorResumeNext<TrackDB, EmptyResultSetException>(
                        TrackDB.empty(),
                        EmptyResultSetException::class.java
                    )
                 ).map {
                    TrackDB.toDomain(it)
                }

    override fun deleteTrack(): Completable
        = trackDao.deleteTrack()

}