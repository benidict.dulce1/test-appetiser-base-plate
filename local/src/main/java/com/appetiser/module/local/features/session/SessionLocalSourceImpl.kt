package com.appetiser.module.local.features.session

import com.appetiser.module.domain.models.Session
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import com.appetiser.module.local.features.token.AccessTokenLocalSource
import com.appetiser.module.local.features.user.UserLocalSource
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class SessionLocalSourceImpl @Inject constructor(
    private val userLocalSource: UserLocalSource,
    private val accessTokenLocalSource: AccessTokenLocalSource
) : SessionLocalSource {

    var session: Session? = null

    override fun getSession(): Single<Session> {
        if (session == null) {
            return getSessionFromDb()
                .doOnSuccess { this.session = it }
        }

        return Single.just(session)
    }

    override fun saveSession(session: Session): Single<Session> {
        return Single
            .zip(
                userLocalSource.saveUser(session.user),
                accessTokenLocalSource.saveAccessToken(session.accessToken),
                BiFunction<User, AccessToken, Pair<User, AccessToken>> { user, accessToken ->
                    Pair(user, accessToken)
                }
            )
            .map { pair ->
                session.user = pair.first
                session.accessToken = pair.second

                this.session = session
                this.session
            }
    }

    override fun getUserToken(): String {
        return accessTokenLocalSource.getAccessTokenFromPref()
    }

    private fun getSessionFromDb(): Single<Session> {
        return Single
            .zip(
                userLocalSource.getUser(),
                accessTokenLocalSource.getAccessToken(),
                BiFunction { user, accessToken ->
                    val session = Session(user, accessToken)

                    session
                }
            )
    }

    override fun clearSession(): Completable {
        return userLocalSource
            .deleteUser()
            .andThen(accessTokenLocalSource.deleteToken())
            .doOnComplete {
                this.session = null
            }
    }
}
