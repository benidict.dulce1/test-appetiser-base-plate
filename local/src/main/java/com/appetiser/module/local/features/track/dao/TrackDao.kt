package com.appetiser.module.local.features.track.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.features.track.model.TrackDB
import io.reactivex.Completable
import io.reactivex.Single

@Dao
abstract class TrackDao: BaseDao<TrackDB>{
    @Query("SELECT * FROM ${TrackDB.TRACK_TABLE_NAME} LIMIT 1")
    abstract fun getTrack(): Single<TrackDB>

    @Query("DELETE FROM ${TrackDB.TRACK_TABLE_NAME}")
    abstract fun deleteTrack(): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveTrack(trackDB: TrackDB): Single<Long>
}